import React, {Component} from 'react';
import {AvField} from 'availity-reactstrap-validation'

class RegistrationFirstStep extends Component {
  render() {
    const {changeState} = this.props;
    return (
      <div className='col-md-8 p-0 mx-auto'>
        <div className="row">
          <div className="col-md-6">
            <label>
              FirstName
            </label>
            <AvField type="text"
              // value={firstName}
                     onChange={changeState}
                     className='form-control'
                     name='firstName'/>
          </div>
          <div className="col-md-6">
            <label>
              LastName
            </label>
            <AvField type="text"
              // value={lastName}
                     onChange={changeState}
                     className='form-control'
                     name='lastName'/>
          </div>
          <div className="col-md-12">
            <label>
              Email
            </label>
            <AvField type="email"
              // value={email}
                     onChange={changeState}
                     className='form-control'
                     name='email'/>
          </div>
          <div className="col-md-12">
            <label>
              PhoneNumber
            </label>
            <AvField type="text"
              // value={phoneNumber}
                     onChange={changeState}
                     className='form-control'
                     name='phoneNumber'/>
          </div>
          <div className="col-md-12">
            <label>
              Password
            </label>
            <AvField type="password"
              // value={password}
                     onChange={changeState}
                     className='form-control'
                     name='password'/>
          </div>
          <div className="col-md-12">
            <label>
              PrePassword
            </label>
            <AvField type="password"
              // value={prePassword}
                     onChange={changeState}
                     className='form-control'
                     name='prePassword'/>
          </div>
        </div>
      </div>
    );
  }
}

RegistrationFirstStep.propTypes = {};

export default RegistrationFirstStep;
