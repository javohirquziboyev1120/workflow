import React, {Component} from 'react';
import {AvField} from 'availity-reactstrap-validation'


class RegistrationSecondStep extends Component {
  render() {
    const {changeState} = this.props;
    return (
      <div className='col-md-8 p-0 mx-auto'>
        <div className="row">
          <div className="col-md-12">
            <label>
              Company Name
            </label>
            <AvField type="text" onChange={changeState} className='form-control'
                     name='companyName'/>
          </div>
          <div className="col-md-12">
            <label>
              Street
            </label>
            <AvField type="text" onChange={changeState} className='form-control'
                     name='street'/>
          </div>
          <div className="col-md-12">
            <label>
              Email
            </label>
            <AvField type="text" onChange={changeState} className='form-control'
                     name='companyEmail'/>
          </div>
          <div className="col-md-12">
            <label>
              Phone Number
            </label>
            <AvField type="text" onChange={changeState}
                     className='form-control'
                     name='companyPhoneNumber'/>
          </div>
          <div className="col-md-12">
            <label>
              Region Id
            </label>
            <AvField type="text" onChange={changeState} className='form-control'
                     name='regionId'/>
          </div>
          <div className="col-md-12">
            <label>
              District Id
            </label>
            <AvField type="text" onChange={changeState}
                     className='form-control'
                     name='districtId'/>
          </div>
        </div>
      </div>
    );
  }
}

RegistrationSecondStep.propTypes = {};

export default RegistrationSecondStep;
