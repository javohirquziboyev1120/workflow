import React, {Component} from 'react';
import {Button, Col, Label, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {AvField,AvForm} from 'availity-reactstrap-validation'

class CompanyModal extends Component {
  render() {
    const {dispatch, openModal, isModalShow, addCompany, currentUser} = this.props;
    const closedModal = () => {
      openModal();
    };
    return (
      <div>
        <Modal isOpen={isModalShow} toggle={openModal}>
          <ModalHeader
            toggle={openModal}>{currentUser.id != null ? "Userni o'zgartirishni xoxlaysizmi?" : "Foydalanuvchi kiritish"}</ModalHeader>
          <ModalBody>
            <AvForm onValidSubmit={addCompany} className="form-horizontal form-material">
                <Label className="my-0">Nomi</Label>
                <AvField name="firstName" value={currentUser.name} placeholder="Nomi"
                         style={{fontSize: 14}} required/>
                <Label className="my-0">Telefon nomer</Label>
                <AvField name="phoneNumber" value={currentUser.phoneNumber} placeholder="+998 ** *** ** **"
                         style={{fontSize: 14}} required/>
                <Label className="my-0">Elektron pochta</Label>
                <AvField name="email" value={currentUser.email} placeholder="hamroz2206@gmail.com"
                         style={{fontSize: 14}}
                         required/>
             <Row>
                <Col md={6} className="offset-7 mt-4">
                  <Button type="submit" className={'btn btn-primary mr-1 rounded'}
                          style={{fontSize: 14, backgroundColor: '#40a9ff', borderColor: '#40a9ff'}}>Saqlash
                  </Button>
                  {' '}
                  <Button onClick={closedModal} className={'btn btn-primary'}
                          style={{
                            fontSize: 14,
                            backgroundColor: '#f0f0f0',
                            borderColor: '#f0f0f0',
                            color: '#000'
                          }}>Bekor qilish
                  </Button>
                </Col>
              </Row>
            </AvForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default CompanyModal;
