import React, {Component} from 'react';
import AdminLayout from "../components/AdminLayout";
import DirectorLayout from "../components/DirectorLayout";
import 'antd/dist/antd.css'

class App extends Component {

  render() {
    return (
      <div>
        {this.props.count === 1 ?
          <div>
            <AdminLayout/>
            {this.props.children}
          </div>
          :
          <div>
            <DirectorLayout/>
            {this.props.children}
          </div>
        }

      </div>
    );
  }
}

App.propTypes = {};

export default App;
