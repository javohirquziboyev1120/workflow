import React from 'react';
import App from "./app";
import {ToastContainer} from "react-toastify";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../global.css';
import 'react-toastify/dist/ReactToastify.css';

function BasicLayout(props) {
  return (
    <App count={5}>
      <div>
        <ToastContainer/>
        {props.children}
      </div>
    </App>
  );
}

export default BasicLayout;
