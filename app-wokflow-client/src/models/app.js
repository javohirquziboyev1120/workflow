import api from 'api'
import {COMPANY_ID, STORAGE_NAME} from "../utils/constant";
import {router} from "umi";
import {connect} from "react-redux";
import config from 'config'

const {userMe,getCategory, getCompany,getContact, getBrand, getProduct, getDetail,getMeasurement} = api;
export default ({
  namespace: 'app',
  state: {
    currentUser: '',
    categories:[],
    companies:[],
    currentItem:'',
    showModal:false,
    showDeleteModal:false,
    contacts:[],
    brands : [],
    products: [],
    details:[],
    measurements:[],

  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (!config.openPages.includes(pathname)) {
          dispatch({
            type: 'userMe',
            payload: {
              pathname
            }
          })
        }
      })
    }
  },
  effects: {
    * userMe({payload}, {call, put}) {
      try {
        const res = yield call(userMe);
        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {currentUser: ''}
          });
          localStorage.removeItem(STORAGE_NAME);
          router.push('/auth/login');
        } else {
          yield put({
            type: 'updateState',
            payload: {
              currentUser: res.object
            }
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: ''}
        });
        localStorage.removeItem(STORAGE_NAME);
        router.push('/auth/login');
      }
    },

    * logOut({}, {call, put,}) {
      yield put({
        type: 'updateState',
        payload: {currentUser: ''}
      });
      localStorage.removeItem(STORAGE_NAME);
      router.push('/auth/login');
    },
    * getCategory({}, {call, put}) {
      let res = yield call(getCategory);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            categories: res._embedded.list
          }
        });
      }
    },
    * getDetail({}, {call, put}) {
      let res = yield call(getDetail);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            details: res._embedded.list
          }
        });
      }
    },
    * getCompany({}, {call, put}) {
      let res = yield call(getCompany);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            companies: res.object
          }
        });
      }
    },

    * getContacts({}, {call, put}) {
      let res = yield call(getContact);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            contacts: res._embedded.list
          }
        });
      }
    },

    * getBrand({}, {call, put}) {
      let companyId = localStorage.getItem(COMPANY_ID)
      let res = yield call(getBrand,{companyId:companyId});
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            brands: res.object
          }
        });
      }
    },

    * getProduct({}, {call, put}) {
      let res = yield call(getProduct);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            products: res._embedded.list
          }
        });
      }
    },

    * getMeasurement({}, {call, put}) {
      let res = yield call(getMeasurement);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            measurements: res._embedded.list
          }
        });
      }
    },

  },


  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
