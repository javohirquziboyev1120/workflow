export default {
  login: 'POST /auth/login',
  register: 'POST /auth/register',
  userMe: '/user/me',

  createInputProductWarehouse: 'POST /input',
  updateInputTrade: 'PUT /input/edit/trade', //{id}
  updateInputProduct: 'PUT /input/edit/input-product', //{id}
  updateInputProductAmount: 'PUT /input/edit/input-product-amount', //{id}
  getInputProduct: '/input/product', //{id}
  getInputTrade: '/input/trade/', //{id}
  getAllInputProduct: '/input/product/all', // ?page= &size=
  getAllInputTrade: '/input/trade/all', // ?page= &size=
  createInputProductReturn: 'POST /input/return',
  addRegion: 'POST /region',
  editRegion: 'PATCH /region/',
  deleteRegion: 'DELETE /region',
  addCategory: 'POST /category',
  editCategory: 'PATCH /category',
  deleteCategory: 'DELETE /category',
  getCategory: '/category',
  getCompany: '/company/page',
  addCompany: 'POST /company',
  deleteCompany: 'DELETE /company',
  addContact: 'POST /contact',
  editContact: 'PUT /contact',
  deleteContact: 'DELETE /contact',
  getContact: '/contact',


  addSupplier: 'POST /supplier',
  editSupplier: 'PATCH /supplier',
  deleteSupplier: 'DELETE /supplier',

  addCustomer: 'POST /customer',
  editCustomer: 'PATCH /customer',
  deleteCustomer: 'DELETE /customer',
  getCustomer: '/customer',
  getSupplier: '/supplier',

  getDetail: '/detail',
  getMeasurement: '/measurement',
  getProduct : '/product/page',
  getValuesByDetail : '/product/getValues',
  getBrand: '/brand',
  addProduct:'POST /product',
  editProduct:'PUT /product',
  deleteProduct:'DELETE /product',
}
