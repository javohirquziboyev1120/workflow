import api from 'api'
import {STORAGE_NAME} from '../../../utils/constant'
import {router} from "umi";


export default ({
  namespace: 'warehouseInputModel',
  state: {},
  subscriptions: {},

  effects: {},
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
});
