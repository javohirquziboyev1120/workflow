import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Container, CustomInput, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import router from "umi/router";
import {STORAGE_NAME} from "../../../utils/constant";
import {AddIcon, DeleteIcon, EditIcon} from "../../../utils/incon";


@connect(({region}) => ({region}))
class Region extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'region/getRegions'
    })
  }


  changeState = (e) => {
    const {name, value} = e.target;
    this.props.dispatch({
      type: 'region/updateState',
      payload: {
        [name]: value
      }
    })
  }

  render() {
    const {dispatch, region} = this.props;
    const {loading, regions, showModal, currentItem, showDeleteModal} = region;

    const handleSubmit = (e, v) => {
      e.preventDefault()
      if (!currentItem){
         dispatch({
           type: 'region/addRegion',
           payload:{
             ...v,
           }
         })
        }else {
         dispatch({
           type: 'region/editRegion',
           payload:{
             id:currentItem.id,
             ...v,
           }
         })
        }
    };
    const openModal = (item) => {
      dispatch({
        type: 'region/updateState',
        payload: {
          showModal: !showModal,
          currentItem: item
        }
      })
    };
    const openDeleteModal = (item) => {
      dispatch({
        type: 'region/updateState',
        payload: {
          showDeleteModal: !showDeleteModal,
          currentItem: item
        }
      })
    };
    const deleteRegion = (v) => {
      dispatch({
        type: 'region/deleteRegion',
        payload: {
          ...v
        }
      })
    };

    if(!localStorage.getItem(STORAGE_NAME)) {
      router.push('/login');
      return <div/>;
    }

    return (
      <div>
        <Container>
          <Row>
            <h1>Region list</h1>
          </Row>
          <AddIcon
            onClick={() => openModal('')}
          />
          <Table className="custom-table">
            <thead>
            <tr>
              <th>#</th>
              <th>NameUz</th>
              <th>NameRu</th>
              <th colSpan="2">Operation</th>
            </tr>
            </thead>
            {regions ?
              <tbody>
              {regions.map((item, i) =>
                <tr key={item.id}>
                  <td>{i + 1}</td>
                  <td>{item.nameUz}</td>
                  <td>{item.nameRu}</td>
                  <td>
                    <EditIcon
                      onClick={() => openModal(item)}
                    />
                  </td>
                  <td>
                    <DeleteIcon
                      onClick={() =>
                        openDeleteModal(item)
                      }
                    />
                  </td>
                </tr>
              )}
              </tbody> :
              <tbody>
              <tr>
                <td colSpan="4">
                  <h3 className="text-center mx-auto"> No information </h3>
                </td>
              </tr>
              </tbody>
            }
          </Table>

        </Container>
        <Modal isOpen={showModal}>
          <AvForm onValidSubmit={handleSubmit}>
            <ModalHeader>
              Add Region
            </ModalHeader>
            <ModalBody>
              <AvField defaultValue={currentItem ? currentItem.nameUz : ""}
                       name="nameUz" label="Region nameUz" placeholder="Enter region nameUz " required/>
              <AvField defaultValue={currentItem ? currentItem.nameRu : ""}
                       name="nameRu" label="Region nameRu" placeholder="Enter region nameRu " required/>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" type="button" onClick={openModal}>Cancel</Button>
              <Button color="success">Save</Button>
            </ModalFooter>
          </AvForm>
        </Modal>

        <Modal isOpen={showDeleteModal}>
          <ModalHeader>
            <h3>Are sure delete {currentItem ? currentItem.nameUz : ""}?</h3>
          </ModalHeader>
          <ModalFooter>
            <Button onClick={openDeleteModal}>Cancel</Button>
            <Button onClick={() => deleteRegion(currentItem)}>Delete</Button>
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

export default Region;

