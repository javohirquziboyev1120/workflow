import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";
import {toast} from "react-toastify";

const {addRegion, getRegion, deleteRegion,editRegion} = api;
export default ({
  namespace: 'region',
  state: {
    loading: false,
    regions:[],
    currentItem:'',
    showModal:false,
    showDeleteModal:false
  },
  subscriptions: {},

  effects: {
    * addRegion({payload}, {call, put, select}) {
      const res = yield call(addRegion, payload);
      if (res.success) {
        toast.success("Region saqlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getRegions'
        })
      }
    },

    * editRegion({payload}, {call, put, select}) {
      const res = yield call(editRegion,{path:payload.id, ...payload});
      if (res.success) {
        toast.success("Region tahrirlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getRegions'
        })
      }
    },

    * getRegions({}, {call, put}) {
      let res = yield call(getRegion);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.list
          }
        });
      }
    },
    * deleteRegion({payload}, {call, put, select}) {
      let res = yield call(deleteRegion, payload);
      if (res.success) {
        toast.success("Region o'chirildi");
        yield put({
          type: 'updateState',
          payload: {
            showDeleteModal: false
          }
        });
        yield put({
          type: 'getRegions'
        })
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
