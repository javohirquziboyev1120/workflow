import React, {Component} from 'react';
import {connect} from "react-redux";
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";
import {AvField,AvForm} from 'availity-reactstrap-validation'
import 'bootstrap/dist/css/bootstrap.css'

@connect(({login}) => ({login}))
class Login extends Component {

  componentDidMount() {
    if (localStorage.getItem(STORAGE_NAME)) {
      router.push('/cabinet')
    }
  }

  render() {
    const {dispatch, login} = this.props;
    const {loading} = login;

    const signIn = (e,v) => {
        dispatch({
        type: 'login/login',
        payload: {
          ...v
        }
      })
    }

    return (
      !localStorage.getItem(STORAGE_NAME) ?
        <div className='container'>
          <div className="row">
            <div className="col-md-5 mx-auto mt-5">
              <h4>Login</h4>
              <AvForm onValidSubmit={signIn}>
                <div>
                  <AvField
                    placeholder='Phone Number'
                    className='form-control'
                    type="text"
                    name="phoneNumber"/>
                </div>
                <div className='mt-3'>
                  <AvField
                    placeholder='Password'
                    className='form-control'
                    type="password"
                    name="password"/>
                </div>
                <button type='submit'
                  className='btn btn-success mt-3'>Login
                </button>
              </AvForm>
            </div>
          </div>
        </div> : ''
    );
  }
}

export default Login;
