import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";

const {login} = api;
export default ({
  namespace: 'login',
  state: {
    loading: false,
    phoneNumber: '',
    password: ''
  },
  subscriptions: {
    // setup({dispatch, history}) {
    //   history.listen(({pathname}) => {
    //     if (localStorage.getItem(STORAGE_NAME)) {
    //       router.push('/cabinet')
    //     }
    //   })
    // }
    // commentdan ochish taqiqlanadi !!!
  },

  effects: {
    * login({payload}, {call, put, select}) {
      const res = yield call(login, payload);
      yield put({
        type: 'updateState',
        payload: {
          loading: true
        }
      })
      if (res.success) {
        localStorage.setItem(STORAGE_NAME, 'Bearer' + ' ' + res.object);
        router.push('/cabinet')
      }
      yield put({
        type: 'updateState',
        payload: {
          loading: false
        }
      })
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
