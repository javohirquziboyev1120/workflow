import React, {Component} from 'react';
import {connect} from "react-redux";
import {STORAGE_NAME} from "../../../utils/constant";

@connect(({app}) => ({app}))
class Cabinet extends Component {
  logout = () => {
    this.props.dispatch({
      type: 'app/logOut'
    })
  };

  render() {
    const {app} = this.props;
    const {currentUser} = app;
    console.log(currentUser);
    return (
      localStorage.getItem(STORAGE_NAME) ?
        <div>
          <h1>Cabinet</h1>
          <button onClick={this.logout}>logout</button>
        </div> :
        ''
    );
  }
}

Cabinet.propTypes = {};

export default Cabinet;
