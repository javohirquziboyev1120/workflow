import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";


const {signUp} = api;

export default ({
  namespace: 'registerModel',
  state: {
    loading: false,
    step: 1
  },
  subscriptions: {},

  effects: {
    * signUp({payload}, {call, put, select}) {
      const res = yield call(signUp, payload);
      if (res.success) {
        console.log(res.object)
        localStorage.setItem(STORAGE_NAME, res.body.tokenType + " " + res.body.accessToken);
        router.push('/cabinet')
      }
    }
  },

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
});
