import React, {Component} from 'react';
import {connect} from "react-redux";
import {AvForm} from 'availity-reactstrap-validation'
import RegistrationFirstStep from "../../../components/RegisterSteps/RegistrationFirstStep";
import RegistrationSecondStep from "../../../components/RegisterSteps/RegistrationSecondStep";

@connect(({registerModel}) => ({registerModel}))
class Index extends Component {
  // state = {
  //   obj: {
  //     ketmon: 'qalay'
  //   }
  // };

  render() {
    const {dispatch, registerModel} = this.props;
    const {step} = registerModel;

    const changeStep = (step) => {
      dispatch({
        type: 'registerModel/updateState',
        payload: {step}
      })
    };

    const register = () => {
      // dispatch({
      //   type: 'registerModel/signUp',
      //   payload: {...registerModel}
      // })
    };

    const changeState = (e) => {
      const {value, name} = e.target;
      dispatch({
        type: 'registerModel/updateState',
        payload: {
          [name]: value
        }
      })
    };


    return (
      <div className="container">
        <AvForm
          model={registerModel}
          onValidSubmit={register}
        >
          <div className="row">
            {
              step === 1 ?
                <RegistrationFirstStep
                  states={registerModel}
                  changeState={changeState}
                />
                : step === 2 &&
                <RegistrationSecondStep
                  states={registerModel}
                  changeState={changeState}
                />
            }
            <div className="col-md-8 mx-auto p-0">
              <div
                className='ml-auto'
                style={{width: 'max-content'}}
              >
                {step > 1 &&
                <button
                  type={'button'}
                  className='btn btn-info mr-2'
                  onClick={() => changeStep(step - 1)}
                >
                  Previous
                </button>
                }
                {step === 2 ?
                  <button
                    type={'submit'}
                    className='btn'
                  >
                    Submit
                  </button> :
                  <button
                    type={'button'}
                    className='btn btn-success'
                    onClick={() => changeStep(step + 1)}
                  >
                    Next
                  </button>
                }
              </div>
            </div>
          </div>
        </AvForm>
      </div>
    );
  }
}

export default Index;



