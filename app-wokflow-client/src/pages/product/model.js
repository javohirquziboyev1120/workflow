import api from 'api'
import {toast} from "react-toastify";
import {COMPANY_ID} from "../../utils/constant";

const {addProduct,
  editProduct,
  deleteProduct,getProduct,getValuesByDetail} = api;

export default ({
  namespace: 'productModel',
  state: {
    count : 1,
    products : [],
    showModal:false,
    showDeleteModal: false,
    currentItem:'',
    values:[],
  },
  subscriptions: {},

  effects: {
    * addProduct({payload}, {call, put, select}) {
      const res = yield call(addProduct, payload);
      if (res.success) {
        toast.success("Product saqlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getProduct'
        })
      }
    },

    * editProduct({payload}, {call, put, select}) {
      const res = yield call(editProduct, payload);
      if (res.success) {
        toast.success("Product tahrirlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getProduct'
        })
      }
    },

    * getValuesByDetail({payload}, {call, put, select}) {
      const res = yield call(getValuesByDetail,{path:payload.id});
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            values: res.object
          }
        });
      }
      },
    * getProduct({}, {call, put}) {
      let companyId = localStorage.getItem(COMPANY_ID)
      let res = yield call(getProduct,{companyId:companyId});
      console.log(res);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            products: res.object
          }
        });
      }
    },
    * deleteProduct({payload}, {call, put, select}) {
      let res = yield call(deleteProduct, payload);
      if (res.success) {
        toast.success("Product o'chirildi");
        yield put({
          type: 'updateState',
          payload: {
            showDeleteModal: false
          }
        });
        yield put({
          type: 'getProduct'
        })
      }
    },

  },

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
});
