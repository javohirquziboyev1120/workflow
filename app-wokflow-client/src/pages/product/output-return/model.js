import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";

const {outputProductReturn} = api;
export default ({
  namespace: 'outputProductReturn',
  state: {
    loading: false
  },

  subscriptions: {},

  effects: {},

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
