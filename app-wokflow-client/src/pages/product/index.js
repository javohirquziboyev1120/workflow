import React, {Component} from 'react';
import {connect} from "react-redux";
import {AddIcon, DeleteIcon, EditIcon} from "../../utils/incon";
import {AvField, AvForm} from 'availity-reactstrap-validation';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import Table from "reactstrap/lib/Table";
import {COMPANY_ID} from "../../utils/constant";

@connect(({productModel, app}) => ({productModel, app}))
class Product extends Component {
  state = {
    count: 1,
    isOpen: false,
  }

  inc = () => {
    this.setState({count: this.state.count + 1});
  }

  dec = () => {
    if(this.state.count === 1) return;
    this.setState({count : this.state.count - 1});
  }

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'productModel/getProduct'
    })
    dispatch({
      type: 'app/getBrand'
    })
    dispatch({
      type: 'app/getCategory'
    })
    dispatch({
      type: 'app/getDetail'
    })
    dispatch({
      type: 'app/getMeasurement'
    })
  }

  handleChange = (id) => {
    this.props.app.details.map(a => {
      if (a.id === id) {
        this.props.dispatch({
          type: 'productModel/getValuesByDetail',
          payload: {
            id,
          }
        })

      }
    })
  }

  render() {
    const {productModel, app, dispatch} = this.props;
    const {currentItem, showModal, showDeleteModal, products, values} = productModel;
    const {currentUser, categories, companies, contacts, brands, details, measurements} = app
    const handleSubmit = (e, v) => {
      let companyId = localStorage.getItem(COMPANY_ID)
      let arr = [];
      Object.keys(v).map(function (key, index) {
        if (key.startsWith('valuesId')) {
          arr.push(v[key])
        }
      })
      v.valuesId=arr

      v.companyId = companyId
      e.preventDefault()
      if (!currentItem) {
        dispatch({
          type: 'productModel/addProduct',
          payload: {
            ...v,
          }
        })
      } else {
        dispatch({
          type: 'productModel/editProduct',
          payload: {
            id: currentItem.id,
            ...v,
          }
        })
      }
    };
    const openModal = (item) => {
      dispatch({
        type: 'productModel/updateState',
        payload: {
          showModal: !showModal,
          currentItem: item
        }
      })
    };
    const openDeleteModal = (item) => {
      dispatch({
        type: 'productModel/updateState',
        payload: {
          showDeleteModal: !showDeleteModal,
          currentItem: item
        }
      })
    };
    const deleteProduct = (v) => {
      dispatch({
        type: 'productModel/deleteProduct',
        payload: {
          ...v
        }
      })
    };
    const changeDetail = (e) => {
      const {value} = e.target;
      this.handleChange(value)
    }
    return (
      <div>
        <div className='container-fluid'>
          <AddIcon onClick={() => openModal('')} className={''}/>
          <div className="row">
            <div className="col-md-12">
              <Table className='table'>
                <thead className='table-light'>
                <tr>
                  <th scope="col">T/R</th>
                  {/*<th scope="col">Logo</th>*/}
                  <th scope="col">Name</th>
                  <th scope="col">Company</th>
                  <th scope="col">Brand</th>
                  <th scope="col">Bar Code</th>
                  <th scope="col">Category</th>
                  <th scope="col">Measurement</th>
                  <th scope="col">Detail</th>
                  <th scope="col">Description</th>
                  <th scope="col">Active</th>
                  <th colSpan="2">Operation</th>
                </tr>
                </thead>
                <tbody>
                {products ? products.map((item, index) => (
                  <tr key={item.id}>
                    <td>{index + 1}</td>
                    <td>
                      {item.nameUz}
                    </td>
                    <td>
                      {item.companyDto.name}
                    </td>
                    <td>
                      {item.brandDto.nameUz}
                    </td>
                    <td>
                      {item.barCode}
                    </td>
                    <td>
                      {item.categoryDto.nameUz}
                    </td>
                    <td>
                      {item.measurementDto.map(item => {
                        return item.nameUz + " "
                      })}
                    </td>
                    <td>
                      {item.valuesDto.map(item => {
                        return item.nameUz
                      })}
                    </td>
                    <td>
                      {item.description}
                    </td>
                    <td>
                      {item.active}
                    </td>
                    <td>
                      <EditIcon
                        onClick={() => openModal(item)}
                      />
                    </td>
                    <td>
                      <DeleteIcon
                        onClick={() =>
                          openDeleteModal(item)
                        }
                      />
                    </td>
                  </tr>
                )) : "No information"}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
        <Modal isOpen={showModal}>
          <AvForm onValidSubmit={handleSubmit}>
            <ModalHeader>
              Add Region
            </ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="col-md-6">
                  <AvField
                    type={'text'}
                    label={'Name Uz'}
                    name={'nameUz'}
                    value={currentItem && currentItem.nameUz}
                    onChange={this.changeState}
                  />
                </div>
                <div className="col-md-6">
                  <AvField
                    type={'text'}
                    label={'Name Ru'}
                    name={'nameRu'}
                    value={currentItem && currentItem.nameRu}
                    onChange={this.changeState}
                  />
                </div>
                <div className="col-md-12">
                  <div>
                    <img
                      src=""
                      alt="#logoProduct"
                      accept="image/png, image/jpeg, image/svg"
                    />
                  </div>
                  <div>
                    <input type="file"/>
                  </div>
                </div>
                <div className="col-md-6">
                  <AvField
                    type="select" name="brandId"
                    label='Brand'
                    // value={currentItem?currentItem.brandDto.nameUz:" "}
                    required>
                    <option value="0" selected>Select Brand</option>
                    {brands && brands.map(item =>
                      <option key={item.id} value={item.id}> {item.nameUz}</option>
                    )}
                  </AvField>
                </div>
                <div className="col-md-6">
                  <AvField
                    type="select" name="categoryId"
                    label='Category'
                    // value={currentItem&& currentItem.categoryDto.nameUz}
                    required>
                    <option value="0" selected>Select Category</option>
                    {categories && categories.map(item =>
                      <option key={item.id} value={item.id}> {item.nameUz}</option>
                    )}
                  </AvField>
                </div>
                <div className="col-md-6">
                  <AvField
                    type="select" name="measurementsId"
                    label='Measure'
                    // defaultValue={currentItem ? (currentItem.companyDto.name) : "0"}
                    required>
                    <option value="0" selected>Select measurement</option>
                    {measurements && measurements.map(item =>
                      <option key={item.id} value={item.id}> {item.nameUz}</option>
                    )}
                  </AvField>
                </div>
                <div className="col-md-6">
                  <AvField
                    type={'text'}
                    label={'Bar Code'}
                    name={'barCode'}
                    value={currentItem && currentItem.barCode}
                    onChange={this.changeState}
                  />
                </div>
                <div className="col-md-6">
                  <AvField
                    type={'text'}
                    label={'Description'}
                    value={currentItem && currentItem.description}
                    name={'description'}
                    onChange={this.changeState}
                  />
                </div>
                <div className="col-md-12">
                  {new Array(this.state.count).fill('').map((key, index) => (
                    <div className='row'>
                      <div className="col-md-6">
                        <AvField
                          type="select"
                          label='Company'
                          name="sdtf"
                          onChange={changeDetail}
                          defaultValue={"0"}
                          required
                        >
                          <option value="0" selected>Select detail</option>
                          {details && details.map(item =>
                            <option key={item.id} value={item.id}> {item.nameUz}</option>
                          )}
                        </AvField>
                      </div>
                      <div className="col-md-5">
                        <AvField
                          type="select" name={"valuesId" + index}
                          label='Values'
                          defaultValue={"0"}
                          required>
                          <option value="0" selected>Select values</option>
                          {values && values.map(item =>
                            <option key={item.id} value={item.id}> {item.nameUz}</option>
                          )}
                        </AvField>
                      </div>
                      <div className="col-md-1 d-flex align-items-center justify-content-end">
                        <button type='button' onClick={this.inc}>+</button>
                        {index !== 0 && <button type='button' onClick={this.dec}>-</button>}
                      </div>
                    </div>
                  ))}
                </div>
                <div className="col-md-6">
                  <AvField type="checkbox" name='active'/>
                </div>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" type="button" onClick={openModal}>Cancel</Button>
              <Button color="success">Save</Button>
            </ModalFooter>
          </AvForm>
        </Modal>

        <Modal isOpen={showDeleteModal}>
          <ModalHeader>
            <h3>Are sure delete {currentItem ? currentItem.nameUz : ""}?</h3>
          </ModalHeader>
          <ModalFooter>
            <Button onClick={openDeleteModal}>Cancel</Button>
            <Button onClick={() => deleteProduct(currentItem)}>Delete</Button>
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

export default Product;

// export const TableProduct = ({products}) => {
//   console.log(products)
//     return (
//       <table className='table'>
//         <thead className='table-light'>
//         <tr>
//           <th scope="col">T/R</th>
//           <th scope="col">Logo</th>
{/*          <th scope="col">Name</th>*/
}
{/*          <th scope="col">Company</th>*/
}
{/*          <th scope="col">Brand</th>*/
}
{/*          <th scope="col">Bar Code</th>*/
}
{/*          <th scope="col">Category</th>*/
}
{/*          <th scope="col">Measurement</th>*/
}
{/*          <th scope="col">Detail</th>*/
}
//           <th scope="col">Description</th>
//           <th scope="col">Active</th>
//         </tr>
{/*        </thead>*/
}
{/*        <tbody>*/
}
{/*        {products? products.map((item, index) => (*/
}
{/*          <tr key={item.id}>*/
}
{/*            <td>{index+1}</td>*/
}

//             <td>
//               {item.nameUz}
{/*            </td>*/
}
//             <td>
//               {item.companyDto.name}
//             </td>
//             <td>
//               {item.brandDto.nameUz}
//             </td>
//             <td>
//               {item.barCode}
//             </td>
//             <td>
//               {item.categoryDto.name}
//             </td>
//             <td>
//               {item.measurementDto.nameUz}
//             </td>
//             <td>
//               {item.valuesDto.name}
//             </td>
//             <td>
//               {item.description}
//             </td>
//             <td>
//               {item.active}
//             </td>
//           </tr>
//         )):"No information"}
//         </tbody>
//       </table>
//     )
// }

export const ModalProduct = ({changeState, count, inc, dec}) => {
  return (
    <div className='modalProduct'>
      <div className='titleModal'>Add/Edit</div>

    </div>
  )
}
