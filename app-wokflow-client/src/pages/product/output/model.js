import api from 'api'

const {outputProduct} = api;
export default ({
  namespace: 'outputProduct',
  state: {
    loading: false
  },

  subscriptions: {},

  effects: {},

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
