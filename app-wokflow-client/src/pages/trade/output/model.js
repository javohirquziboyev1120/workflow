import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";

const {outputTrade} = api;
export default ({
  namespace: 'outputTrade',
  state: {
    loading: false
  },

  subscriptions: {},

  effects: {},

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
