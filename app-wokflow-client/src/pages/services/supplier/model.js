import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";
import {toast} from "react-toastify";

const {addSupplier, getSupplier, deleteSupplier, editSupplier} = api;
export default ({
  namespace: 'supplier',
  state: {
    loading: false,
    suppliers: [],
    currentItem: '',
    showModal: false,
    showDeleteModal: false
  },
  subscriptions: {},

  effects: {
    * addSupplier({payload}, {call, put, select}) {
      console.log(payload)
      const res = yield call(addSupplier, payload);
      if (res.success) {
        toast.success("Supplier saqlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getSuppliers'
        })
      }
    },
    * editSupplier({payload}, {call, put, select}) {
      const res = yield call(editSupplier, {path: payload.id, ...payload});
      if (res.success) {
        toast.success("Supplier tahrirlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getSuppliers'
        })
      }
    },
    * getSuppliers({}, {call, put}) {
      let res = yield call(getSupplier);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            suppliers: res._embedded.list
          }
        });
      }
    },
    * deleteSupplier({payload}, {call, put, select}) {
      let res = yield call(deleteSupplier, payload);
      if (res.success) {
        toast.success("Supplier o'chirildi");
        yield put({
          type: 'updateState',
          payload: {
            showDeleteModal: false
          }
        });
        yield put({
          type: 'getSuppliers'
        })
      }
    },
  },

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
