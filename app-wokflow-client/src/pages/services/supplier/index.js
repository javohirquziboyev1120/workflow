import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {AddIcon, DeleteIcon, EditIcon} from '../../../utils/incon'

@connect(({supplier}) => ({supplier}))
class Supplier extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'supplier/getSuppliers'
    })
  }

  // changeState = (e) => {
  //   this.props.dispatch({
  //     type: 'supplier/updateState',
  //     payload: {
  //       [e.target.name]: e.target.value
  //     }
  //   })
  // }

  render() {

    const {dispatch, supplier} = this.props;
    const {loading, suppliers, showModal, currentItem, showDeleteModal} = supplier;

    const handleSubmit = (e, v) => {
      if (!currentItem) {
        e.preventDefault()
        dispatch({
          type: 'supplier/addSupplier',
          payload: {
            ...v,
          }
        })
      } else {
        e.preventDefault()
        dispatch({
          type: 'supplier/editSupplier',
          payload: {
            id: currentItem.id,
            ...v,
          }
        })
      }
    };

    const openModal = (item) => {
      dispatch({
        type: 'supplier/updateState',
        payload: {
          showModal: !showModal,
          currentItem: item
        }
      })
    };
    const openDeleteModal = (item) => {
      dispatch({
        type: 'supplier/updateState',
        payload: {
          showDeleteModal: !showDeleteModal,
          currentItem: item
        }
      })
    };
    const deleteSupplier = (v) => {
      dispatch({
        type: 'supplier/deleteSupplier',
        payload: {
          ...v
        }
      })
    };
    return (
      <div>
        <Container>
          <Row>
            <h1>Supplier list</h1>
          </Row>
          <AddIcon
            onClick={() => openModal('')}
          />
          <Table className="custom-table">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>PhoneNumber</th>
              <th>Address</th>
              <th>Status</th>
              <th>Company</th>
              <th colSpan="2">Operation</th>
            </tr>
            </thead>
            {suppliers ?
              <tbody>
              {suppliers.map((item, i) =>
                <tr key={item.id}>
                  <td>{i + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>{item.phoneNumber}</td>
                  <td>{item.address}</td>
                  <td>{item.status}</td>
                  <td>
                    <EditIcon
                      onClick={() => openModal(item)}
                    />
                  </td>
                  <td>
                    <DeleteIcon
                      onClick={() =>
                        openDeleteModal(item)
                      }
                    />
                  </td>
                </tr>
              )}
              </tbody> :
              <tbody>
              <tr>
                <td colSpan="4">
                  <h3 className="text-center mx-auto"> No information </h3>
                </td>
              </tr>
              </tbody>
            }
          </Table>

        </Container>
        <Modal isOpen={showModal}>
          <AvForm onValidSubmit={handleSubmit}>
            <ModalHeader>
              Add Supplier
            </ModalHeader>
            <ModalBody>
              <AvField defaultValue={currentItem ? currentItem.name : ""}
                       name="name" label="Supplier name" placeholder="Enter supplier name" required/>
              <AvField defaultValue={currentItem ? currentItem.email : ""}
                       name="email" label="Supplier email" placeholder="Enter supplier email " required/>
              <AvField defaultValue={currentItem ? currentItem.phoneNumber : ""}
                       name="phoneNumber" label="Supplier phoneNumber" placeholder="Enter supplier phoneNumber " required/>
              <AvField defaultValue={currentItem ? currentItem.address : ""}
                       name="address" label="Supplier address" placeholder="Enter supplier address " required/>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" type="button" onClick={openModal}>Cancel</Button>
              <Button color="success">Save</Button>
            </ModalFooter>
          </AvForm>
        </Modal>

        <Modal isOpen={showDeleteModal}>
          <ModalHeader>
            <h3>Are sure delete {currentItem ? currentItem.nameUz : ""}?</h3>
          </ModalHeader>
          <ModalFooter>
            <Button onClick={openDeleteModal}>Cancel</Button>
            <Button onClick={() => deleteSupplier(currentItem)}>Delete</Button>
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

Supplier.propTypes = {};

export default Supplier;
