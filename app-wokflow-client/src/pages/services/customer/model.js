import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";
import {router} from "umi";
import {toast} from "react-toastify";

const {addCustomer, getCustomer, deleteCustomer, editCustomer} = api;
export default ({
  namespace: 'customer',
  state: {
    loading: false,
    customers: [],
    currentItem: '',
    showModal: false,
    showDeleteModal: false
  },
  subscriptions: {},

  effects: {
    * addCustomer({payload}, {call, put, select}) {
      console.log(payload)
      const res = yield call(addCustomer, payload);
      if (res.success) {
        toast.success("Customer saqlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getCustomers'
        })
      }
    },
    * editCustomer({payload}, {call, put, select}) {
      const res = yield call(editCustomer, {path: payload.id, ...payload});
      if (res.success) {
        toast.success("Customer tahrirlandi");
        yield put({
          type: 'updateState',
          payload: {
            loading: false,
            showModal: false
          }
        })
        yield put({
          type: 'getCustomers'
        })
      }
    },
    * getCustomers({}, {call, put}) {
      let res = yield call(getCustomer);
      if (res.statusCode === 200) {
        yield put({
          type: 'updateState',
          payload: {
            customers: res._embedded.list
          }
        });
      }
    },
    * deleteCustomer({payload}, {call, put, select}) {
      let res = yield call(deleteCustomer, payload);
      if (res.success) {
        toast.success("Customer o'chirildi");
        yield put({
          type: 'updateState',
          payload: {
            showDeleteModal: false
          }
        });
        yield put({
          type: 'getCustomers'
        })
      }
    },
  },

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
