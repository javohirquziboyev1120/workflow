import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Container, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {AddIcon, DeleteIcon, EditIcon} from '../../../utils/incon'

@connect(({customer}) => ({customer}))
class Customer extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'customer/getCustomers'
    })
  }

  // changeState = (e) => {
  //   this.props.dispatch({
  //     type: 'customer/updateState',
  //     payload: {
  //       [e.target.name]: e.target.value
  //     }
  //   })
  // }

  render() {

    const {dispatch, customer} = this.props;
    const {loading, customers, showModal, currentItem, showDeleteModal} = customer;

    const handleSubmit = (e, v) => {
      if (!currentItem) {
        e.preventDefault()
        dispatch({
          type: 'customer/addCustomer',
          payload: {
            ...v,
          }
        })
      } else {
        e.preventDefault()
        dispatch({
          type: 'customer/editCustomer',
          payload: {
            id: currentItem.id,
            ...v,
          }
        })
      }
    };

    const openModal = (item) => {
      dispatch({
        type: 'customer/updateState',
        payload: {
          showModal: !showModal,
          currentItem: item
        }
      })
    };
    const openDeleteModal = (item) => {
      dispatch({
        type: 'customer/updateState',
        payload: {
          showDeleteModal: !showDeleteModal,
          currentItem: item
        }
      })
    };
    const deleteCustomer = (v) => {
      dispatch({
        type: 'customer/deleteCustomer',
        payload: {
          ...v
        }
      })
    };
    return (
      <div>
        <Container>
          <Row>
            <h1>Customer list</h1>
          </Row>
          <AddIcon
            onClick={() => openModal('')}
          />
          <Table className="custom-table">
            <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>PhoneNumber</th>
              <th>Address</th>
              <th>Status</th>
              <th>Company</th>
              <th colSpan="2">Operation</th>
            </tr>
            </thead>
            {customers ?
              <tbody>
              {customers.map((item, i) =>
                <tr key={item.id}>
                  <td>{i + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.email}</td>
                  <td>{item.phoneNumber}</td>
                  <td>{item.address}</td>
                  <td>{item.status}</td>
                  <td>
                    <EditIcon
                      onClick={() => openModal(item)}
                    />
                  </td>
                  <td>
                    <DeleteIcon
                      onClick={() =>
                        openDeleteModal(item)
                      }
                    />
                  </td>
                </tr>
              )}
              </tbody> :
              <tbody>
              <tr>
                <td colSpan="4">
                  <h3 className="text-center mx-auto"> No information </h3>
                </td>
              </tr>
              </tbody>
            }
          </Table>

        </Container>
        <Modal isOpen={showModal}>
          <AvForm onValidSubmit={handleSubmit}>
            <ModalHeader>
              Add Customer
            </ModalHeader>
            <ModalBody>
              <AvField defaultValue={currentItem ? currentItem.name : ""}
                       name="name" label="Customer name" placeholder="Enter customer name" required/>
              <AvField defaultValue={currentItem ? currentItem.email : ""}
                       name="email" label="Customer email" placeholder="Enter customer email " required/>
              <AvField defaultValue={currentItem ? currentItem.phoneNumber : ""}
                       name="phoneNumber" label="Customer phoneNumber" placeholder="Enter customer phoneNumber "
                       required/>
              <AvField defaultValue={currentItem ? currentItem.address : ""}
                       name="address" label="Customer address" placeholder="Enter customer address " required/>
            </ModalBody>
            <ModalFooter>
              <Button color="danger" type="button" onClick={openModal}>Cancel</Button>
              <Button color="success">Save</Button>
            </ModalFooter>
          </AvForm>
        </Modal>

        <Modal isOpen={showDeleteModal}>
          <ModalHeader>
            <h3>Are sure delete {currentItem ? currentItem.nameUz : ""}?</h3>
          </ModalHeader>
          <ModalFooter>
            <Button onClick={openDeleteModal}>Cancel</Button>
            <Button onClick={() => deleteCustomer(currentItem)}>Delete</Button>
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

Customer.propTypes = {};

export default Customer;
