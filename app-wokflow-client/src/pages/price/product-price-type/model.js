import api from 'api'
import {STORAGE_NAME} from "../../../utils/constant";

const {productPriceType} = api;
export default ({
  namespace: 'productPriceType',
  state: {
    loading: false
  },

  subscriptions: {},

  effects: {},

  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
