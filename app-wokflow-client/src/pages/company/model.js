import api from 'api'

const {addCompany, getCompany, deleteCompany} = api;
export default ({
  namespace: 'companyModel',
  state: {
    loading: false,
    isModalShow: false,
    currentUser:{}
  },
  subscriptions: {},
  effects: {
    * addCompany({payload}, {call, put, select}) {
      const res = yield call(addCompany, payload);
      if (res.success) {
        yield put({
          type: "upadateState",
          payload: {
            isModalShow: false
          }
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
