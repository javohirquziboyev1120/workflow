import React, {Component} from 'react';
import {connect} from "react-redux";
import CompanyModal from '../../components/CompanyModal/index'
@connect(({companyModel}) => ({companyModel}))
class Company extends Component {
  render() {
    const {dispatch, companyModel} = this.props;
    const {isModalShow, currentUser} = companyModel;

    const openModal = () => {
      dispatch({
        type: 'users/updateState',
        payload: {
          isModalShow: !isModalShow,
          currentUser: {}
        }
      })
    };

    const addCompany = (e, v) => {
      if (currentUser.id != null) {
        dispatch({
          type: 'companyModel/addCompany',
          payload: {
            id: currentUser.id,
            ...v
          }
        })
      }else {
        dispatch({
          type: 'companyModel/addCompany',
          payload: {
            ...v
          }
        })
      }
    }
    return (
      <div>

{/*
        <CompanyModal isModalShow={isModalShow}
                   openModal={openModal}
                   addCompany={addCompany}
                   currentUser={currentUser}
        />*/}
      </div>
    );
  }
}

Company.propTypes = {};

export default Company;
