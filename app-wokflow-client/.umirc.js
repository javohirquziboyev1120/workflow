// ref: https://umijs.org/config/
import {resolve} from 'path'

export default {
  // chunks: ['vendors', 'umi'],
  // chainWebpack: function (config, { webpack }) {
  //   config.merge({
  //     optimization: {
  //       minimize: true,
  //       splitChunks: {
  //         chunks: 'all',
  //         minSize: 30000,
  //         minChunks: 3,
  //         automaticNameDelimiter: '.',
  //         cacheGroups: {
  //           vendor: {
  //             name: 'vendors',
  //             priority: 10,
  //           },
  //         },
  //       },
  //     }
  //   });
  // },
  // hash:true,
  treeShaking: true,
  // routes: [
  //   {
  //     path: '/',
  //     component: '../layouts/index',
  //     routes: [
  //       {path: '/', component: '../pages/index'}
  //     ]
  //   }
  // ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react', {
      antd: false,
      dva: true,
      dynamicImport: {webpackChunkName: true},
      title: 'app-wokflow-client',
      // dll: true,
      // locale: {
      //   enable:true,
      //   default: 'en_US'
      // },
      routes: {
        exclude: [
          /models\//,
          /services\//,
          /model\.(t|j)sx?$/,
          /service\.(t|j)sx?$/,
          /components\//,
        ],
      },
    }],
  ],
  proxy: {
    "/api": {
      "target": "http://localhost/",
      "changeOrigin": true
    }
  },
  alias: {
    api: resolve(__dirname, './src/services/'),
    utils: resolve(__dirname, "./src/utils"),
    components: resolve(__dirname, "./src/components"),
    config: resolve(__dirname, "./src/utils/config"),
  },
  // outputPath:'../app-mfaktor-server/src/main/resources/static'
}
