package ai.ecma.workflow.utils;

import org.springframework.data.domain.Sort;

import java.sql.Timestamp;

public interface AppConstants {
    String DEFAULT_PAGE_NUMBER = "0";
    String DEFAULT_PAGE_SIZE = "10";
    int MAX_PAGE_SIZE = 20;
    String DEFAULT_BEGIN_DATE = "2019-04-20 11:39:47.136000";
    String DEFAULT_END_DATE = "2100-04-20 11:39:47.136000";
    String DEFAULT_ORDER_BEGIN_DATE = String.valueOf(new Timestamp(System.currentTimeMillis()));
    //    String DEFAULT_ORDER_BEGIN_DATE = DEFAULT_ORDER_BEGIN_DATE1;
    String DEFAULT_ORDER_END_DATE = new Timestamp(System.currentTimeMillis() + 86400000).toString();
    String COLUMN_NAME_FOR_SORT = "createdAt";
    Sort.Direction ASC_OR_DECK = Sort.Direction.ASC;
    String ERROR_UZ = "Xatolik";
    String ERROR_RU = "Ошибка";
}
