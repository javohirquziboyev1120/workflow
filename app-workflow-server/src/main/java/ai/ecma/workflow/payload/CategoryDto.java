package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryDto {

    private UUID id;

    private String nameUz;

    private String nameRu;

    private Integer index;

    private CompanyDto companyDto;

    private CategoryDto categoryDto;

    private boolean active;

    public CategoryDto(UUID id, String nameUz, String nameRu, Integer index, CompanyDto companyDto, CategoryDto categoryDto) {
        this.id=id;
        this.nameUz=nameUz;
        this.nameRu=nameRu;
        this.index=index;
        this.companyDto=companyDto;
        this.categoryDto=categoryDto;
    }
}