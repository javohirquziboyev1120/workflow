package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class CustomerDto {
    private UUID id;
    private String name;
    private String email;
    private String phoneNumber;
    private String address;
    private boolean status;
    private CompanyDto companyDto;
    private UUID companyId;
}
