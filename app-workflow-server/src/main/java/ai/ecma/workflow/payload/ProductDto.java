package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {

    private UUID id;

    private String nameUz;//1

    private String nameRu;//2

    private boolean active;//8

    private String barCode;//3

    private String description;//7

    private UUID brandId;//4

    private BrandDto brandDto;

    private UUID categoryId;//5

    private CategoryDto categoryDto;

    private UUID companyId;//6

    private CompanyDto companyDto;

    private List<UUID> attachmentId = new ArrayList<>();//0

    private List<Attachment> photos = new ArrayList<>();


    private List<ValuesDto> valuesDto ;

    private List<UUID> valuesId = new ArrayList<>();

    private List<MeasurementDto> measurementDto ;

    private List<UUID> measurementsId = new ArrayList<>();

    public ProductDto(UUID id, String nameUz, String nameRu, boolean active,String barCode, String description, BrandDto brand, CategoryDto category, List<Attachment> photos,
                      CompanyDto company, List<ValuesDto> valuesDto, List<MeasurementDto> measurementDto) {
        this.id = id;
        this.nameUz = nameUz;
        this.nameRu = nameRu;
        this.active=active;
        this.barCode = barCode;
        this.description = description;
        this.brandDto = brand;
        this.categoryDto = category;
        this.photos = photos;
        this.companyDto = company;
        this.valuesDto = valuesDto;
        this.measurementDto = measurementDto;
    }
}
