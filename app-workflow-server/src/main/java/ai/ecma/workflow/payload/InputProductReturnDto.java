package ai.ecma.workflow.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputProductReturnDto {
    private UUID id;
    private UUID inputProductId;
    private boolean defect;
    private String description;
    private Timestamp date;
    private List<InputProductReturnAmountDto> inputProductReturnAmountDto;

}
