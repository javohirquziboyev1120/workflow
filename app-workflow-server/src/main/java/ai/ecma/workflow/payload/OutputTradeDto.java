package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class OutputTradeDto {
    private UUID id;
    private Date date = new Date();
    private String description;
    private String number;
    private CustomerDto customerDto;
    private UUID customerId;
    private CurrencyTypeDto currencyTypeDto;
    private UUID currencyTypeId;
}
