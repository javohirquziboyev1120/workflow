package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.Warehouse;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class OutputProductDto {
    private UUID id;
    private String description;
    private Double price;
    private OutputTradeDto outputTradeDto;
    private UUID outputTradeId;
    private Warehouse warehouse;
    private UUID warehouseId;
    private List<OutputProductAmountDto> outputProductAmountDtoList;
    private UUID productId;
}
