package ai.ecma.workflow.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OutputProductReturnAmountDto {
    private UUID id;

    private OutputProductReturnDto outputProductReturnDto;
    private UUID outputProductReturnId;

    private MeasurementDto measurementDto;
    private UUID measurementId;

    private PackagingDto packagingDto;
    private UUID packagingId;

    private double amountPackaging;

    private double amount;

    public OutputProductReturnAmountDto(UUID id, UUID outputProductReturnId, MeasurementDto measurementDto, UUID measurementId, PackagingDto packagingDto, UUID packagingId, double amountPackaging, double amount) {
        this.id = id;
        this.outputProductReturnId = outputProductReturnId;
        this.measurementDto = measurementDto;
        this.measurementId = measurementId;
        this.packagingDto = packagingDto;
        this.packagingId = packagingId;
        this.amountPackaging = amountPackaging;
        this.amount = amount;
    }
}
