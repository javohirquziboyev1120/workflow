package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class OutputProductAmountDto {
    private UUID id;
    private Double amountPackaging;
    private Double amount;
    private String description;
    private OutputProductDto outputProductDto;
    private UUID outputProductId;
    private PackagingDto packagingDto;
    private UUID packagingId;
    private MeasurementDto measurementDto;
    private UUID measurementId;
}
