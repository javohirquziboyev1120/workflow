package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ProductPriceTypeDto {
    private UUID id;
    private Double price;
    private ProductDto productDto;
    private UUID productId;
    private MeasurementDto measurementDto;
    private UUID measurementId;
    private PriceTypeDto priceTypeDto;
    private UUID priceTypeId;
}
