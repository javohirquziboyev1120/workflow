package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputProductDto {
    private UUID id;

    @NotNull
    private UUID productId;
    private Double price;
    private String productNameUz;
    private String productNameRu;
    private String productBarcode;

    @NotNull
    private UUID priceMeasurementId;
    private String priceMeasurementNameUz;
    private String priceMeasurementNameRu;

    private String inputProductDescription;
    private List<InputProductAmountDto> inputProductAmountDtoList;

    private UUID inputTradeId;
    private String inputTradeNumber;
    private String inputTradeFactureNumber;

    private UUID warehouseId;
    private String warehouseName;

    public InputProductDto(UUID id, UUID productId, Double price, String productNameUz, String productNameRu, String productBarcode, UUID priceMeasurementId, String priceMeasurementNameUz, String priceMeasurementNameRu, String inputProductDescription,UUID inputTradeId, String inputTradeNumber, String inputTradeFactureNumber, UUID warehouseId, String warehouseName) {
        this.id = id;
        this.productId = productId;
        this.price = price;
        this.productNameUz = productNameUz;
        this.productNameRu = productNameRu;
        this.productBarcode = productBarcode;
        this.priceMeasurementId = priceMeasurementId;
        this.priceMeasurementNameUz = priceMeasurementNameUz;
        this.priceMeasurementNameRu = priceMeasurementNameRu;
        this.inputProductDescription = inputProductDescription;
        this.inputTradeId = inputTradeId;
        this.inputTradeNumber = inputTradeNumber;
        this.inputTradeFactureNumber = inputTradeFactureNumber;
        this.warehouseId = warehouseId;
        this.warehouseName = warehouseName;
    }

    public InputProductDto(UUID productId, Double price, UUID priceMeasurementId, UUID warehouseId, String inputProductDescription, List<InputProductAmountDto> inputProductAmountDtoList) {
        this.productId = productId;
        this.price = price;
        this.priceMeasurementId = priceMeasurementId;
        this.warehouseId = warehouseId;
        this.inputProductDescription = inputProductDescription;
        this.inputProductAmountDtoList = inputProductAmountDtoList;
    }
}
