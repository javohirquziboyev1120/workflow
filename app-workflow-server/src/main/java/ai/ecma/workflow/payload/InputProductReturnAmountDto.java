package ai.ecma.workflow.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputProductReturnAmountDto {
    private UUID id;
    private UUID measurementId;
    private UUID packagingId;
    private double amountPackaging;
    private double amount;

    private UUID inputProductReturnId;
}
