package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class CurrencyTypeDto {
    private UUID id;
    private CompanyDto companyDto;
    private UUID companyId;
    private String nameUz;
    private String nameRu;
    private boolean active;
    private String description;
}
