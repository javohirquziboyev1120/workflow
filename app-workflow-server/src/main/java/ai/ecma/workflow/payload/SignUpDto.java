package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignUpDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private String prePassword;
    private String companyName;
    private String companyPhoneNumber;
    private String companyEmail;
    private Float lan;
    private Float lat;
    private String street;
    private Integer districtId;
}
