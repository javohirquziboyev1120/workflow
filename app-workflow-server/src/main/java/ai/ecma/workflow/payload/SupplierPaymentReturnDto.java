package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SupplierPaymentReturnDto {
    private UUID id;
    private UUID inputTradeId;
    private UUID payTypeId;
    private Double amount;
    private Double haqiqatda;
    private Date date;
    private String description;
}
