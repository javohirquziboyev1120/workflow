package ai.ecma.workflow.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    private String message;
    private boolean success;
    private Object object;


    public ApiResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public ApiResponse(boolean success) {
        this.success = success;
    }

    public ApiResponse(boolean success, Object object) {
        this.success = success;
        this.object = object;
    }

    public ApiResponse(Object object) {
        this.object = object;
    }

    public ApiResponse(String s, String message, boolean success,Object object) {
        this.message = message;
        this.message = s;
        this.success = success;
        this.object = object;
    }

    public ApiResponse(String s, String message,Object object) {
        this.message = message;
        this.message = s;
        this.object = object;
    }


}
