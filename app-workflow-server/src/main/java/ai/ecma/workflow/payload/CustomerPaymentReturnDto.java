package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.CashBox;
import ai.ecma.workflow.entity.OutputTrade;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerPaymentReturnDto {

    private UUID id;

    private UUID outputTradeId;

    private OutputTrade outputTrade;

    private  UUID cashBoxId;

    private CashBox cashBox;

    private Double amount;

    private Date date;

    private String description;

    public CustomerPaymentReturnDto(UUID id, OutputTrade outputTrade, CashBox cashBox, Double amount, Date date, String description) {
        this.id=id;
        this.outputTrade=outputTrade;
        this.cashBox=cashBox;
        this.amount=amount;
        this.date=date;
        this.description=description;
    }
}
