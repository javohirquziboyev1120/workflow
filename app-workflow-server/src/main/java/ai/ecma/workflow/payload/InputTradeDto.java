package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.CurrencyType;
import ai.ecma.workflow.entity.Supplier;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputTradeDto {

    private UUID id;
    @NotNull
    private UUID supplierId;
    private String supplierName;
    @NotNull
    private UUID currencyTypeId;
    private String currencyTypeNameUz;
    private String currencyTypeNameRu;
    @NotNull
    private UUID warehouseId;
    private String number;
    private String factureNumber;
    private Timestamp date;
    private String inputTradeDescription;
    private List<InputProductDto> inputProductDtoList;

    public InputTradeDto(UUID id, UUID supplierId, UUID currencyTypeId, String number, String factureNumber, Timestamp date, String inputTradeDescription) {
        this.id = id;
        this.supplierId = supplierId;
        this.currencyTypeId = currencyTypeId;
        this.number = number;
        this.factureNumber = factureNumber;
        this.date = date;
        this.inputTradeDescription = inputTradeDescription;
    }

    public InputTradeDto(UUID id, UUID supplierId, String supplierName, UUID currencyTypeId, String currencyTypeNameUz, String currencyTypeNameRu, String number, String factureNumber, Timestamp date, String inputTradeDescription) {
        this.id = id;
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.currencyTypeId = currencyTypeId;
        this.currencyTypeNameUz = currencyTypeNameUz;
        this.currencyTypeNameRu = currencyTypeNameRu;
        this.number = number;
        this.factureNumber = factureNumber;
        this.date = date;
        this.inputTradeDescription = inputTradeDescription;
    }

    public InputTradeDto(UUID id, UUID supplierId, UUID currencyTypeId, UUID warehouseId, String number, String factureNumber, Timestamp date, String inputTradeDescription, List<InputProductDto> inputProductDtoList) {
        this.id = id;
        this.supplierId = supplierId;
        this.currencyTypeId = currencyTypeId;
        this.warehouseId = warehouseId;
        this.number = number;
        this.factureNumber = factureNumber;
        this.date = date;
        this.inputTradeDescription = inputTradeDescription;
        this.inputProductDtoList = inputProductDtoList;
    }
}
