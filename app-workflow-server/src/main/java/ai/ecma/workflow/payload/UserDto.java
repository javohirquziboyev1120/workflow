package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private List<SystemRoleNameEnum> systemRoleNameEnums;
    private List<String> companiesName;
    private UUID companyRoleId;

    public UserDto(UUID id, String firstName, String lastName, String email, String phoneNumber, String password, List<SystemRoleNameEnum> systemRoleNameEnums, List<String> companiesName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.systemRoleNameEnums = systemRoleNameEnums;
        this.companiesName = companiesName;
    }
}
