package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.OutputTrade;
import ai.ecma.workflow.entity.PayType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerPaymentDto {

    private UUID id;

    private OutputTrade outputTrade;

    private UUID outputTradeId;

    private PayType payType;

    private UUID payTypeId;

    private Double amount;

    private Double inFactAmount;

    private Timestamp date;

    private String description;

    public CustomerPaymentDto(UUID id, OutputTrade outputTrade, PayType payType, Double amount, Double inFactAmount, Timestamp date, String description) {
    this.id=id;
    this.outputTrade=outputTrade;
    this.payType=payType;
    this.amount=amount;
    this.inFactAmount=inFactAmount;
    this.date=date;
    this.description=description;

    }
}
