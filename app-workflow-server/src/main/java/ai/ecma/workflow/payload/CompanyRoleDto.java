package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.CompanyPermission;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyRoleDto {
    private UUID id;
    private String name;
    private String description;
    private UUID companyId;
    private String companyName;
    private List<UUID> companyPermissionsId;

    public CompanyRoleDto(UUID id, String name, String description, UUID companyId, String companyName, List<CompanyPermission> companyPermissions) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyPermissions = companyPermissions;
    }

    private List<CompanyPermission> companyPermissions;
}
