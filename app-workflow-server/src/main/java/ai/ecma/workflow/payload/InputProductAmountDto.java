package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InputProductAmountDto {
    private UUID measurementId;
    private UUID packagingId;
    private double amountPackaging;
    private double amount;
}
