package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.Measurement;
import ai.ecma.workflow.entity.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PackagingDto {

    private UUID id;

    private String nameUz;

    private String nameRu;

    private boolean active;

    private UUID productId;

    private ProductDto productDto;

    private UUID measurementId;

    private MeasurementDto measurementDto;

    private Double value;

    private String description;

    public PackagingDto(UUID id, String nameUz, String nameRu, boolean active, ProductDto product, MeasurementDto measurement, Double value, String description) {
        this.id=id;
        this.nameUz=nameUz;
        this.nameRu=nameRu;
        this.active=active;
        this.productDto=product;
        this.measurementDto=measurement;
        this.value=value;
        this.description=description;
    }
}
