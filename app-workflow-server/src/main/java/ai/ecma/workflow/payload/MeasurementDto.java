package ai.ecma.workflow.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeasurementDto {
    private UUID id;

    private String nameUz;

    private String nameRu;

    private boolean active;
}
