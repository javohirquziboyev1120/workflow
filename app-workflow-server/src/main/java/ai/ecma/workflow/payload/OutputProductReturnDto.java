package ai.ecma.workflow.payload;

import ai.ecma.workflow.entity.OutputProductReturnAmount;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class OutputProductReturnDto {
    private UUID id;
    private String description;
    private boolean defect;
    private OutputProductDto outputProductDto;
    private UUID outputProductId;
    private List<OutputProductReturnAmountDto> outputProductReturnAmountDtos;
}
