package ai.ecma.workflow.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyDto {
    private UUID id;
    private String name;
//    private ContactDto contactDto;
    private String phoneNumber;
    private String email;
    private Float lan;
    private Float lat;
    private String street;
    private Integer districtId;
    private String districtNameUz;
    private String districtNameRu;
    private Integer regionId;
    private String regionNameUz;
    private String regionNameRu;

    public CompanyDto(UUID id, String name, String phoneNumber, String email, Float lan, Float lat, String street, Integer districtId) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.lan = lan;
        this.lat = lat;
        this.street = street;
        this.districtId = districtId;
    }
}
