package ai.ecma.workflow.security;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.entity.enums.CompanyPermissionEnum;
import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.SignInDto;
import ai.ecma.workflow.payload.SignUpDto;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.repository.CompanyRoleRepository;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.repository.WarehouseRepository;
import ai.ecma.workflow.repository.dataRest.CompanyPermissionRepository;
import ai.ecma.workflow.repository.dataRest.DistrictRepository;
import ai.ecma.workflow.repository.dataRest.SystemRoleRepository;
import ai.ecma.workflow.service.CompanyService;
import ai.ecma.workflow.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    SystemRoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    MessageSource messageSource;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    CompanyService companyService;
    @Autowired
    CompanyRoleRepository companyRoleRepository;
    @Autowired
    MessageLanguage messageLanguage;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ContactService contactService;
    @Autowired
    CompanyPermissionRepository companyPermissionRepository;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Autowired
    AuthenticationManager authenticate;
    @Autowired
    DistrictRepository districtRepository;


    public ApiResponse register(SignUpDto dto) {
        Optional<User> optionalUser = userRepository.findByPhoneNumber(dto.getPhoneNumber());
        if (optionalUser.isPresent()) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("auth.notfound"), false);
        } else {
            Company company = new Company();
            company.setName(dto.getCompanyName());
            Contact contact = new Contact();
            contact.setPhoneNumber(dto.getCompanyPhoneNumber());
            contact.setEmail(dto.getCompanyEmail());
            contact.setLat(dto.getLat());
            contact.setLan(dto.getLan());
            contact.setStreet(dto.getStreet());
            contact.setDistrict(dto.getDistrictId()!=null?districtRepository.getOne(dto.getDistrictId()):null);
            company.setContact(contact);

            List<CompanyPermission> companyPermissions = new ArrayList<>();
            for (CompanyPermissionEnum permissionName : CompanyPermissionEnum.values()) {
                companyPermissions.add(companyPermissionRepository.save(new CompanyPermission(permissionName)));
            }
            CompanyRole companyRole = new CompanyRole();
            companyRole.setName("ROLE_DIRECTOR");
            companyRole.setCompanyPermissions(companyPermissions);

            User user = new User();
            user.setFirstName(dto.getFirstName());
            user.setLastName(dto.getLastName());
            user.setEmail(dto.getEmail());
            user.setPhoneNumber(dto.getPhoneNumber());
            if (dto.getPassword().equals(dto.getPrePassword())) {
                user.setPassword(passwordEncoder.encode(dto.getPassword()));
            } else {
                return new ApiResponse(messageLanguage.getMessageByLanguage("auth.pre.phone.number.error"), false);
            }
            user.setSystemRoles(roleRepository.findAllBySystemRoleNameEnumIn(
                    Collections.singletonList(SystemRoleNameEnum.ROLE_ADMIN)));

            CompanyRole savedCompanyRole = companyRoleRepository.save(companyRole);
            user.setCompanyRole(savedCompanyRole);

            Company saveCompany = companyRepository.save(company);
            List<Company> companies = new ArrayList<>();
            companies.add(saveCompany);

            user.setCompanies(companies);
            System.out.println(user);
            userRepository.save(user);
            return new ApiResponse(messageLanguage.getMessageByLanguage("auth.register"), true, getApiToken(user));
        }

    }

    public ApiResponse login(SignInDto signInDto) {
        try {
            Authentication authentication = authenticate.authenticate(new UsernamePasswordAuthenticationToken(signInDto.getPhoneNumber(), signInDto.getPassword()));
            return new ApiResponse(messageLanguage.getMessageByLanguage("success"), true, getApiToken((User) authentication.getPrincipal()));
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    private String getApiToken(User user) {
        return jwtTokenProvider.generateToken(user);
    }

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumberAndEnabledTrue(phoneNumber).orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
    }

    public UserDetails loadUserById(UUID userId) {
        return userRepository.findByIdAndEnabledTrue(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }
}
