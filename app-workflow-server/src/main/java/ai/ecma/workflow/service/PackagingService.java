package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Packaging;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.PackagingDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.repository.PackagingRepository;
import ai.ecma.workflow.repository.ProductRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PackagingService {
    @Autowired
    MeasurementRepository measurementRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    PackagingRepository packagingRepository;
    @Autowired
    MessageLanguage messageLanguage;
    @Autowired
    ProductService productService;
    @Autowired
    ProductPriceTypeService productPriceTypeService;

    public ApiResponse addAndEditPackaging(PackagingDto dto){
        try{
            if (dto.getId()!=null){
                Optional<Packaging> optionalPackaging = packagingRepository.findByIdAndProductId(dto.getId(), dto.getProductId());
                if (optionalPackaging.isPresent()) {
                    Packaging packaging = optionalPackaging.get();
                    packagingRepository.save(makePackaging(dto,packaging));
                    return new ApiResponse(messageLanguage.getMessageByLanguage("packaging.edit"), true);
                }
                return new ApiResponse(messageLanguage.getMessageByLanguage("packaging.notfound"), false);
            }
            packagingRepository.save(makePackaging(dto, new Packaging()));
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        }catch (Exception e){
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deletePackaging(UUID id) {
        try {
            packagingRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public PackagingDto getPackagingDto(Packaging packaging){
        return new PackagingDto(
                packaging.getId(),
                packaging.getNameUz(),
                packaging.getNameRu(),
                packaging.isActive(),
                productService.getProductDto(packaging.getProduct()),
                productPriceTypeService.getMeasurementDto( packaging.getMeasurement()),
                packaging.getValue(),
                packaging.getDescription()
        );
    }


    public PageableDto getPackagingPage(UUID productId, int page, int size){
        Page<Packaging> packagingPage = packagingRepository.findByProductId(productId,(PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT))));
        return new PageableDto(page, size, packagingPage.getTotalPages(), packagingPage.getTotalElements(),
                packagingPage.getContent().stream().map(this::getPackagingDto).collect(Collectors.toList()));
    }

    public Packaging makePackaging(PackagingDto dto, Packaging packaging){
        packaging.setNameUz(dto.getNameUz());
        packaging.setNameRu(dto.getNameRu());
        packaging.setActive(dto.isActive());
        packaging.setProduct(productRepository.findById(dto.getProductId()).orElseThrow(() -> new ResourceNotFoundException("GetProduct")));
        packaging.setMeasurement(measurementRepository.findById(dto.getMeasurementId()).orElseThrow(() -> new ResourceNotFoundException("getMeasurement")));
        packaging.setValue(dto.getValue());
        packaging.setDescription(dto.getDescription());
        return packaging;
    }
}
