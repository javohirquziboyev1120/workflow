package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.payload.*;
import ai.ecma.workflow.repository.*;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OutputProductService {
    @Autowired
    OutputProductRepository outputProductRepository;

    @Autowired
    OutputTradeRepository outputTradeRepository;

    @Autowired
    InputProductRepository inputProductRepository;

    @Autowired
    InputProductAmountRepository inputProductAmountRepository;

    @Autowired
    ProductPriceTypeService productPriceTypeService;

    @Autowired
    MeasurementRepository measurementRepository;

    @Autowired
    PackagingRepository packagingRepository;

    @Autowired
    OutputTradeService outputTradeService;

    @Autowired
    PackagingService packagingService;

    @Autowired
    WarehouseRepository warehouseRepository;

    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse saveOutputProduct(OutputProductDto outputProductDto) {
        try {
            OutputProduct outputProduct = outputProductRepository.findById(outputProductDto.getId()).orElseGet(OutputProduct::new);
            outputProduct.setDescription(outputProductDto.getDescription());
            outputProduct.setPrice(outputProductDto.getPrice());

            Optional<Warehouse> optionalWarehouse = warehouseRepository.findById(outputProductDto.getWarehouseId());
            if (optionalWarehouse.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("warehouse.not.found"), false);
            outputProduct.setWarehouse(optionalWarehouse.get());

            Optional<OutputTrade> optionalOutputTrade = outputTradeRepository.findById(outputProductDto.getOutputTradeId());
            if (optionalOutputTrade.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("trade.notfound"), false);
            outputProduct.setOutputTrade(optionalOutputTrade.get());

            OutputProduct savedOutputProduct = outputProductRepository.save(outputProduct);

            List<OutputProductAmount> outputProductAmountList = new ArrayList<>();

            for (OutputProductAmountDto outputProductAmountDto : outputProductDto.getOutputProductAmountDtoList()) {
                OutputProductAmount outputProductAmount = makeOutputProductAmount(outputProductAmountDto, savedOutputProduct);

                List<InputProductAmount> inputProductAmountList = inputProductAmountRepository.getAllInputProduct(outputProductDto.getProductId(), outputProductAmount.getMeasurement().getId(), outputProductAmount.getPackaging().getId(), outputProductAmount.getAmountPackaging());
                if (inputProductAmountList.isEmpty())
                    return new ApiResponse(messageLanguage.getMessageByLanguage("product.not.enough"), false);

                for (InputProductAmount inputProductAmount : inputProductAmountList) {
                    Double outputAmount = outputProductAmount.getAmount();
                    Double outputAmountPackaging = outputProductAmount.getAmountPackaging();

                    double inputAmount = inputProductAmount.getAmount();
                    double inputAmountPackaging = inputProductAmount.getAmountPackaging();

                    inputProductAmount.setAmountPackaging(inputAmountPackaging - outputAmountPackaging);

                    if (inputAmount >= outputAmount) {
                        inputProductAmount.setAmount(inputAmount - outputAmount);
                    } else {
                        Double evryPackPcs = inputProductAmount.getPackaging().getValue(); // Har bir pachkadagi maxsulot soni
                        double residue = outputAmount / evryPackPcs; // Nechta pachkada bizga kerakli maxsulot chiqadi ?
                        double roundingResidue = Math.floor(residue); // kerakli pachkani yaxlitlash

                        if (roundingResidue > inputAmountPackaging) {
                            return new ApiResponse(messageLanguage.getMessageByLanguage("product.not.enough"), false);

                        } else {
                            inputProductAmount.setAmountPackaging(inputAmountPackaging - roundingResidue);
                            inputProductAmount.setAmount((inputAmount + (roundingResidue * evryPackPcs)) - outputAmount);
                        }
                    }
                    inputProductAmountRepository.save(inputProductAmount);
                }
            }

            savedOutputProduct.setOutputProductAmounts(outputProductAmountList);
            outputProductRepository.save(savedOutputProduct);

            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse getOutputProductPageable(int page, int size, UUID companyId) {
        try {
            List<OutputProductDto> outputProductDtoList = outputProductRepository.getAllByCompanyIdPageable(companyId, page, size).stream().map(this::getOutputProductDto).collect(Collectors.toList());
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.price.type.list"), true, outputProductDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deleteOutputProduct(UUID id) {
        try {
            outputProductRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public OutputProductDto getOutputProductDto(OutputProduct outputProduct) {
//        return new OutputProductDto(
//                outputProduct.getId(),
//                outputTradeService.getOutputTradeDto(outputProduct.getOutputTrade()),
//                outputProduct.getOutputTrade() != null ? outputProduct.getOutputTrade().getId() : null,
////                getInputTradeDto(outputProduct.getInputProduct().getInputTrade()),
////                outputProduct.getInputProduct() != null ? outputProduct.getInputProduct().getId() : null,
////                productPriceTypeService.getMeasurementDto(outputProduct.getMeasurement()),
////                outputProduct.getMeasurement() != null ? outputProduct.getMeasurement().getId() : null,
//                outputProduct.getPrice(),
////                packagingService.getPackagingDto(outputProduct.getPackaging()),
////                outputProduct.getPackaging() != null ? outputProduct.getPackaging().getId() : null,
////                outputProduct.getPackagingAmount(),
////                outputProduct.getAmount(),
//                outputProduct.getDescription()
//        );
        return null;
    }

    public InputProductDto getInputProductDto(InputProduct inputProduct) {
        return new InputProductDto(
                inputProduct.getId(),
                inputProduct.getPrice(),
                inputProduct.getPriceMeasurement() != null ? inputProduct.getPriceMeasurement().getId() : null,
                inputProduct.getWarehouse() != null ? inputProduct.getWarehouse().getId() : null,
                inputProduct.getDescription(),
                inputProduct.getInputProductAmounts().stream().map(this::getInputProductAmountDto).collect(Collectors.toList())
        );
    }

    public InputTradeDto getInputTradeDto(InputTrade inputTrade) {
        return new InputTradeDto(
                inputTrade.getId(),
                inputTrade.getSupplier() != null ? inputTrade.getSupplier().getId() : null,
                inputTrade.getCurrencyType() != null ? inputTrade.getCurrencyType().getId() : null,
                inputTrade.getNumber(),
                inputTrade.getFactureNumber(),
                inputTrade.getDate(),
                inputTrade.getDescription()
        );
    }

    public InputProductAmountDto getInputProductAmountDto(InputProductAmount inputProductAmount) {
        return new InputProductAmountDto(
                inputProductAmount.getMeasurement() != null ? inputProductAmount.getMeasurement().getId() : null,
                inputProductAmount.getPackaging() != null ? inputProductAmount.getPackaging().getId() : null,
                inputProductAmount.getAmountPackaging(),
                inputProductAmount.getAmount()
        );
    }

    private Double calculatePackagingAmount(Double packCapacity, Double packAmount, Double pieceAmount) {
        while (pieceAmount > packCapacity) {
            pieceAmount = -packCapacity;
            packAmount++;
        }
        packAmount *= packCapacity;
        return packAmount + pieceAmount;
    }

    private OutputProductAmount makeOutputProductAmount(OutputProductAmountDto outputProductAmountDto, OutputProduct outputProduct) throws Exception {
        Packaging packaging = packagingRepository.findById(outputProductAmountDto.getPackagingId()).orElseThrow(Exception::new);
        Measurement measurement = measurementRepository.findById(outputProductAmountDto.getMeasurementId()).orElseThrow(Exception::new);
        return new OutputProductAmount(
                outputProduct,
                packaging,
                measurement,
                outputProductAmountDto.getAmountPackaging(),
                outputProductAmountDto.getAmount(),
                outputProductAmountDto.getDescription()
        );
    }
}
