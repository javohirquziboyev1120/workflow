package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.CashBox;
import ai.ecma.workflow.entity.CustomerPaymentReturn;
import ai.ecma.workflow.entity.OutputTrade;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CustomerPaymentReturnDto;
import ai.ecma.workflow.repository.CashBoxRepository;
import ai.ecma.workflow.repository.CustomerPaymentReturnRepository;
import ai.ecma.workflow.repository.OutputTradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerPaymentReturnService {
    @Autowired
    OutputTradeRepository outputTradeRepository;
    @Autowired
    CashBoxRepository cashBoxRepository;
    @Autowired
    CustomerPaymentReturnRepository customerPaymentReturnRepository;
    @Autowired
    MessageLanguage messageLanguage;


    public ApiResponse addAndEditCustomerPaymentReturn(CustomerPaymentReturnDto dto){
        Optional<OutputTrade> outputTradeOptional = outputTradeRepository.findById(dto.getOutputTradeId());
        if(!outputTradeOptional.isPresent()){
            return new ApiResponse(messageLanguage.getMessageByLanguage("trade.notfound"),false);
        }
        Optional<CashBox> optionalCashBox = cashBoxRepository.findById(dto.getCashBoxId());
        if (!optionalCashBox.isPresent()){
            return new ApiResponse(messageLanguage.getMessageByLanguage("pay.type.not.found"), false);
        }
        CashBox cashBox = optionalCashBox.get();
        OutputTrade outputTrade = outputTradeOptional.get();
        if (dto.getId()!=null){
            Optional<CustomerPaymentReturn> returnOptional = customerPaymentReturnRepository.findById(dto.getId());
            if (returnOptional.isPresent()){
                CustomerPaymentReturn customerPaymentReturn = returnOptional.get();
                customerPaymentReturnRepository.save(makeCustomerReturn(dto,customerPaymentReturn,cashBox, outputTrade));
                return new ApiResponse(messageLanguage.getMessageByLanguage("saved"),true);
            }
        }
        customerPaymentReturnRepository.save(makeCustomerReturn(dto,new CustomerPaymentReturn(),cashBox, outputTrade));
        return new ApiResponse(messageLanguage.getMessageByLanguage("saved"),true);
    }

    public CustomerPaymentReturnDto getDto(CustomerPaymentReturn customerPaymentReturn) {
        return new CustomerPaymentReturnDto(
                customerPaymentReturn.getId(),
                customerPaymentReturn.getOutputTrade(),
                customerPaymentReturn.getCashBox(),
                customerPaymentReturn.getAmount(),
                customerPaymentReturn.getDate(),
                customerPaymentReturn.getDescription()
        );
    }

    public ApiResponse deleteCustomerPaymentReturn(UUID id){
        try {
            customerPaymentReturnRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }

    }


    public CustomerPaymentReturn makeCustomerReturn(CustomerPaymentReturnDto dto, CustomerPaymentReturn paymentReturn, CashBox cashBox,OutputTrade outputTrade) {
        paymentReturn.setAmount(dto.getAmount());
        paymentReturn.setCashBox(cashBox);
        paymentReturn.setOutputTrade(outputTrade);
        paymentReturn.setDate(dto.getDate());
        paymentReturn.setDescription(dto.getDescription());
        return paymentReturn;
    }
}
