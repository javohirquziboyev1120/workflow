package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.CurrencyType;
import ai.ecma.workflow.entity.Customer;
import ai.ecma.workflow.entity.OutputTrade;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CurrencyTypeDto;
import ai.ecma.workflow.payload.CustomerDto;
import ai.ecma.workflow.payload.OutputTradeDto;
import ai.ecma.workflow.repository.OutputTradeRepository;
import ai.ecma.workflow.repository.dataRest.CurrencyTypeRepository;
import ai.ecma.workflow.repository.dataRest.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OutputTradeService {
    @Autowired
    OutputTradeRepository outputTradeRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CurrencyTypeRepository currencyTypeRepository;

    @Autowired
    CompanyService companyService;

    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse saveOutputTrade(OutputTradeDto outputTradeDto) {
        try {
            OutputTrade outputTrade = outputTradeRepository.findById(outputTradeDto.getId()).orElseGet(OutputTrade::new);

            Optional<Customer> optionalCustomer = customerRepository.findById(outputTradeDto.getCustomerId());
            if (optionalCustomer.isPresent()) outputTrade.setCustomer(optionalCustomer.get());
            else return new ApiResponse(messageLanguage.getMessageByLanguage("client.notfound"), true);

            Optional<CurrencyType> optionalCurrencyType = currencyTypeRepository.findById(outputTradeDto.getCurrencyTypeId());
            if (optionalCurrencyType.isPresent()) outputTrade.setCurrencyType(optionalCurrencyType.get());
            else return new ApiResponse(messageLanguage.getMessageByLanguage("currency.notfound"), true);

            outputTrade.setDescription(outputTradeDto.getDescription());
            outputTrade.setDate(outputTradeDto.getDate());

            outputTradeRepository.save(outputTrade);

            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse getOutputTradePageable(int page, int size, UUID companyId) {
        try {
            List<OutputTradeDto> outputTradeDtoList = outputTradeRepository.getAllByCompanyIdPageable(companyId, page, size).stream().map(this::getOutputTradeDto).collect(Collectors.toList());
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.price.type.list"), true, outputTradeDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deleteOutputTrade(UUID id) {
        try {
            outputTradeRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public OutputTradeDto getOutputTradeDto(OutputTrade outputTrade) {
        return new OutputTradeDto(
                outputTrade.getId(),
                outputTrade.getDate(),
                outputTrade.getDescription(),
                outputTrade.getNumber(),
                getCustomerDto(outputTrade.getCustomer()),
                outputTrade.getCustomer() != null ? outputTrade.getCustomer().getId() : null,
                getCurrencyTypeDto(outputTrade.getCurrencyType()),
                outputTrade.getCurrencyType() != null ? outputTrade.getCurrencyType().getId() : null
        );
    }

    private CustomerDto getCustomerDto(Customer customer) {
        return new CustomerDto(
                customer.getId(),
                customer.getName(),
                customer.getEmail(),
                customer.getPhoneNumber(),
                customer.getAddress(),
                customer.isStatus(),
                companyService.getCompanyDto(customer.getCompany()),
                customer.getCompany() != null ? customer.getCompany().getId() : null
        );
    }

    private CurrencyTypeDto getCurrencyTypeDto(CurrencyType currencyType) {
        return new CurrencyTypeDto(
                currencyType.getId(),
                companyService.getCompanyDto(currencyType.getCompany()),
                currencyType.getCompany() != null ? currencyType.getCompany().getId() : null,
                currencyType.getNameUz(),
                currencyType.getNameRu(),
                currencyType.isActive(),
                currencyType.getDescription()
        );
    }

}
