package ai.ecma.workflow.service;

import ai.ecma.workflow.entity.SystemRole;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class CheckUser {

    public boolean isSuperAdmin(User user) {

        return checkSystemUser(user, SystemRoleNameEnum.ROLE_SUPER_ADMIN);

    }

    public boolean isAdmin(User user) {

        return checkSystemUser(user, SystemRoleNameEnum.ROLE_ADMIN);

    }

    public boolean isUser(User user) {

        return checkSystemUser(user, SystemRoleNameEnum.ROLE_USER);

    }

    public boolean checkSystemUser(User user, SystemRoleNameEnum roleName) {

        List<SystemRole> systemRoles = user.getSystemRoles();

        for (SystemRole systemRole : systemRoles) {
            if (systemRole.getSystemRoleNameEnum().equals(roleName)) {
                return true;
            }
        }

        return false;
    }
}
