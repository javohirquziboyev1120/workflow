package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Measurement;
import ai.ecma.workflow.entity.PriceType;
import ai.ecma.workflow.entity.Product;
import ai.ecma.workflow.entity.ProductPriceType;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.MeasurementDto;
import ai.ecma.workflow.payload.PriceTypeDto;
import ai.ecma.workflow.payload.ProductPriceTypeDto;
import ai.ecma.workflow.repository.ProductPriceTypeRepository;
import ai.ecma.workflow.repository.ProductRepository;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import ai.ecma.workflow.repository.dataRest.PriceTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductPriceTypeService {
    @Autowired
    MeasurementRepository measurementRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductPriceTypeRepository productPriceTypeRepository;

    @Autowired
    PriceTypeRepository priceTypeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductService productService;

    @Autowired
    CompanyService companyService;

    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse saveProductPriceType(ProductPriceTypeDto productPriceTypeDto) {
        try {
            ProductPriceType productPriceType = productPriceTypeRepository.findById(productPriceTypeDto.getId()).orElseGet(ProductPriceType::new);

            Optional<Product> optionalProduct = productRepository.findById(productPriceTypeDto.getProductId());
            if (!optionalProduct.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            productPriceType.setProduct(optionalProduct.get());

            Optional<Measurement> optionalMeasurement = measurementRepository.findById(productPriceTypeDto.getMeasurementId());
            if (!optionalMeasurement.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            productPriceType.setMeasurement(optionalMeasurement.get());

            Optional<PriceType> optionalPriceType = priceTypeRepository.findById(productPriceTypeDto.getPriceTypeId());
            if (!optionalPriceType.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            productPriceType.setPriceType(optionalPriceType.get());

            productPriceType.setPrice(productPriceType.getPrice());

            productPriceTypeRepository.save(productPriceType);

            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse getProductPriceTypePageable(int page, int size, UUID companyId) {
        try {
            List<ProductPriceTypeDto> productPriceTypeDtoList = productPriceTypeRepository.getAllByCompanyIdPageable(companyId, page, size).stream().map(this::getProductPriceTypeDto).collect(Collectors.toList());
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.price.type.list"), true, productPriceTypeDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deleteProductPriceType(UUID id) {
        try {
            productPriceTypeRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ProductPriceTypeDto getProductPriceTypeDto(ProductPriceType productPriceType) {
        return new ProductPriceTypeDto(
                productPriceType.getId(),
                productPriceType.getPrice(),
                productService.getProductDto(productPriceType.getProduct()),
                productPriceType.getProduct() != null ? productPriceType.getProduct().getId() : null,
                getMeasurementDto(productPriceType.getMeasurement()),
                productPriceType.getMeasurement() != null ? productPriceType.getMeasurement().getId() : null,
                getPriceTypeDto(productPriceType.getPriceType()),
                productPriceType.getPriceType() != null ? productPriceType.getPriceType().getId() : null
        );
    }

    public MeasurementDto getMeasurementDto(Measurement measurement) {
        return new MeasurementDto(
                measurement.getId(),
                measurement.getNameUz(),
                measurement.getNameRu(),
                measurement.isActive()
        );
    }

    public PriceTypeDto getPriceTypeDto(PriceType priceType) {
        return new PriceTypeDto(
                priceType.getId(),
                companyService.getCompanyDto(priceType.getCompany()),
                priceType.getCompany() != null ? priceType.getCompany().getId() : null,
                priceType.getNameUz(),
                priceType.getNameRu(),
                priceType.isActive()
        );
    }

}
