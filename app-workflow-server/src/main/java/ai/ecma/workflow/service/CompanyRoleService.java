package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Company;
import ai.ecma.workflow.entity.CompanyPermission;
import ai.ecma.workflow.entity.CompanyRole;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CompanyRoleDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.repository.CompanyRoleRepository;
import ai.ecma.workflow.repository.dataRest.CompanyPermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CompanyRoleService {
    @Autowired
    MessageLanguage messageLanguage;
    @Autowired
    CompanyRoleRepository companyRoleRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    CompanyPermissionRepository companyPermissionRepository;

    public ApiResponse addCompanyRole(CompanyRoleDto companyRoleDto) {

        CompanyRole newCompanyRole = new CompanyRole();
        return makeCompanyRole(newCompanyRole, companyRoleDto, false);
    }

    public ApiResponse editCompanyRole(UUID companyRoleId, CompanyRoleDto companyRoleDto) {
        try {
            CompanyRole foundedCompanyRole = companyRoleRepository.findById(companyRoleId).orElseThrow(() -> new ResourceNotFoundException("Company not found"));
            return makeCompanyRole(foundedCompanyRole, companyRoleDto, true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    public ApiResponse makeCompanyRole(CompanyRole companyRole, CompanyRoleDto companyRoleDto, boolean edit) {
        try {
            if (edit) {
                companyRole.setCompany(companyRoleDto.getCompanyId() == null ? companyRole.getCompany() : companyRepository.findById(companyRoleDto.getCompanyId()).orElseThrow(() -> new ResourceNotFoundException("Company not found")));

                if (companyRoleDto.getCompanyPermissionsId().isEmpty()) {
                    return new ApiResponse("CompanyPermissionsId is null", false);
                }

                List<CompanyPermission> companyPermissionList = companyPermissionRepository.findAllById(companyRoleDto.getCompanyPermissionsId());
                List<CompanyPermission> companyPermissions = companyRole.getCompanyPermissions();
                companyPermissions.addAll(companyPermissionList);

                companyRole.setCompanyPermissions(companyPermissions);

            } else {
                if (companyRoleDto.getCompanyId() == null) {
                    return new ApiResponse("Company kerak", false);
                }

                Optional<Company> company = companyRepository.findById(companyRoleDto.getCompanyId());
                if (company.isPresent()) {
                    companyRole.setCompany(company.get());
                } else {
                    return new ApiResponse("Company not found", false);
                }

                if (companyRoleDto.getCompanyPermissionsId().isEmpty()) {
                    return new ApiResponse("CompanyPermissionsId is null", false);
                }
                companyRole.setCompanyPermissions(companyPermissionRepository.findAllById(companyRoleDto.getCompanyPermissionsId()));
            }
            companyRole.setName(companyRoleDto.getName());
            companyRole.setDescription(companyRoleDto.getDescription());

            companyRoleRepository.save(companyRole);
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    public ApiResponse getCompanyRoleDto(UUID id) {
        return new ApiResponse(true, getCompanyRoleDto(companyRoleRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("CompanyRole not found"))));
    }

    public CompanyRoleDto getCompanyRoleDto(CompanyRole companyRole) {
        return new CompanyRoleDto(
                companyRole.getId(),
                companyRole.getName(),
                companyRole.getDescription(),
                companyRole.getCompany().getId(),
                companyRole.getCompany().getName(),
                companyRole.getCompanyPermissions()
        );
    }

    public PageableDto getAllCompanyRole(Integer page, Integer size) {
        Page<CompanyRole> companyRoles = companyRoleRepository.findAll(PageRequest.of(page, size));
        return new PageableDto(
                page,
                size,
                companyRoles.getTotalPages(),
                companyRoles.getTotalElements(),
                companyRoles.stream().map(this::getCompanyRoleDto).collect(Collectors.toList())
        );
    }

    public PageableDto getAllPermissions(Integer page, Integer size) {
        Page<CompanyPermission> permissions = companyPermissionRepository.findAll(PageRequest.of(page, size));
        return new PageableDto(
                page,
                size,
                permissions.getTotalPages(),
                permissions.getTotalElements(),
                permissions.getContent()
        );
    }
}
