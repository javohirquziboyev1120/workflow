package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CustomerPaymentDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.repository.CustomerPaymentRepository;
import ai.ecma.workflow.repository.OutputTradeRepository;
import ai.ecma.workflow.repository.dataRest.CurrencyRateRepository;
import ai.ecma.workflow.repository.dataRest.PayTypeRepository;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CustomerPaymentService {
    @Autowired
    OutputTradeRepository outputTradeRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    CustomerPaymentRepository customerPaymentRepository;
    @Autowired
    CurrencyRateRepository currencyRateRepository;
    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse addAndEditCustomerPayment(CustomerPaymentDto dto) {
        Optional<OutputTrade> optionalOutputTrade = outputTradeRepository.findById(dto.getOutputTradeId());
        Optional<PayType> optionalPayType = payTypeRepository.findById(dto.getPayTypeId());
        if (!optionalPayType.isPresent()) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("pay.type.not.found"), false);
        }
        if (!optionalOutputTrade.isPresent()) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
        }
        if (dto.getId()!=null){
            Optional<CustomerPayment> byId = customerPaymentRepository.findById(dto.getId());
            if (!byId.isPresent()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("pay.type.not.found"), false);
            }
            CustomerPayment customerPayment = byId.get();
            customerPaymentRepository.save(makeCustomerPayment(dto, customerPayment));
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        }
        customerPaymentRepository.save(makeCustomerPayment(dto, new CustomerPayment()));
        return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
    }
    public CustomerPaymentDto getCustomerPaymentDto(CustomerPayment customerPayment) {
        return new CustomerPaymentDto(
                customerPayment.getId(),
                customerPayment.getOutputTrade(),
                customerPayment.getPayType(),
                customerPayment.getAmount(),
                customerPayment.getInFactAmount(),
                customerPayment.getDate(),
                customerPayment.getDescription()
        );
    }

    public ApiResponse deleteCustomerPayment(UUID id) {
        try {
            customerPaymentRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), true);

        }
    }

    public PageableDto getCustomerPaymentPage(UUID outputTradeId, int page, int size) {
        Page<CustomerPayment> customerPaymentPage = customerPaymentRepository.findByOutputTradeId(outputTradeId, (PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT))));
        return new PageableDto(page, size, customerPaymentPage.getTotalPages(), customerPaymentPage.getTotalElements(),
                customerPaymentPage.getContent().stream().map(this::getCustomerPaymentDto).collect(Collectors.toList()));
    }

    public Double getCurrencyValue(CurrencyType fromCurrency, CurrencyType toCurrency, Double amount) {
        CurrencyRate currencyRate = currencyRateRepository.findByFromCurrencyAndToCurrency(fromCurrency, toCurrency);
        Double formValue = currencyRate.getFormValue();
        Double toValue = currencyRate.getToValue();
        Double inFactAmount = 0d;
        if (formValue > toValue) {
            inFactAmount = formValue / toValue;
        } else if (formValue < toValue) {
            inFactAmount = toValue / formValue;
        } else {
            inFactAmount = toValue;
        }
        return inFactAmount * amount;
    }

    public CustomerPayment makeCustomerPayment(CustomerPaymentDto dto, CustomerPayment customerPayment) {
        Optional<OutputTrade> optionalOutputTrade = outputTradeRepository.findById(dto.getOutputTradeId());
        Optional<PayType> optionalPayType = payTypeRepository.findById(dto.getPayTypeId());
        OutputTrade outputTrade = optionalOutputTrade.get();
        PayType payType = optionalPayType.get();
        customerPayment.setOutputTrade(outputTrade);
        customerPayment.setPayType(payType);
        CurrencyType currencyTypeOutPut = outputTrade.getCurrencyType();
        CurrencyType currencyTypePayType = payType.getCurrencyType();
        if (currencyTypeOutPut.equals(currencyTypePayType)) {
            customerPayment.setAmount(dto.getAmount());
            customerPayment.setInFactAmount(dto.getAmount());
            customerPayment.setDate(dto.getDate());
            customerPayment.setDescription(dto.getDescription());
            return customerPayment;
        }
        Double value = getCurrencyValue(currencyTypePayType, currencyTypeOutPut, dto.getAmount());
        customerPayment.setAmount(dto.getAmount());
        customerPayment.setInFactAmount(value);
        customerPayment.setDate(dto.getDate());
        customerPayment.setDescription(dto.getDescription());
        return customerPayment;
    }
}
