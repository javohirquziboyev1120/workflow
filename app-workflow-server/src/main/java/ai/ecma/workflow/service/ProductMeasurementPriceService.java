package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Measurement;
import ai.ecma.workflow.entity.Product;
import ai.ecma.workflow.entity.ProductMeasurementPrice;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.ProductMeasurementPriceDto;
import ai.ecma.workflow.repository.ProductMeasurementPriceRepository;
import ai.ecma.workflow.repository.ProductRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductMeasurementPriceService {
    @Autowired
    ProductMeasurementPriceRepository productMeasurementPriceRepository;

    @Autowired
    ProductPriceTypeService productPriceTypeService;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    MeasurementRepository measurementRepository;

    @Autowired
    ProductService productService;

    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse saveProductMeasurementPrice(ProductMeasurementPriceDto productMeasurementPriceDto) {
        try {
            ProductMeasurementPrice productMeasurementPrice = productMeasurementPriceRepository.findById(productMeasurementPriceDto.getId()).orElseGet(ProductMeasurementPrice::new);

            productMeasurementPrice.setInputPrice(productMeasurementPriceDto.getInputPrice());
            productMeasurementPrice.setMinPrice(productMeasurementPriceDto.getMinPrice());
            productMeasurementPrice.setSellPrice(productMeasurementPriceDto.getSellPrice());

            Optional<Measurement> optionalMeasurement = measurementRepository.findById(productMeasurementPriceDto.getMeasurementId());
            if (!optionalMeasurement.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);
            productMeasurementPrice.setMeasurement(optionalMeasurement.get());

            Optional<Product> optionalProduct = productRepository.findById(productMeasurementPriceDto.getProductId());
            if (!optionalProduct.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            productMeasurementPrice.setProduct(optionalProduct.get());

            productMeasurementPriceRepository.save(productMeasurementPrice);
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), false);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse getProductMeasurementPrice(int page, int size, UUID companyId) {
        try {
            List<ProductMeasurementPriceDto> productMeasurementPriceDtoList = productMeasurementPriceRepository.getProductMeasurementPricePageable(companyId, page, size).stream().map(this::getProductMeasurementPriceDto).collect(Collectors.toList());
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.price.type.list"), true, productMeasurementPriceDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deleteproductMeasurementPrice(UUID id) {
        try {
            productMeasurementPriceRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ProductMeasurementPriceDto getProductMeasurementPriceDto(ProductMeasurementPrice productMeasurementPrice) {
        return new ProductMeasurementPriceDto(
                productMeasurementPrice.getId(),
                productMeasurementPrice.getMinPrice(),
                productMeasurementPrice.getInputPrice(),
                productMeasurementPrice.getSellPrice(),
                productService.getProductDto(productMeasurementPrice.getProduct()),
                productMeasurementPrice.getProduct() != null ? productMeasurementPrice.getProduct().getId() : null,
                productPriceTypeService.getMeasurementDto(productMeasurementPrice.getMeasurement()),
                productMeasurementPrice.getMeasurement() != null ? productMeasurementPrice.getMeasurement().getId() : null
        );
    }
}
