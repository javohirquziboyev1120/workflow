package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.payload.SupplierPaymentDto;
import ai.ecma.workflow.repository.CashBoxRepository;
import ai.ecma.workflow.repository.InputTradeRepository;
import ai.ecma.workflow.repository.SupplierPaymentRepository;
import ai.ecma.workflow.repository.dataRest.CurrencyRateRepository;
import ai.ecma.workflow.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SupplierPaymentService {

    @Autowired
    SupplierPaymentRepository supplierPaymentRepository;
    @Autowired
    InputTradeRepository inputTradeRepository;
    @Autowired
    CashBoxRepository cashBoxRepository;
    @Autowired
    CurrencyRateRepository currencyRateRepository;
    @Autowired
    CustomerPaymentService customerPaymentService;
    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse addOrEditSupplierPayment(SupplierPaymentDto supplierPaymentDto) {
        try {
            SupplierPayment supplierPayment = new SupplierPayment();
            CashBox cashBox = cashBoxRepository.getOne(supplierPaymentDto.getCashBoxId());
            InputTrade inputTrade = inputTradeRepository.getOne(supplierPaymentDto.getInputTradeId());
            if (supplierPaymentDto.getId() != null) {
                supplierPayment = supplierPaymentRepository.findById(supplierPaymentDto.getId()).orElseThrow(() -> new ResourceNotFoundException("SupplierPayment", "id", supplierPaymentDto.getId()));
            }
            supplierPayment.setInputTrade(inputTrade);
            supplierPayment.setCashBox(cashBox);
            supplierPayment.setAmount(supplierPaymentDto.getAmount());
            supplierPayment.setHaqiqatda(inputTrade.getCurrencyType() == getCurrencyType(cashBox.getPayTypes()) ? customerPaymentService.getCurrencyValue(inputTrade.getCurrencyType(), getCurrencyType(cashBox.getPayTypes()),supplierPaymentDto.getAmount()) : supplierPaymentDto.getAmount());

            supplierPayment.setDate(supplierPaymentDto.getDate());
            supplierPayment.setDescription(supplierPaymentDto.getDescription());


            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"),false);
        }
    }

    private CurrencyType getCurrencyType(List<PayType> payTypes) {
        CurrencyType currency = null;
        for (PayType item : payTypes) {
               currency = item.getCurrencyType();
               break;
        }
        return currency;
    }

    public SupplierPaymentDto getSupplierPayment(SupplierPayment supplierPayment) {
        return new SupplierPaymentDto(
                supplierPayment.getId(),
                supplierPayment.getInputTrade().getId(),
                supplierPayment.getCashBox().getId(),
                supplierPayment.getAmount(),
                supplierPayment.getHaqiqatda(),
                supplierPayment.getDate(),
                supplierPayment.getDescription()
        );
    }


    public PageableDto getSupplierPayments(int page, int size, UUID inputTradeId) {
        Page<SupplierPayment> supplierPayments = supplierPaymentRepository.findAllByInputTradeId(inputTradeId, CommonUtils.getPageable(page, size));

        return new PageableDto(
                page,
                size,
                supplierPayments.getTotalPages(),
                supplierPayments.getTotalElements(),
                supplierPayments.getContent().stream().map(this::getSupplierPayment).collect(Collectors.toList())
        );
    }
}
