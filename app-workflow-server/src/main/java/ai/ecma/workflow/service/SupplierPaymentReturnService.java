package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.InputTrade;
import ai.ecma.workflow.entity.PayType;
import ai.ecma.workflow.entity.SupplierPaymentReturn;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.payload.SupplierPaymentReturnDto;
import ai.ecma.workflow.repository.InputTradeRepository;
import ai.ecma.workflow.repository.SupplierPaymentReturnRepository;
import ai.ecma.workflow.repository.dataRest.PayTypeRepository;
import ai.ecma.workflow.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SupplierPaymentReturnService {

    @Autowired
    SupplierPaymentReturnRepository supplierPaymentReturnRepository;
    @Autowired
    InputTradeRepository inputTradeRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    CustomerPaymentService customerPaymentService;
    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse addOrEditSupplierPaymentReturn(SupplierPaymentReturnDto supplierPaymentReturnDto) {
        try {
            SupplierPaymentReturn supplierPaymentReturn = new SupplierPaymentReturn();
            PayType payType = payTypeRepository.getOne(supplierPaymentReturnDto.getPayTypeId());
            InputTrade inputTrade = inputTradeRepository.getOne(supplierPaymentReturnDto.getInputTradeId());
            if (supplierPaymentReturnDto.getId() != null) {
                supplierPaymentReturn = supplierPaymentReturnRepository.getOne(supplierPaymentReturnDto.getId());
            }
            supplierPaymentReturn.setInputTrade(inputTrade);
            supplierPaymentReturn.setPayType(payType);
            supplierPaymentReturn.setAmount(supplierPaymentReturnDto.getAmount());

            supplierPaymentReturn.setHaqiqatda(inputTrade.getCurrencyType() == payType.getCurrencyType() ? supplierPaymentReturnDto.getAmount() : customerPaymentService.getCurrencyValue(inputTrade.getCurrencyType(), payType.getCurrencyType(), supplierPaymentReturnDto.getAmount()));

            supplierPaymentReturn.setDate(supplierPaymentReturnDto.getDate());
            supplierPaymentReturnDto.setDescription(supplierPaymentReturnDto.getDescription());
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public SupplierPaymentReturnDto getSupplierPaymentReturn(SupplierPaymentReturn supplierPaymentReturn) {
        return new SupplierPaymentReturnDto(
                supplierPaymentReturn.getId(),
                supplierPaymentReturn.getInputTrade().getId(),
                supplierPaymentReturn.getPayType().getId(),
                supplierPaymentReturn.getAmount(),
                supplierPaymentReturn.getHaqiqatda(),
                supplierPaymentReturn.getDate(),
                supplierPaymentReturn.getDescription()
        );
    }


    public PageableDto getSupplierPaymentReturns(UUID inputTradeId, int page, int size) {
        Page<SupplierPaymentReturn> supplierPaymentReturns = supplierPaymentReturnRepository.findAllByInputTradeId(inputTradeId, CommonUtils.getPageable(page, size));

        return new PageableDto(
                page,
                size,
                supplierPaymentReturns.getTotalPages(),
                supplierPaymentReturns.getTotalElements(),
                supplierPaymentReturns.getContent().stream().map(this::getSupplierPaymentReturn).collect(Collectors.toList())
        );
    }

}
