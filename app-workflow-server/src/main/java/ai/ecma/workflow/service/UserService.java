package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Company;
import ai.ecma.workflow.entity.SystemRole;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.payload.UserDto;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.repository.CompanyRoleRepository;
import ai.ecma.workflow.repository.dataRest.SystemRoleRepository;
import ai.ecma.workflow.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    SystemRoleRepository systemRoleRepository;
    @Autowired
    CompanyRoleRepository companyRoleRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    CompanyService companyService;
    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse addUser(UserDto dto, User currentUser) {
        User user = new User();
        if (dto.getId() != null) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setSystemRoles(systemRoleRepository.findAllBySystemRoleNameEnumIn(Collections.singletonList(SystemRoleNameEnum.ROLE_USER)));
        user.setCompanyRole(companyRoleRepository.getOne(dto.getCompanyRoleId()));
        user.setCompanies(currentUser.getCompanies());
        userRepository.save(user);

        return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
    }

    public UserDto getUser(User user) {
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPhoneNumber(),
                user.getPassword(),
                getRoleNames(user.getSystemRoles()),
                getCompanyNames(user.getCompanies())
        );
    }

    private List<SystemRoleNameEnum> getRoleNames(List<SystemRole> roleList) {
        List<SystemRoleNameEnum> roleNameList = new ArrayList<>();
        for (SystemRole role : roleList) {
            roleNameList.add(role.getSystemRoleNameEnum());
        }
        return roleNameList;
    }

    private List<String> getCompanyNames(List<Company> companies) {
        List<String> strings = new ArrayList<>();
        for (Company company : companies) {
            strings.add(company.getName());
        }
        return strings;
    }

    public PageableDto getUsers(int page, int size, User user, UUID companyId) {
        Page<User> userList = userRepository.getAllByCompany(companyId, CommonUtils.getPageable(page, size));
        return new PageableDto(
                page,
                size,
                userList.getTotalPages(),
                userList.getTotalElements(),
                userList
        );
    }

    public ApiResponse editUser(UserDto dto, User currentUser) {
        try {
            if (dto.getPhoneNumber() != null && userRepository.existsByPhoneNumber(dto.getPhoneNumber())) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("user.phone.number"), false);
            }
            currentUser.setId(dto.getId());
            currentUser.setFirstName(dto.getFirstName());
            currentUser.setLastName(dto.getLastName());
            currentUser.setEmail(dto.getEmail());
            currentUser.setPhoneNumber(dto.getPhoneNumber());
            userRepository.save(currentUser);

            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }
}
