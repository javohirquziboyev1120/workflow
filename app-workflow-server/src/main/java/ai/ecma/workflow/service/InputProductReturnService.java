package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.InputProduct;
import ai.ecma.workflow.entity.InputProductReturn;
import ai.ecma.workflow.entity.InputProductReturnAmount;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.InputProductReturnAmountDto;
import ai.ecma.workflow.payload.InputProductReturnDto;
import ai.ecma.workflow.repository.InputProductRepository;
import ai.ecma.workflow.repository.InputProductReturnAmountRepository;
import ai.ecma.workflow.repository.InputProductReturnRepository;
import ai.ecma.workflow.repository.PackagingRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class InputProductReturnService {
    @Autowired
    InputProductReturnRepository inputProductReturnRepository;
    @Autowired
    InputProductRepository inputProductRepository;
    @Autowired
    MeasurementRepository measurementRepository;
    @Autowired
    PackagingRepository packagingRepository;
    @Autowired
    InputProductReturnAmountRepository inputProductReturnAmountRepository;
    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse createInputProductReturn(InputProductReturnDto inputProductReturnDto) {
        InputProductReturn newInputProductReturn = new InputProductReturn();
        ApiResponse inputProductReturn = makeInputProductReturn(newInputProductReturn, inputProductReturnDto, false);

        if (inputProductReturn.isSuccess()) {
            InputProductReturn createdInputProductReturn = (InputProductReturn) inputProductReturn.getObject();

            if (!inputProductReturnDto.getInputProductReturnAmountDto().isEmpty()) {

                for (InputProductReturnAmountDto inputProductReturnAmountDto : inputProductReturnDto.getInputProductReturnAmountDto()) {
                    InputProductReturnAmount newInputProductReturnAmount = new InputProductReturnAmount();
                    ApiResponse inputProductReturnAmounts = makeInputProductReturnAmounts(newInputProductReturnAmount, inputProductReturnAmountDto, createdInputProductReturn, false);

                    if (!inputProductReturnAmounts.isSuccess()) {
                        return inputProductReturnAmounts;
                    }
                }
            } else {
                return new ApiResponse("InputProductReturnAmount not found", false);
            }
        } else {
            return inputProductReturn;
        }
        return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
    }

    public ApiResponse updateInputProductReturn(UUID inputProductReturnId, InputProductReturnDto inputProductReturnDto) {
        InputProductReturn foundedInputProductReturn = inputProductReturnRepository.findById(inputProductReturnId).orElseThrow(() -> new ResourceNotFoundException("InputProductReturn not found"));
        ApiResponse inputProductReturn = makeInputProductReturn(foundedInputProductReturn, inputProductReturnDto, true);
        if (inputProductReturn.isSuccess()) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("edited"), true);
        } else {
            return inputProductReturn;
        }
    }

    public ApiResponse makeInputProductReturn(InputProductReturn inputProductReturn, InputProductReturnDto inputProductReturnDto, boolean edit) {
        try {
            if (edit) {
                inputProductReturn.setInputProduct(inputProductReturnDto.getInputProductId() == null ? inputProductReturn.getInputProduct() : inputProductRepository.findById(inputProductReturnDto.getInputProductId()).orElseThrow(() -> new ResourceNotFoundException("Input Product not found")));
            } else {
                if (inputProductReturnDto.getInputProductId() != null) {
                    Optional<InputProduct> inputProduct = inputProductRepository.findById(inputProductReturnDto.getInputProductId());
                    if (!inputProduct.isPresent()) {
                        return new ApiResponse("InputProduct topilmadi", false);
                    }
                    inputProductReturn.setInputProduct(inputProduct.get());
                } else {
                    return new ApiResponse("InputProduct tanlash kerak", false);
                }
            }
            inputProductReturn.setDate(inputProductReturnDto.getDate());
            inputProductReturn.setDefect(inputProductReturnDto.isDefect());
            inputProductReturn.setDescription(inputProductReturnDto.getDescription());

            InputProductReturn savedInputProductReturn = inputProductReturnRepository.save(inputProductReturn);
            return new ApiResponse(true, savedInputProductReturn);
        } catch (Exception e) {
            return new ApiResponse("InputProductReturn topilmadi", false);
        }
    }

    public ApiResponse makeInputProductReturnAmounts(InputProductReturnAmount inputProductReturnAmount, InputProductReturnAmountDto inputProductReturnAmountDto, InputProductReturn inputProductReturn, boolean edit) {
        try {
            if (edit) {
                inputProductReturnAmount.setMeasurement(inputProductReturnAmountDto.getMeasurementId() == null ? inputProductReturnAmount.getMeasurement() : measurementRepository.findById(inputProductReturnAmountDto.getMeasurementId()).orElseThrow(() -> new ResourceNotFoundException("Measurement not found")));
                inputProductReturnAmount.setPackaging(inputProductReturnAmountDto.getPackagingId() == null ? inputProductReturnAmount.getPackaging() : packagingRepository.findById(inputProductReturnAmountDto.getPackagingId()).orElseThrow(() -> new ResourceNotFoundException("Packaging not found")));
            } else {
                inputProductReturnAmount.setMeasurement(measurementRepository.findById(inputProductReturnAmountDto.getMeasurementId()).orElseThrow(() -> new ResourceNotFoundException("Measurement not found")));
                inputProductReturnAmount.setPackaging(packagingRepository.findById(inputProductReturnAmountDto.getPackagingId()).orElseThrow(() -> new ResourceNotFoundException("Packaging not found")));
                inputProductReturnAmount.setInputProductReturn(inputProductReturn);
            }

            inputProductReturnAmount.setAmountPackaging(inputProductReturnAmountDto.getAmountPackaging());
            inputProductReturnAmount.setAmount(inputProductReturnAmountDto.getAmount());

            inputProductReturnAmountRepository.save(inputProductReturnAmount);
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(false);
        }
    }
}
