package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.OutputProductReturnAmountDto;
import ai.ecma.workflow.payload.OutputProductReturnDto;
import ai.ecma.workflow.repository.*;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OutputProductReturnService {
    @Autowired
    OutputProductRepository outputProductRepository;

    @Autowired
    OutputProductReturnRepository outputProductReturnRepository;

    @Autowired
    OutputProductReturnAmountRepository outputProductReturnAmountRepository;

    @Autowired
    OutputTradeRepository outputTradeRepository;

    @Autowired
    ProductPriceTypeService productPriceTypeService;

    @Autowired
    InputProductRepository inputProductRepository;

    @Autowired
    InputProductLeftoverRepository inputProductLeftoverRepository;

    @Autowired
    MeasurementRepository measurementRepository;

    @Autowired
    PackagingRepository packagingRepository;

    @Autowired
    PackagingService packagingService;

    @Autowired
    OutputProductService outputProductService;

    @Autowired
    MessageLanguage messageLanguage;

    private Double calculatePackagingAmount(Double packCapacity, Double packAmount, Double pieceAmount) {
        while (pieceAmount > packCapacity) {
            pieceAmount = -packCapacity;
            packAmount++;
        }
        packAmount *= packCapacity;
        return packAmount + pieceAmount;
    }

    public ApiResponse saveOutputProductReturn(OutputProductReturnDto outputProductReturnDto) {
        try {
            OutputProductReturn outputProductReturn = outputProductReturnRepository.findById(outputProductReturnDto.getId()).orElseGet(OutputProductReturn::new);
            outputProductReturn.setDescription(outputProductReturnDto.getDescription());
            outputProductReturn.setDefect(outputProductReturnDto.isDefect());

            OutputProductReturn savedOutputProductReturn = outputProductReturnRepository.save(outputProductReturn);

            Optional<OutputProduct> optionalOutputProduct = outputProductRepository.findById(outputProductReturnDto.getOutputProductId());
            if (!optionalOutputProduct.isPresent())
                return new ApiResponse(messageLanguage.getMessageByLanguage("selled.product.not.found"), false);
            OutputProduct outputProduct = optionalOutputProduct.get();

            List<OutputProductReturnAmount> outputProductReturnAmountList = new ArrayList<>();

            for (OutputProductReturnAmountDto outputProductReturnAmountDto : outputProductReturnDto.getOutputProductReturnAmountDtos()) {
                double returnAmountPackaging = outputProductReturnAmountDto.getAmountPackaging();
                double returnAmount = outputProductReturnAmountDto.getAmount();

                Optional<Measurement> optionalMeasurement = measurementRepository.findById(outputProductReturnAmountDto.getMeasurementId());
                if (!optionalMeasurement.isPresent())
                    return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);

                Optional<Packaging> optionalPackaging = packagingRepository.findById(outputProductReturnAmountDto.getPackagingId());
                if (!optionalPackaging.isPresent())
                    return new ApiResponse(messageLanguage.getMessageByLanguage("packaging.notfound"), false);

                outputProductReturnAmountList.add(new OutputProductReturnAmount(savedOutputProductReturn, optionalMeasurement.get(), optionalPackaging.get(), returnAmountPackaging, returnAmount));

            }
            savedOutputProductReturn.setOutputProductReturnAmounts(outputProductReturnAmountList);
            outputProductReturnRepository.save(outputProductReturn);
            return new ApiResponse(messageLanguage.getMessageByLanguage("success"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse getOutputProductReturnPageable(int page, int size, UUID companyId) {
        try {
            List<OutputProductReturnDto> outputProductReturnDtoList = outputProductReturnRepository.getAllByCompanyIdPageable(companyId, page, size).stream().map(this::getOutputProductReturnDto).collect(Collectors.toList());
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.price.type.list"), true, outputProductReturnDtoList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public ApiResponse deleteOutputProductReturn(UUID id) {
        try {
            outputProductReturnRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    private OutputProductReturnDto getOutputProductReturnDto(OutputProductReturn outputProductReturn) {
        return new OutputProductReturnDto(
                outputProductReturn.getId(),
                outputProductReturn.getDescription(),
                outputProductReturn.isDefect(),
                outputProductService.getOutputProductDto(outputProductReturn.getOutputProduct()),
                outputProductReturn.getOutputProduct() != null ? outputProductReturn.getOutputProduct().getId() : null,
                outputProductReturn.getOutputProductReturnAmounts().stream().map(this::getOutputProductReturnAmountDto).collect(Collectors.toList())
        );
    }

    private OutputProductReturnAmountDto getOutputProductReturnAmountDto(OutputProductReturnAmount outputProductReturnAmount) {
        return new OutputProductReturnAmountDto(
                outputProductReturnAmount.getId(),
                outputProductReturnAmount.getOutputProductReturn() != null ? outputProductReturnAmount.getOutputProductReturn().getOutputProduct().getId() : null,
                productPriceTypeService.getMeasurementDto(outputProductReturnAmount.getMeasurement()),
                outputProductReturnAmount.getMeasurement() != null ? outputProductReturnAmount.getMeasurement().getId() : null,
                packagingService.getPackagingDto(outputProductReturnAmount.getPackaging()),
                outputProductReturnAmount.getPackaging() != null ? outputProductReturnAmount.getPackaging().getId() : null,
                outputProductReturnAmount.getAmountPackaging(),
                outputProductReturnAmount.getAmount()
        );
    }
}
