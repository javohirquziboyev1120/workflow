package ai.ecma.workflow.service;

import ai.ecma.workflow.entity.Contact;
import ai.ecma.workflow.entity.District;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ContactDto;
import ai.ecma.workflow.repository.dataRest.ContactRepository;
import ai.ecma.workflow.repository.dataRest.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    DistrictRepository districtRepository;

    public Contact addOrEditContact(ContactDto reqContact) {
        Contact contact = new Contact();
        if (reqContact.getId() != null) {
            contact = contactRepository.getOne(reqContact.getId());
        }
        District district = districtRepository.findById(reqContact.getDistrictId()).orElseThrow(() -> new ResourceNotFoundException("District", "id", reqContact.getDistrictId()));
        contact.setDistrict(district);
        contact.setPhoneNumber(reqContact.getPhoneNumber());
        contact.setEmail(reqContact.getEmail());
        contact.setLan(reqContact.getLan());
        contact.setLat(reqContact.getLat());
        contact.setStreet(reqContact.getStreet());
        return contact;
    }

    public ContactDto getContact(Contact contact) {
        return new ContactDto(
                contact.getId(),
                contact.getPhoneNumber(),
                contact.getEmail(),
                contact.getLan(),
                contact.getLat(),
                contact.getStreet(),
                contact.getDistrict().getId(),
                contact.getDistrict().getNameUz(),
                contact.getDistrict().getNameRu(),
                contact.getDistrict().getRegion().getId(),
                contact.getDistrict().getRegion().getNameUz(),
                contact.getDistrict().getRegion().getNameRu()
        );
    }
}
