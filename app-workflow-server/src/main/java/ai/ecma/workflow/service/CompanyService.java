package ai.ecma.workflow.service;

import ai.ecma.workflow.entity.Company;
import ai.ecma.workflow.entity.Contact;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.entity.Warehouse;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CompanyDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.repository.WarehouseRepository;
import ai.ecma.workflow.repository.dataRest.ContactRepository;
import ai.ecma.workflow.repository.dataRest.DistrictRepository;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    CompanyService companyService;
    @Autowired
    ContactService contactService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    WarehouseRepository warehouseRepository;
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    DistrictRepository districtRepository;


    public ApiResponse addAndEditCompany(User user, CompanyDto dto) {
        try {
            Company company = new Company();
            if (dto.getId() != null) {
                company = companyRepository.findById(dto.getId()).orElseThrow(() -> new ResourceNotFoundException("Company", "id", dto.getId()));
            }
            company.setName(dto.getName());
//            if (dto.getContactDto() != null) {
//                company.setContact(contactService.addOrEditContact(dto.getContactDto()));
//            }
            Contact contact = new Contact();
            contact.setPhoneNumber(dto.getPhoneNumber());
            contact.setEmail(dto.getEmail());
            contact.setLan(dto.getLan());
            contact.setLat(dto.getLat());
            contact.setStreet(dto.getStreet());
            contact.setDistrict(districtRepository.getOne(dto.getDistrictId()));
            Contact saveContact = contactRepository.save(contact);
            company.setContact(saveContact);
            Company saveCompany = companyRepository.save(company);
            Warehouse warehouse = new Warehouse();
            warehouse.setName("Warehouse");
            warehouse.setCompany(saveCompany);
            warehouseRepository.save(warehouse);

            user.getCompanies().add(saveCompany);
            userRepository.save(user);

            return new ApiResponse("Company taxrirlandi.", "Company taxrirlandi.", true);
        } catch (Exception e) {
            return new ApiResponse("Xatolik.", "Xatolik.", false);
        }
    }

    public CompanyDto getCompanyDto(Company company) {
        return new CompanyDto(
                company.getId(),
                company.getName(),
                company.getContact().getPhoneNumber(),
                company.getContact().getEmail(),
                company.getContact().getLan(),
                company.getContact().getLat(),
                company.getContact().getStreet(),
                company.getContact().getDistrict().getId(),
                company.getContact().getDistrict().getNameUz(),
                company.getContact().getDistrict().getNameRu(),
                company.getContact().getDistrict().getRegion().getId(),
                company.getContact().getDistrict().getRegion().getNameUz(),
                company.getContact().getDistrict().getRegion().getNameRu()
        );
    }

    public PageableDto getCompanyPage(int page, int size){
        Page<Company> companyPage = companyRepository.findAll((PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT))));
        return new PageableDto(page, size, companyPage.getTotalPages(), companyPage.getTotalElements(),
                companyPage.getContent().stream().map(this::getCompanyDto).collect(Collectors.toList()));
    }

}
