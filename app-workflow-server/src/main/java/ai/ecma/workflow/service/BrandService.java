package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.Brand;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.BrandDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.repository.BrandRepository;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BrandService {

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    MessageLanguage messageLanguage;

    public ApiResponse addOrEditBrand(BrandDto request) {
        try {
            Brand brand = new Brand();
            if (brand.getId() != null) {
                brand = brandRepository.findById(request.getId()).orElseThrow(() -> new ResourceNotFoundException("addAndEditBrand", "id", request.getId()));
            }
            brand.setNameUz(request.getNameUz());
            brand.setNameRu(request.getNameRu());
            brand.setCompany(companyRepository.getOne(request.getId()));
            brandRepository.save(brand);

            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    public BrandDto getBrand(Brand brand) {
        return new BrandDto(
                brand.getId(),
                brand.getNameUz(),
                brand.getNameRu(),
                brand.getCompany().getId()
        );
    }


    public PageableDto getBrands(int page, int size, UUID companyId) {
        Page<Brand> brandPage = brandRepository.findAllByCompanyId(companyId, CommonUtils.getPageable(page, size));
        return new PageableDto(
                page,
                size,
                brandPage.getTotalPages(),
                brandPage.getTotalElements(),
                brandPage.getContent().stream().map(this::getBrand).collect(Collectors.toList())
        );
    }

    public ApiResponse deleteBrand(UUID brandId) {
        Optional<Brand> brandOptional = brandRepository.findById(brandId);
        if (brandOptional.isPresent()) {
            try {
                brandRepository.deleteById(brandId);
                return new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true);
            } catch (Exception e) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
            }
        }
        return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
    }
}
