package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.payload.*;
import ai.ecma.workflow.repository.AttachmentRepository;
import ai.ecma.workflow.repository.BrandRepository;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.repository.ProductRepository;
import ai.ecma.workflow.repository.dataRest.CategoryRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import ai.ecma.workflow.repository.dataRest.ValuesRepository;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    BrandRepository brandRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    MeasurementRepository measurementRepository;
    @Autowired
    ValuesRepository valuesRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    MessageLanguage messageLanguage;
    @Autowired
    BrandService brandService;
    @Autowired
    CompanyService companyService;


    /***
     * yangi product qushish
     * @param dto qushiladigan product malumotlari
     * @return Jarayon haqida malumot
     */
    public ApiResponse addAndEditProduct(ProductDto dto) {
        try {
            if (dto.getId()!=null){
                Optional<Product> optionalProduct = productRepository.findByIdAndCompanyId(dto.getId(), dto.getCompanyId());
                if (optionalProduct.isPresent()) {
                    Product product = optionalProduct.get();
                    productRepository.save(makeProduct(dto, product));
                    return new ApiResponse(messageLanguage.getMessageByLanguage("product.edit"), true);
                }
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            }
            productRepository.save(makeProduct(dto, new Product()));
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }

    }
    /***
     * productlarni dto sini qaytaradi
     * @param
     * @return productDto
     */
    public ProductDto getProductDto(Product product) {
        return new ProductDto(
                product.getId(),
                product.getNameUz(),
                product.getNameRu(),
                product.isActive(),
                product.getBarCode(),
                product.getDescription(),
                brandService.getBrand(product.getBrand()),
                getCategoryDto(product.getCategory()),
                product.getPhotos(),
                companyService.getCompanyDto(product.getCompany()),
                product.getValues().stream().map(this::getValuesDto).collect(Collectors.toList()),
                product.getMeasurements().stream().map(this::getMeasurementDto).collect(Collectors.toList())
        );
    }

    public MeasurementDto getMeasurementDto(Measurement measurement) {
        return new MeasurementDto(
                measurement.getId(),
                measurement.getNameUz(),
                measurement.getNameRu(),
                measurement.isActive()
        );
    }
    private CategoryDto getCategoryDto(Category category){
        return new CategoryDto(
                category.getId(),
                category.getNameUz(),
                category.getNameRu(),
                category.getIndex(),
                companyService.getCompanyDto(category.getCompany()),null
//                getCategoryDto(category.getParent())
        );
    }

    /***
     * bitta paroductni o'chirish
     * @param id produxt id si keladi
     * @return javib qaytaramiz
     */
    public ApiResponse deleteProduct(UUID id) {
        try {
            productRepository.deleteById(id);
            return new ApiResponse(messageLanguage.getMessageByLanguage("product.delete"), true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("error"), false);
        }
    }

    /***
     * add va edit qilish uchun asosiy qilib  bitta product qushadigan Method  yasadek
     * @param dto malumotlar keladi
     * @param product qaysi product ekani yangi product bulsa new buladi eskisi bulsa usha prodeuct keladi
     * @return tayyor bulgan product qaytramiz
     */
    public Product makeProduct(ProductDto dto, Product product) {
        product.setNameUz(dto.getNameUz());
        product.setNameRu(dto.getNameRu());
        product.setActive(dto.isActive());
        product.setBarCode(dto.getBarCode());
        product.setBrand(brandRepository.findById(dto.getBrandId()).orElseThrow(() -> new ResourceNotFoundException("GetBrand")));
        product.setCategory(categoryRepository.findById(dto.getCategoryId()).orElseThrow(() -> new ResourceNotFoundException("getCategory")));
        product.setCompany(companyRepository.findById(dto.getCompanyId()).orElseThrow(() -> new ResourceNotFoundException("getCompany")));
        product.setDescription(dto.getDescription());
        product.setMeasurements(measurementRepository.findAllById(dto.getMeasurementsId()));
        product.setValues(valuesRepository.findAllById(dto.getValuesId()));
        product.setPhotos(attachmentRepository.findAllById(dto.getAttachmentId()));
        return product;
    }

    public PageableDto getProductPage( UUID companyId, int page, int size){
        Page<Product> productPage = productRepository.findAllByCompanyId(companyId,(PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT))));
//        Page<Product> productPage = productRepository.findAll((PageRequest.of(page, size, Sort.by(AppConstants.ASC_OR_DECK, AppConstants.COLUMN_NAME_FOR_SORT))));
        return new PageableDto(
                page,
                size,
                productPage.getTotalPages(),
                productPage.getTotalElements(),
                productPage.getContent().stream().map(this::getProductDto).collect(Collectors.toList()));
    }

    public ApiResponse getValuesByDetail(UUID detailId) {
        List<ValuesDto> dtos = valuesRepository.findAllByDetailId(detailId).stream().map(this::getValuesDto).collect(Collectors.toList());
        return new ApiResponse(messageLanguage.getMessageByLanguage("saved"),true,dtos);
    }

    public ValuesDto getValuesDto(Values values){
        return new ValuesDto(
                values.getId(),
                values.getNameUz(),
                values.getNameRu(),
                values.isActive(),
                getDetailDto(values.getDetail())
                );
    }
    public DetailDto getDetailDto(Detail detail){
        return new DetailDto(
                detail.getId(),
                detail.getNameUz(),
                detail.getNameRu(),
                detail.isActive(),
                companyService.getCompanyDto(detail.getCompany())
                );
    }
}



