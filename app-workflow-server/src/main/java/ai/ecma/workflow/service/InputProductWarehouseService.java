package ai.ecma.workflow.service;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.*;
import ai.ecma.workflow.payload.*;
import ai.ecma.workflow.repository.*;
import ai.ecma.workflow.repository.dataRest.CurrencyTypeRepository;
import ai.ecma.workflow.repository.dataRest.MeasurementRepository;
import ai.ecma.workflow.repository.dataRest.SupplierRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class InputProductWarehouseService {

    final
    SupplierRepository supplierRepository;
    final
    CurrencyTypeRepository currencyTypeRepository;
    final
    InputTradeRepository inputTradeRepository;
    final
    ProductRepository productRepository;
    final
    WarehouseRepository warehouseRepository;
    final
    MeasurementRepository measurementRepository;
    final
    InputProductRepository inputProductRepository;
    final
    PackagingRepository packagingRepository;
    final
    InputProductAmountRepository inputProductAmountRepository;
    final
    InputProductLeftoverRepository inputProductLeftoverRepository;
    final
    CheckUser checkUser;
    final
    MessageLanguage messageLanguage;

    public InputProductWarehouseService(InputTradeRepository inputTradeRepository, SupplierRepository supplierRepository, CurrencyTypeRepository currencyTypeRepository, ProductRepository productRepository, WarehouseRepository warehouseRepository, MeasurementRepository measurementRepository, InputProductRepository inputProductRepository, PackagingRepository packagingRepository, InputProductAmountRepository inputProductAmountRepository, InputProductLeftoverRepository inputProductLeftoverRepository, CheckUser checkUser, MessageLanguage messageLanguage) {
        this.inputTradeRepository = inputTradeRepository;
        this.supplierRepository = supplierRepository;
        this.currencyTypeRepository = currencyTypeRepository;
        this.productRepository = productRepository;
        this.warehouseRepository = warehouseRepository;
        this.measurementRepository = measurementRepository;
        this.inputProductRepository = inputProductRepository;
        this.packagingRepository = packagingRepository;
        this.inputProductAmountRepository = inputProductAmountRepository;
        this.inputProductLeftoverRepository = inputProductLeftoverRepository;
        this.checkUser = checkUser;
        this.messageLanguage = messageLanguage;
    }

    // Omborga kirim bo'lish jarayoni
    // step 1 => create InputTrade
    // step 2 => create InputProduct
    // step 3 => create InputProductAmount
    // step 4 => create InputProductLeftover

    /***
     *  CLIENT tomondan inputProductDto da ma'lumotlar keladi va ishlar ketma-ketligi shu yerda taqsimlangan
     *
     * @param inputTradeDto
     * @return
     *
     */
    public ApiResponse incomeProcess(InputTradeDto inputTradeDto) {
        ApiResponse inputTrade = createInputTrade(inputTradeDto);
        if (inputTrade.isSuccess()) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("saved"), true);
        } else {
            return inputTrade;
        }
    }

    /***
     *
     * @param inputTradeDto
     * @return
     *
     */
    public ApiResponse createInputTrade(InputTradeDto inputTradeDto) {
        try {
            InputTrade newInputTrade = new InputTrade();
            ApiResponse makeInputTrade = makeInputTrade(newInputTrade, inputTradeDto, false);

            if (makeInputTrade.isSuccess()) {
                InputProduct newInputProduct = new InputProduct();
                InputTrade inputTrade = (InputTrade) makeInputTrade.getObject();

                if (!inputTradeDto.getInputProductDtoList().isEmpty()) {
                    for (InputProductDto inputProductDto : inputTradeDto.getInputProductDtoList()) {
                        ApiResponse makeInputProduct = makeInputProduct(newInputProduct, inputProductDto, inputTradeDto.getWarehouseId(), inputTrade, false);

                        if (makeInputProduct.isSuccess()) {
                            InputProduct savedInputProduct = (InputProduct) makeInputProduct.getObject();

                            if (!inputProductDto.getInputProductAmountDtoList().isEmpty()) {
                                for (InputProductAmountDto inputProductAmountDto : inputProductDto.getInputProductAmountDtoList()) {
                                    ApiResponse inputProductAmount = createInputProductAmount(inputProductAmountDto, savedInputProduct);

                                    if (!inputProductAmount.isSuccess()) {
                                        inputProductRepository.deleteAllByInputTradeId(inputTrade.getId());
                                        inputTradeRepository.deleteById(inputTrade.getId());
                                        return inputProductAmount;
                                    }
                                }
                            } else {
                                return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);
                            }
                        } else {
                            inputTradeRepository.deleteById(inputTrade.getId());
                            return makeInputProduct;
                        }
                    }
                } else {
                    return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
                }
            } else {
                return makeInputTrade;
            }
            return new ApiResponse(true);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    /***
     *
     * @param inputTradeId
     * @param inputTradeDto
     * @return
     *
     */
    public ApiResponse updateInputTrade(UUID inputTradeId, InputTradeDto inputTradeDto) {
        try {
            InputTrade foundedInputTrade = inputTradeRepository.findById(inputTradeId).orElseThrow(() -> new ResourceNotFoundException("InputTrade not found"));
            ApiResponse apiResponse = makeInputTrade(foundedInputTrade, inputTradeDto, true);
            if (apiResponse.isSuccess()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("edited"), true);
            }
            return new ApiResponse(false);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    /***
     *
     * @param inputTrade
     * @param inputTradeDto
     * @param edit
     * @return
     *
     */
    public ApiResponse makeInputTrade(InputTrade inputTrade, InputTradeDto inputTradeDto, boolean edit) {
        if (edit) {
            inputTrade.setSupplier(inputTradeDto.getSupplierId() == null ? inputTrade.getSupplier() : supplierRepository.findById(inputTradeDto.getSupplierId()).orElseThrow(() -> new ResourceNotFoundException("Supplier not found")));
            inputTrade.setCurrencyType(inputTradeDto.getCurrencyTypeId() == null ? inputTrade.getCurrencyType() : currencyTypeRepository.findById(inputTradeDto.getCurrencyTypeId()).orElseThrow(() -> new ResourceNotFoundException("CurrencyType not found")));
        } else {

            if (inputTradeDto.getSupplierId() == null) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("must.select.supplier"), false);
            }

            Optional<Supplier> supplier = supplierRepository.findById(inputTradeDto.getSupplierId());
            if (!supplier.isPresent()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("not.found.supplier"), false);
            }

            if (inputTradeDto.getCurrencyTypeId() == null) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("must.select.currency.type"), false);
            }

            Optional<CurrencyType> currencyType = currencyTypeRepository.findById(inputTradeDto.getCurrencyTypeId());
            if (!currencyType.isPresent()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("not.found.currency.type"), false);
            }

            inputTrade.setSupplier(supplier.get());
            inputTrade.setCurrencyType(currencyType.get());

        }
        inputTrade.setNumber(inputTradeDto.getNumber());
        inputTrade.setFactureNumber(inputTradeDto.getFactureNumber());
        inputTrade.setDate(inputTradeDto.getDate());
        inputTrade.setDescription(inputTradeDto.getInputTradeDescription());

        InputTrade savedInputTrade = inputTradeRepository.save(inputTrade);
        return new ApiResponse(true, savedInputTrade);
    }

//    public ApiResponse createInputProduct(InputTradeDto inputTradeDto, InputTrade savedInputTrade) {
//        try {
//            InputProduct newInputProduct = new InputProduct();
//
//            List<InputProduct> inputProducts = new ArrayList<>();
//
//
//            return new ApiResponse(false, inputProducts);
//
//        } catch (Exception e) {
//            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
//        }
//    }

    /***
     *
     * @param inputProductId
     * @param inputProductDto
     * @return
     *
     */
    public ApiResponse updateInputProduct(UUID inputProductId, InputProductDto inputProductDto) {
        try {
            InputProduct foundedInputProduct = inputProductRepository.findById(inputProductId).orElseThrow(() -> new ResourceNotFoundException("InputProduct not found"));
            //TODO : input trade
            ApiResponse apiResponse = makeInputProduct(foundedInputProduct, inputProductDto, inputProductDto.getWarehouseId(), null, true);
            if (apiResponse.isSuccess()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("edited"), true);
            }
            return new ApiResponse(false);
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    /***
     *
     * @param inputProduct
     * @param inputProductDto
     * @param inputTrade
     * @param edit
     * @return
     *
     */
    public ApiResponse makeInputProduct(InputProduct inputProduct, InputProductDto inputProductDto, UUID warehouseId, InputTrade inputTrade, boolean edit) {
        if (edit) {
            inputProduct.setProduct(inputProductDto.getProductId() == null ? inputProduct.getProduct() : productRepository.findById(inputProductDto.getProductId()).orElseThrow(() -> new ResourceNotFoundException("Product not found")));
            inputProduct.setWarehouse(warehouseId == null ? inputProduct.getWarehouse() : warehouseRepository.findById(warehouseId).orElseThrow(() -> new ResourceNotFoundException("Warehouse ot found")));
            inputProduct.setPriceMeasurement(inputProductDto.getPriceMeasurementId() == null ? inputProduct.getPriceMeasurement() : measurementRepository.findById(inputProductDto.getPriceMeasurementId()).orElseThrow(() -> new ResourceNotFoundException("PriceMeasurement not found")));
        } else {

            if (inputProductDto.getProductId() == null) {
                return new ApiResponse("Mahsulot tanlanishi shart", false);
            }

            Optional<Product> product = productRepository.findById(inputProductDto.getProductId());
            if (!product.isPresent()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("product.notfound"), false);
            }

            if (warehouseId == null) {
                return new ApiResponse("Ombor tanlanishi shart", false);
            }

            Optional<Warehouse> warehouse = warehouseRepository.findById(warehouseId);
            if (!warehouse.isPresent()) {
                return new ApiResponse("Ombor topilmadi", false);
            }

            if (inputProductDto.getPriceMeasurementId() == null) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);
            }

            Optional<Measurement> measurement = measurementRepository.findById(inputProductDto.getPriceMeasurementId());
            if (!measurement.isPresent()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);
            }

            inputProduct.setProduct(product.get());
            inputProduct.setWarehouse(warehouse.get());
            inputProduct.setPriceMeasurement(measurement.get());
            inputProduct.setInputTrade(inputTrade);
        }

        inputProduct.setPrice(inputProductDto.getPrice());
        inputProduct.setDescription(inputProductDto.getInputProductDescription());

        InputProduct savedInputProduct = inputProductRepository.save(inputProduct);
        return new ApiResponse(true, savedInputProduct);

    }

    /***
     *
     * @param inputProductAmountDto
     * @param savedInputProduct
     * @return
     *
     */
    public ApiResponse createInputProductAmount(InputProductAmountDto inputProductAmountDto, InputProduct savedInputProduct) {
        try {
            InputProductAmount newInputProductAmount = new InputProductAmount();

            return makeInputProductAmount(newInputProductAmount, inputProductAmountDto, savedInputProduct, false);

        } catch (Exception e) {
            return new ApiResponse(false);
        }
    }

    /***
     *
     * @param inputProductAmountId
     * @param inputProductAmountDto
     * @return
     *
     */
    public ApiResponse updateInputProductAmount(UUID inputProductAmountId, InputProductAmountDto inputProductAmountDto) {
        try {
            InputProductAmount foundedInputProductAmount = inputProductAmountRepository.findById(inputProductAmountId).orElseThrow(() -> new ResourceNotFoundException("InputProductAmount not found"));
            ApiResponse apiResponse = makeInputProductAmount(foundedInputProductAmount, inputProductAmountDto, null, true);
            if (apiResponse.isSuccess()) {
                return new ApiResponse(messageLanguage.getMessageByLanguage("edited"), true);
            }
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);

        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("server.error"), false);
        }
    }

    /***
     *
     * @param inputProductAmount
     * @param inputProductAmountDto
     * @param inputProduct
     * @param edit
     * @return
     *
     */
    public ApiResponse makeInputProductAmount(InputProductAmount inputProductAmount, InputProductAmountDto inputProductAmountDto, InputProduct inputProduct, boolean edit) {
        if (edit) {
            inputProductAmount.setMeasurement(inputProductAmountDto.getMeasurementId() == null ? inputProductAmount.getMeasurement() : measurementRepository.findById(inputProductAmountDto.getMeasurementId()).orElseThrow(() -> new ResourceNotFoundException("Measurement not found")));
            inputProductAmount.setPackaging(inputProductAmountDto.getPackagingId() == null ? inputProductAmount.getPackaging() : packagingRepository.findById(inputProductAmountDto.getPackagingId()).orElseThrow(() -> new ResourceNotFoundException("Packaging not found")));
        } else {

            if (inputProductAmountDto.getMeasurementId() == null) {
                inputProductAmount.setMeasurement(null);
            } else {
                Optional<Measurement> measurement = measurementRepository.findById(inputProductAmountDto.getMeasurementId());
                if (measurement.isPresent()) {
                    inputProductAmount.setMeasurement(measurement.get());
                } else {
                    return new ApiResponse(messageLanguage.getMessageByLanguage("measurement.notfound"), false);
                }
            }

            if (inputProductAmountDto.getPackagingId() == null) {
                inputProductAmount.setPackaging(null);
            } else {
                Optional<Packaging> packaging = packagingRepository.findById(inputProductAmountDto.getPackagingId());
                if (packaging.isPresent()) {
                    inputProductAmount.setPackaging(packaging.get());
                } else {
                    return new ApiResponse(messageLanguage.getMessageByLanguage("packaging.notfound"), false);
                }
            }

            inputProductAmount.setInputProduct(inputProduct);
        }
        inputProductAmount.setAmountPackaging(inputProductAmountDto.getAmountPackaging());
        inputProductAmount.setAmount(inputProductAmountDto.getAmount());

        InputProductAmount savedInputProductAmount = inputProductAmountRepository.save(inputProductAmount);
        return new ApiResponse(true, savedInputProductAmount);
    }

    /***
     *
     * @param inputProduct
     * @param inputProductAmount
     * @return
     *
     */
//    public ApiResponse createInputProductLeftover(InputProduct inputProduct, InputProductAmount inputProductAmount) {
//        try {
//            if (inputProduct != null && inputProductAmount != null) {
//                InputProductLeftover newInputProductLeftover = new InputProductLeftover();
//
//                newInputProductLeftover.setPackaging(inputProductAmount.getPackaging());
//                newInputProductLeftover.setLeftoverPackaging(inputProductAmount.getAmountPackaging());
//                newInputProductLeftover.setLeftover(inputProductAmount.getAmount());
//                newInputProductLeftover.setInputProduct(inputProduct);
//
//                InputProductLeftover savedInputProductLeftover = inputProductLeftoverRepository.save(newInputProductLeftover);
//                return new ApiResponse("", true, savedInputProductLeftover);
//            }
//            return new ApiResponse("", false);
//        } catch (Exception e) {
//            return new ApiResponse("", false);
//        }
//    }

    /***
     *
     * @param inputProduct
     * @return
     *
     */
    public InputProductDto getInputProductDto(InputProduct inputProduct) {
        return new InputProductDto(
                inputProduct.getId(),
                inputProduct.getProduct().getId(),
                inputProduct.getPrice(),
                inputProduct.getProduct().getNameUz(),
                inputProduct.getProduct().getNameRu(),
                inputProduct.getProduct().getBarCode(),
                inputProduct.getPriceMeasurement().getId(),
                inputProduct.getPriceMeasurement().getNameUz(),
                inputProduct.getPriceMeasurement().getNameRu(),
                inputProduct.getDescription(),
                inputProduct.getInputTrade().getId(),
                inputProduct.getInputTrade().getNumber(),
                inputProduct.getInputTrade().getFactureNumber(),
                inputProduct.getWarehouse().getId(),
                inputProduct.getWarehouse().getName()
        );
    }

    public ApiResponse getInputProduct(UUID productId) {
        try {
            InputProduct inputProduct = inputProductRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("InputProduct not found"));
            return
                    new ApiResponse(true, getInputProductDto(inputProduct));
        } catch (Exception e) {
            return new ApiResponse("InputProduct topilmadi", false, new InputProduct());
        }
    }

    public PageableDto getAllInputProduct(Integer page, Integer size) {
        Page<InputProduct> allInputProducts = inputProductRepository.findAll(PageRequest.of(page, size));
        return new PageableDto(page,
                size,
                allInputProducts.getTotalPages(),
                allInputProducts.getTotalElements(),
                allInputProducts.stream().map(this::getInputProductDto).collect(Collectors.toList())
        );
    }

    public InputTradeDto getInputTradeDto(InputTrade inputTrade) {
        return new InputTradeDto(
                inputTrade.getId(),
                inputTrade.getSupplier().getId(),
                inputTrade.getSupplier().getName(),
                inputTrade.getCurrencyType().getId(),
                inputTrade.getCurrencyType().getNameUz(),
                inputTrade.getCurrencyType().getNameRu(),
                inputTrade.getNumber(),
                inputTrade.getFactureNumber(),
                inputTrade.getDate(),
                inputTrade.getDescription()
        );
    }

    public ApiResponse getInputTrade(UUID inputTradeId) {
        try {
            InputTrade inputTrade = inputTradeRepository.findById(inputTradeId).orElseThrow(() -> new ResourceNotFoundException("InputTrade not found"));
            return new ApiResponse(true, getInputTradeDto(inputTrade));
        } catch (Exception e) {
            return new ApiResponse(messageLanguage.getMessageByLanguage("trade.notfound"), false, new InputTrade());
        }
    }

    public PageableDto getAllInputTrade(Integer page, Integer size) {
        Page<InputTrade> allInputTrades = inputTradeRepository.findAll(PageRequest.of(page, size));
        return new PageableDto(page,
                size,
                allInputTrades.getTotalPages(),
                allInputTrades.getTotalElements(),
                allInputTrades.stream().map(this::getInputTradeDto).collect(Collectors.toList())
        );
    }
}

