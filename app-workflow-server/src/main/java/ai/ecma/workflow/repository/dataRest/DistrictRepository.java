package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.District;
import ai.ecma.workflow.projection.CustomDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "district", collectionResourceRel = "list", excerptProjection = CustomDistrict.class)
public interface DistrictRepository extends JpaRepository<District, Integer> {

    @RestResource(path = "byRegion")
    List<District> findAllByRegionId(@Param(value = "id") Integer id);
}
