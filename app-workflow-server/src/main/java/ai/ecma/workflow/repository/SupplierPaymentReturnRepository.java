package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.SupplierPaymentReturn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SupplierPaymentReturnRepository extends JpaRepository<SupplierPaymentReturn, UUID> {
    Page<SupplierPaymentReturn> findAllByInputTradeId(UUID inputTrade_id, Pageable pageable);
}
