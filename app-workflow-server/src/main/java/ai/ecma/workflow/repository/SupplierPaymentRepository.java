package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.SupplierPayment;
import ai.ecma.workflow.entity.SupplierPaymentReturn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SupplierPaymentRepository extends JpaRepository<SupplierPayment, UUID> {
    Page<SupplierPayment> findAllByInputTradeId(UUID inputTrade_id, Pageable pageable);

}
