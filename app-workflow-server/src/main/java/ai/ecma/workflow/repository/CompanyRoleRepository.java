package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.CompanyRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CompanyRoleRepository extends JpaRepository<CompanyRole, UUID> {

}
