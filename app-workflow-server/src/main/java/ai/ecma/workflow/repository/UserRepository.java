package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Company;
import ai.ecma.workflow.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByPhoneNumberAndEnabledTrue(String phoneNumber);

    Optional<User> findByIdAndEnabledTrue(UUID userId);

    Optional<User> findByPhoneNumber(String phoneNumber);


    boolean existsByPhoneNumber(String phoneNumber);

    @Query(value = "select * from users inner join user_company as uc on users.id = uc.user_id where uc.company_id=:companyId", nativeQuery = true)
    Page<User> getAllByCompany(@Param("companyId") UUID companyId, Pageable pageable);


}
