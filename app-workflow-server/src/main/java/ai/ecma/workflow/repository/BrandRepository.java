package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Brand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BrandRepository extends JpaRepository<Brand, UUID> {
    Page<Brand> findAllByCompanyId(UUID company_id, Pageable pageable);
}
