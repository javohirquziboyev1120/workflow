package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputProductReturn;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InputProductReturnRepository extends JpaRepository<InputProductReturn, UUID> {
}
