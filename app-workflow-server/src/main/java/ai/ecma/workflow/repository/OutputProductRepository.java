package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.OutputProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface OutputProductRepository extends JpaRepository<OutputProduct, UUID> {
    @Query(value = "select * from output_product op where op.id in (select ip.id from input_product ip where ip.product_id in (select p.id from product p where p.company_id = :companyId)) offset :page * :size limit :size", nativeQuery = true)
    List<OutputProduct> getAllByCompanyIdPageable(@Param("companyId") UUID companyId, @Param("page") int page, @Param("size") int size);
}
