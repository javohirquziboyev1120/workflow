package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.CustomerPaymentReturn;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CustomerPaymentReturnRepository extends JpaRepository<CustomerPaymentReturn, UUID> {

}
