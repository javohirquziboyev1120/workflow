package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.PriceType;
import ai.ecma.workflow.projection.CustomPriceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "priceType",collectionResourceRel = "list",excerptProjection = CustomPriceType.class)
public interface PriceTypeRepository extends JpaRepository<PriceType, UUID> {
}
