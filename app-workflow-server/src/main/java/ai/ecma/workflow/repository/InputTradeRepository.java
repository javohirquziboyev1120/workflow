package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputTrade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InputTradeRepository extends JpaRepository<InputTrade, UUID> {
}
