package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.CashBox;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CashBoxRepository extends JpaRepository<CashBox, UUID> {
}
