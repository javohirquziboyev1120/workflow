package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    Page<Product> findAllByCompanyId(UUID company_id, Pageable pageable);

    Optional<Product> findByIdAndCompanyId(UUID id, UUID company_id);
}
