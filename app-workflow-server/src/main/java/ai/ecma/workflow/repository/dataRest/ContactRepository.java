package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.Contact;
import ai.ecma.workflow.projection.CustomContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;
@CrossOrigin
@RepositoryRestResource(path = "contact", collectionResourceRel = "list", excerptProjection = CustomContact.class)
public interface ContactRepository extends JpaRepository<Contact, UUID> {
}
