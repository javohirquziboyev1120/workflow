package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.CurrencyType;
import ai.ecma.workflow.projection.CustomCurrencyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "currencyType", collectionResourceRel = "list", excerptProjection = CustomCurrencyType.class)
public interface CurrencyTypeRepository extends JpaRepository<CurrencyType, UUID> {
}
