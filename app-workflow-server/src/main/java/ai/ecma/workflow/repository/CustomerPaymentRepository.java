package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.CustomerPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CustomerPaymentRepository extends JpaRepository<CustomerPayment, UUID> {
    Page<CustomerPayment> findByOutputTradeId(UUID outputTrade_id, Pageable pageable);
}
