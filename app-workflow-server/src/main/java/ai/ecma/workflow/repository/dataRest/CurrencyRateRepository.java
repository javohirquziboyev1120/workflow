package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.CurrencyRate;
import ai.ecma.workflow.entity.CurrencyType;
import ai.ecma.workflow.projection.CustomCurrencyRate;
import ai.ecma.workflow.projection.CustomDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;
@RepositoryRestResource(path = "currencyRate", collectionResourceRel = "list", excerptProjection = CustomCurrencyRate.class)
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, UUID> {
    CurrencyRate findByFromCurrencyAndToCurrency(CurrencyType fromCurrency, CurrencyType toCurrency);
}
