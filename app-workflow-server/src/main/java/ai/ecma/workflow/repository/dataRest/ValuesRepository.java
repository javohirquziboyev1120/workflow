package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.Values;
import ai.ecma.workflow.projection.CustomValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "value", collectionResourceRel = "list", excerptProjection = CustomValue.class)
public interface ValuesRepository extends JpaRepository<Values, UUID> {
    @Query(nativeQuery = true,
            value = "select * from value where active=true and detail_id=:detail_id" )
    List<Values> findAllByDetailId(@Param("detail_id") UUID detail_id);}
