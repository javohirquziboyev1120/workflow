package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.Customer;
import ai.ecma.workflow.projection.CustomCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "customer", collectionResourceRel = "list", excerptProjection = CustomCustomer.class)
public interface CustomerRepository extends JpaRepository<Customer, UUID> {

}
