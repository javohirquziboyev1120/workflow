package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.OutputProductReturnAmount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OutputProductReturnAmountRepository extends JpaRepository<OutputProductReturnAmount, UUID> {
}
