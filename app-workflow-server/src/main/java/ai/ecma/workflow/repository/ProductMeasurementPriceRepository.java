package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.ProductMeasurementPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProductMeasurementPriceRepository extends JpaRepository<ProductMeasurementPrice, UUID> {
    @Query(value = "select * from product_measurement_price pmp where pmp.product_id in (select p.id from product p where p.company_id =:companyId) offset :page*:size limit :size", nativeQuery = true)
    List<ProductMeasurementPrice> getProductMeasurementPricePageable(@Param("companyId") UUID companyId, @Param("page") int page, @Param("size") int size);
}
