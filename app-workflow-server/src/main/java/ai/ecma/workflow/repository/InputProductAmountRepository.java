package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputProductAmount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface InputProductAmountRepository extends JpaRepository<InputProductAmount, UUID> {
    @Query(value = "select * from input_product_amount ipa where ipa.input_product_id in (select id from input_product ip where ip.product_id = :productId and ip.warehouse_id = :warehouseId) and ipa.packaging_id = :packagingId and ipa.amount_packaging >= :amountPackaging", nativeQuery = true)
    List<InputProductAmount> getAllInputProduct(@Param("productId") UUID productId, @Param("warehouseId") UUID warehouseId, @Param("packagingId") UUID packagingId, @Param("amountPackaging") Double amountPackaging);
}
