package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.Expense;
import ai.ecma.workflow.projection.CustomExpense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "expense", collectionResourceRel = "list", excerptProjection = CustomExpense.class)
public interface ExpenseRepository extends JpaRepository<Expense, UUID> {
}
