package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputProduct;
import ai.ecma.workflow.entity.InputTrade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InputProductRepository extends JpaRepository<InputProduct, UUID> {
    void deleteAllByInputTradeId(UUID inputTrade_id);
}
