package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.ExpenseType;
import ai.ecma.workflow.projection.CustomExpenseType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "expenseType", collectionResourceRel = "list", excerptProjection = CustomExpenseType.class)
public interface ExpenseTypeRepository extends JpaRepository<ExpenseType, UUID> {
}
