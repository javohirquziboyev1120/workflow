package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Attachment;
import ai.ecma.workflow.entity.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    Optional<AttachmentContent> findByAttachment(Attachment attachment);

    AttachmentContent getByAttachment(Attachment attachment);
}
