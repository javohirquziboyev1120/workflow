package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputProductReturnAmount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InputProductReturnAmountRepository extends JpaRepository<InputProductReturnAmount, UUID> {
}
