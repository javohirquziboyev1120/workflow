package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Packaging;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface PackagingRepository extends JpaRepository<Packaging, UUID> {
    Page<Packaging> findByProductId(UUID product_id, Pageable pageable);
    Optional<Packaging> findByIdAndProductId(UUID id, UUID product_id);
}
