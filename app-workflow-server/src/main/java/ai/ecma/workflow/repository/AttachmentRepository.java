package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    List<Attachment> findAllByNameAndSize(String name, long size);
}
