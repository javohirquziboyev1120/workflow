package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.PayType;
import ai.ecma.workflow.projection.CustomPayType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.UUID;

@CrossOrigin
@RepositoryRestResource(path = "payType", collectionResourceRel = "list", excerptProjection = CustomPayType.class)
public interface PayTypeRepository extends JpaRepository<PayType, UUID> {
}
