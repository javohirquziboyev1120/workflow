package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.SystemRole;
import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import ai.ecma.workflow.projection.CustomSystemRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Set;

@RepositoryRestResource(path = "role", collectionResourceRel = "list", excerptProjection = CustomSystemRole.class)
public interface SystemRoleRepository extends JpaRepository<SystemRole, Integer> {
    List<SystemRole> findAllBySystemRoleNameEnumIn(List<SystemRoleNameEnum> roleNames);
}
