package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.Note;
import ai.ecma.workflow.projection.CustomNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "note", collectionResourceRel = "list", excerptProjection = CustomNote.class)
public interface NoteRepository extends JpaRepository<Note, UUID> {
}
