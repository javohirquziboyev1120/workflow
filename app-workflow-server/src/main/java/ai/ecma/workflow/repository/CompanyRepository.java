package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface CompanyRepository extends JpaRepository<Company, UUID> {
//    List<Company> getAllCurrentCompany(UUID id);
    @Query(value = "select * from company c inner join user_company uc on uc.company_id=c.id and uc.user_id=:id", nativeQuery = true)
    List<Company> getAllCurrentCompany(@Param("id") UUID id);

}
