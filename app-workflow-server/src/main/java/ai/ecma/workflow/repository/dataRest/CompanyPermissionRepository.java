package ai.ecma.workflow.repository.dataRest;

import ai.ecma.workflow.entity.CompanyPermission;
import ai.ecma.workflow.entity.CompanyRole;
import ai.ecma.workflow.projection.CustomCompanyPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "companyPermission", collectionResourceRel = "list", excerptProjection = CustomCompanyPermission.class)
public interface CompanyPermissionRepository extends JpaRepository<CompanyPermission, UUID> {
//
//    @RestResource(path = "byCompanyRole")
//    List<CompanyPermission> findAllByCompanyRole(@Param("companyRole") CompanyRole companyRole);
}
