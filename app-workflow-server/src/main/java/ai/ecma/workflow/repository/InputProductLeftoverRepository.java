package ai.ecma.workflow.repository;

import ai.ecma.workflow.entity.InputProductLeftover;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface InputProductLeftoverRepository extends JpaRepository<InputProductLeftover, UUID> {

    Optional<InputProductLeftover> findByInputProduct_Id(UUID id);
}
