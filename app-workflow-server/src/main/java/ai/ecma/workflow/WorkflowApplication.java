package ai.ecma.workflow;

import ai.ecma.workflow.config.InitConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkflowApplication {

    public static void main(String[] args) {
        if (InitConfig.isStart()) {
            SpringApplication.run(WorkflowApplication.class, args);
        } else {
            System.err.println("Boshlang'ich ma'lumotlar kiritilmadi.");
        }
    }
}
