package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//JAVOHIR DATAREST//
public class CurrencyRate extends AbsEntity {//convertatsiya uchun
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CurrencyType fromCurrency;//USD(1),SUM(2)

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CurrencyType toCurrency;//SUM(2),USD(1)

    @Column(nullable = false)
    private Double formValue;//15,105600

    @Column(nullable = false)
    private Double toValue;//105500,10

    //bu convertatsiyani to'gri ishlashi uchun
// agar fromValue katta bo'lsa toValuedan - fromValue toValuega bo'linadi,kichik bo'lsa aksi
}
