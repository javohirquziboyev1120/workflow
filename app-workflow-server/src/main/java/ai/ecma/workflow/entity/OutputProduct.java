package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class OutputProduct extends AbsEntity {//Ombordan sotiladigan maxsulot

    @ManyToOne(fetch = FetchType.LAZY)
    private OutputTrade outputTrade;// QAYSI SAVDO UCHUN

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Warehouse warehouse;// Ombor

    @Column(nullable = false)
    private Double price;//narxi

    @OneToMany(mappedBy = "outputProduct", cascade = CascadeType.ALL)
    private List<OutputProductAmount> outputProductAmounts;//Ulchov birligi tonna metr, kg, gr, smm, mtr

    private String description;//xaqida

    //    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    //    private InputProduct inputProduct;//kiritilgan maxsulot("Masalan 10 ta maxsulot kirgan edi ushandan 5 ta sotildi")

    //    @Enumerated(EnumType.STRING)
    //    private PayStatus payStatus;

}
