package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class OutputProductReturnAmount extends AbsEntity {//Biror maxsulotning xolati yaxshi chiqmasa usha
    // maxsulotni pulini qaytarib berishimiz

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private OutputProductReturn outputProductReturn;// Qaysi maxsulot ekanligi

    @ManyToOne(fetch = FetchType.LAZY)
    private Measurement measurement;//o'lchov birligi

    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;//katta karopka

    private double amountPackaging;//10

    private double amount;//0
}
