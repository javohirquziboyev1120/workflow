package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Contact extends AbsEntity {//Kompaniyani yoki Omborning adrressi

    private String phoneNumber;

    private String email;

    private Float lan;

    private Float lat;

    private String street;

    @ManyToOne(fetch = FetchType.LAZY)
    private District district;


}
