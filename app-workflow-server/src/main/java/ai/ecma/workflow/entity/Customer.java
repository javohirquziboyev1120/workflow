package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//JAVOHIR DATAREST//
public class Customer extends AbsEntity {//Kompaniyaning mijozi

    private String name;

    private String email;

    private String phoneNumber;

    private String address;

    private boolean status;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company company;//Qaysi kompaniyaga mijoz ekanligi
}
