package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class InputTrade extends AbsEntity {//kirib keladigan maxsulot jarayoni
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;//yetkazib beruvchi  || agar uzi ishlab chiqorgan maxsulotni kiritsa supplier uzi buladi

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private CurrencyType currencyType;// valyuta kursi

    @Column(nullable = false)//generate qilyotganda bu nomer boshqa qaytarilmasligi kerak shuni hisoblab ketish kere!!!
    private String number;//Kirvotgan maxsulot nomeri

    private String factureNumber;//bilmadik

    private Timestamp date = new Timestamp(System.currentTimeMillis());//Qachon kiritilganini sanasi

    private String description;//Haqida about

}
