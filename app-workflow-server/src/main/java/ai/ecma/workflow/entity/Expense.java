package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Expense extends AbsNameEntity {

    @Column(nullable = false)
    private Double amount;

    private Timestamp time = new Timestamp(System.currentTimeMillis());

    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ExpenseType expenseType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CashBox cashBox;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private UserWarehouse userWarehouse;

    //agar warehouse null bulsa companyni harajatlari buladi aks holda companyni harajatlari
}
