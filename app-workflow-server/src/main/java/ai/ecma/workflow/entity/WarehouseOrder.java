package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WarehouseOrder extends AbsEntity {

    @ManyToOne
    private Warehouse warehouse;

    private double amount;
}
