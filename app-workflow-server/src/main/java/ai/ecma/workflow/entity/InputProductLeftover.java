package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class InputProductLeftover extends AbsEntity {//Omborda shu maxsulotdan qancha qolgani

    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;//katta karopka

    private double leftoverPackaging;//qolgan karobka soni

    private double leftover;// QOLGAN INPUTPRODUCT SONI ( DONADA )

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private InputProduct inputProduct;//qaysi maxsulot uchun son amount ekanligi

    //" bazaga qolgan maxsulotlar miqdorini topish uchun har bir karobkani ichidagi
    // measurement miqdori topib keyin leftOverPackaginga kupaytiramiz va chiqqan miqdorni leftOverga qushiladi va shu xisobdan ayiriladi"

}
