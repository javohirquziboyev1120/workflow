package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category extends AbsNameEntity {//productlarni tartiblar uchun alohida alohida bulimlar
    @Column(unique = true)
    private Integer index;//Tartib raqam yani birinchi urinda ikkinchi urinda turishi

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company company;//Qaysi kompaniya ekanligi

    @ManyToOne(fetch = FetchType.LAZY)
    private Category parent;//masalan Oziq - ovqat bulsa uning ichidagi kategoriyalar
    // yangi qushilvotgan kategory shu kategory ichida turishi: Oziq ovqat-> Quruq mevalar

    @JsonIgnore
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private List<Category> categories;
}
