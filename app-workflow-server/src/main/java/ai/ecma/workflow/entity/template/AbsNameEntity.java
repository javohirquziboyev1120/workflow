package ai.ecma.workflow.entity.template;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.MappedSuperclass;

@EqualsAndHashCode(callSuper = true)
@Data
@MappedSuperclass
public abstract class AbsNameEntity extends AbsEntity {

    private String nameUz;

    private String nameRu;

    private boolean active;
}
