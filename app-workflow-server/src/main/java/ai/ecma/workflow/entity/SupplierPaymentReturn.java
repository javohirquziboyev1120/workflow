package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//XAMROZJON
public class SupplierPaymentReturn extends AbsEntity {//Bizga yetkazuvchi tomonidan qaytariladigan tuvol

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private InputTrade inputTrade;//Qaysi savdo ekanligi

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PayType payType;// tulov turi

    @Column(nullable = false)
    private Double amount;//yetkaziib beruchining pul miqdori va uning valyutasi asosida kelgan miqdor

    @Column(nullable = false)
    private Double haqiqatda;//yetkazib beruvchining valyutasini uzimining valyutaga convert qilib olibgandagi tulov

    private Date date = new Date();

    private String description;
}
