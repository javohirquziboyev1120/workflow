package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"name", "company_id"}))//ZIYODULLA AKA
public class Warehouse extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @OneToOne
    private Contact contact;

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

}
