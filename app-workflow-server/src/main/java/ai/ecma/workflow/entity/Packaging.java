package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nameUz", "product_id"}),
        @UniqueConstraint(columnNames = {"nameRu", "product_id"})
})//JAVOHIR
public class Packaging extends AbsNameEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Measurement measurement;

    @Column(nullable = false)
    private Double value;// NECHTA PRODUCT SIG'ISHI

    private String description;
}
