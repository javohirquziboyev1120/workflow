package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Brand extends AbsNameEntity {//Companiyani brandi yani Artel, Samsung,Redmi,Xiaomi,
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Company company;//Qaysi kompaniyaga tegishli ekanligi

}
