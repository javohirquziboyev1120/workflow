package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nameUz", "company_id"}),
        @UniqueConstraint(columnNames = {"nameRu", "company_id"})
})
public class PriceType extends AbsNameEntity {// Yaqin insonlar uchun, eskilar uchun, Optomchilar uchun degan tushuncha

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Company company;
}
