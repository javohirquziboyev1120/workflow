package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Company extends AbsEntity {//User kompaniyalari

    @Column(nullable = false)
    private String name;//Kompaniya namemi

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Contact contact;//Qayerda turishi

//    @ManyToOne(fetch = FetchType.LAZY)
//    private User user;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<Warehouse> warehouses;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    private List<CompanyRole> companyRoles;

}


