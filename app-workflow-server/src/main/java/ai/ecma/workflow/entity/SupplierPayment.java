package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//XAMROZJON
public class SupplierPayment extends AbsEntity {//maxsulot yetkazib beruvchiga to'lagan pul

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private InputTrade inputTrade;//kiri

     @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Supplier supplier;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CashBox cashBox;//kassa turi

    private Double amount;//1000$

    private Double haqiqatda;//savdoning valuta turida qancha bo'lishi (Agar savdo currency bilan
    // cashbox currency bir xil bo'lmasa qo'lda kiritishini so'raymiz)

    private Date date = new Date();//sanasi

    private String description;//haqida
}
