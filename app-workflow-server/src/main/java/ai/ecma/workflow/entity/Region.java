package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntityInteger;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Region extends AbsNameEntityInteger {
    @JsonIgnore
    @OneToMany(mappedBy = "region", cascade = CascadeType.ALL)
    private List<District> districts;


}
