package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemRole implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private SystemRoleNameEnum systemRoleNameEnum;

    @Override
    public String getAuthority() {
        return systemRoleNameEnum.name();
    }
}
