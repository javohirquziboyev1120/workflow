package ai.ecma.workflow.entity.enums;

import java.util.Arrays;
import java.util.List;

public enum CompanyPermissionEnum {

    ADD_BRAND("Brend qo'shish", "s", "Brand", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_BRAND("Brendni tahrirlash", "s", "Brand", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_BRAND("Brendni o'chirish", "s", "Brand", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_CATEGORY("Category qo'shish", "s", "Category", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_CATEGORY("Categoryni tahrirlash", "s", "Category", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_CATEGORY("Categoryni o'chirish", "s", "Category", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_CURRENCY_RATE("Valyuta kursi qo'shish", "s", "Currency Rate", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_CURRENCY_RATE("Valyuta kursini tahrirlash", "s", "Currency Rate", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_CURRENCY_RATE("Valyuta kursini o'chirish", "s", "Currency Rate", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_CURRENCY_TYPE("Valyuta turi qo'shish", "s", "Currency Type", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_CURRENCY_TYPE("Valyuta turini tahrirlash", "s", "Currency Type", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_CURRENCY_TYPE("Valyuta turini o'chirish", "s", "Currency Type", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_CUSTOMER("Mijoz qo'shish", "s", "Customer", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_CUSTOMER("Mijozni tahrirlash", "s", "Customer", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_CUSTOMER("Mijozni o'chirish", "s", "Customer", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_SUPPLIER("Yetkazib beruvchi qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_SUPPLIER("Yetkazib beruvchini tahrirlash", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_SUPPLIER("Yetkazib beruvchini o'chirish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_DETAIL("Detal qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_DETAIL("Detalni tahrirlash", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_DETAIL("Detalni o'chirish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_VALUES("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_VALUES("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_VALUES("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_DISTRICT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_DISTRICT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_DISTRICT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_REGION("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_REGION("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_REGION("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_MEASUREMENT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_MEASUREMENT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_MEASUREMENT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_NOTE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_NOTE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_NOTE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_PACKAGING("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_PACKAGING("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_PACKAGING("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_PAY_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_PAY_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_PAY_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_PRICE_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_PRICE_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_PRICE_TYPE("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_COMPANY("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_COMPANY("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_COMPANY("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    ADD_PRODUCT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    EDIT_PRODUCT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),
    DELETE_PRODUCT("Maxsulot qo'shish", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    CREATE_INPUT_PRODUCT_WAREHOUSE("Maxsulot kirish jarayoni", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    EDIT_INPUT_TRADE("Maxsulot kirish jarayoni", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    EDIT_INPUT_PRODUCT("Maxsulot kirish jarayonini tahrirlash", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER)),

    EDIT_INPUT_PRODUCT_AMOUNT("Maxsulot kirishdagi miqdorini tahrirlash", "s", "", Arrays.asList(SystemRoleNameEnum.ROLE_ADMIN, SystemRoleNameEnum.ROLE_SUPER_ADMIN, SystemRoleNameEnum.ROLE_USER));

    public String nameUz;
    public String nameRu;
    public List<SystemRoleNameEnum> roleNames;
    public String generalName;

    CompanyPermissionEnum(String nameUz, String nameRu, String generalName, List<SystemRoleNameEnum> roleNames) {
        this.nameUz = nameUz;
        this.nameRu = nameRu;
        this.generalName = generalName;
        this.roleNames = roleNames;
    }
}
