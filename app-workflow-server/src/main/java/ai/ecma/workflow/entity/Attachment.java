package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Attachment extends AbsEntity {
    private String name;
    private String type;
    private long size;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "attachment", cascade = CascadeType.ALL)
    private AttachmentContent attachmentContent;

    public Attachment(String name, String type, long size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

}
