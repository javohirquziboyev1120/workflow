package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nameUz", "currency_type_id"}),
        @UniqueConstraint(columnNames = {"nameRu", "currency_type_id"})
})
public class PayType extends AbsNameEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private CurrencyType currencyType;

    @ManyToOne(fetch = FetchType.LAZY)
    private CashBox cashBox;
}
