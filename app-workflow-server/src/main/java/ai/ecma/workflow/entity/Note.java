package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Note extends AbsNameEntity {

    private Timestamp noteTime;//shu ishning qachon bajarilishi kerakligi

    private Integer repeat;//qancha maboynida ogohlantirib turilishi

    private Integer noteDuration;//qancha maboynida ogohlantirib turilishi

    private boolean enabled;//bu ishning har doim shu vaqtda bajarilishi yoki bajarilmasligi
}
