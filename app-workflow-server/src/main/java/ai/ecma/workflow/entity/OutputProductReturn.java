package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class OutputProductReturn extends AbsEntity {//Biror maxsulotning xolati yaxshi chiqmasa usha
    // maxsulotni pulini qaytarib berishimiz

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private OutputProduct outputProduct;// Qaysi maxsulot ekanligi

    @OneToMany(mappedBy = "outputProductReturn", cascade = CascadeType.ALL)
    private List<OutputProductReturnAmount> outputProductReturnAmounts;

    private boolean defect;

    @Column(nullable = false)
    private String description;// nima uchun qaytarilgani
}
