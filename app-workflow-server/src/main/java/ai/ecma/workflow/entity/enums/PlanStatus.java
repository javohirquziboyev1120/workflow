package ai.ecma.workflow.entity.enums;

public enum PlanStatus {
    PLANNING,
    REALIZED,
    REJECTED
}
