package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class OutputProductAmount extends AbsEntity {//Ombordan sotiladigan maxsulot

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private OutputProduct outputProduct;// QAYSI SAVDO UCHUN

    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;// paket

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Measurement measurement;//Ulchov birligi tonna metr, kg, gr, smm, mtr

    @Column(nullable = false)
    private Double amountPackaging;//paketning soni

    @Column(nullable = false)
    private Double amount;//Miqdori soni

    private String description;//xaqida

//    @Enumerated(EnumType.STRING)
//    private PayStatus payStatus;

}
