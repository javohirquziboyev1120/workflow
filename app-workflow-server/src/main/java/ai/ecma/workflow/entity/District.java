package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntityInteger;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class District extends AbsNameEntityInteger {//qarshi

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Region region;//Qashqadaryo
}
