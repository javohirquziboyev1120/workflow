package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"barCode", "company_id"}))
public class Product extends AbsNameEntity {//JAVOHIR

    @Column(nullable = false)
    private String barCode;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private Brand brand;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Category category;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Attachment> photos;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Company company;

    @ManyToMany
    @JoinTable(name = "product_value",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "value_id")})
    private List<Values> values;

    @ManyToMany
    @JoinTable(name = "product_measurement",
            joinColumns = {@JoinColumn(name = "product_id")},
            inverseJoinColumns = {@JoinColumn(name = "measurement_id")})
    private List<Measurement> measurements;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<Packaging> packagingList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<ProductPriceType> productPriceTypes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
    private List<ProductMeasurementPrice> productMeasurementPrices;

}
