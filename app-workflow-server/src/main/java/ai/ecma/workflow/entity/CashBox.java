package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ZIYODULLA AKA
public class CashBox extends AbsNameEntity {//bu kassa bo'lib unda payType lar to'plami bo'ladi,
    // currencyTypelari bir xil bo'lishi kerak
    @OneToMany(mappedBy = "cashBox", cascade = CascadeType.ALL)
    private List<PayType> payTypes;
}
