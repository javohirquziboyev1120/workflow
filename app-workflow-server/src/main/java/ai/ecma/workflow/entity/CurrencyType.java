package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"nameUz", "company_id"}),
        @UniqueConstraint(columnNames = {"nameRu", "company_id"}) // har bir kompaniyada bitta nomi bo'lishi kerak
})
public class CurrencyType extends AbsNameEntity {//Pul birligi

    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Company company;
}
