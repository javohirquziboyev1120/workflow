package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//XAMROZJON
public class CustomerPayment extends AbsEntity {//USD (1900$)//1 | bizga kelib tushgan pul

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private OutputTrade outputTrade;//1 amalga oshirilgan savdo

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PayType payType;//naqd dollar,naqd so'm(2),visa card(1) | to'lov turi

    private Double amount;//70 0$, 5 000 000, 300$ | istoriya(client kiritgan default summa, yani USD, SUM, RUBL)

    private Double inFactAmount;//700$, 473.48$, 300$  | Hisobot uchun(clientning kiritgan valyutasini savdo valyutasiga moslab olgandagi narxi)

    private Timestamp date = new Timestamp(System.currentTimeMillis());//sana

    private String description;
}
