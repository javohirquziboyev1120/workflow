package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class InputProductReturnAmount extends AbsEntity {//Kiritilgan maxsulot soni

    @ManyToOne(fetch = FetchType.LAZY)
    private Measurement measurement;//o'lchov birligi

    @ManyToOne(fetch = FetchType.LAZY)
    private Packaging packaging;//katta karopka

    private double amountPackaging;//10

    private double amount;//0

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private InputProductReturn inputProductReturn;//1,

}
