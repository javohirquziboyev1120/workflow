package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.enums.CompanyPermissionEnum;
import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR UZI BILADI
public class CompanyPermission extends AbsEntity implements GrantedAuthority {//Kompaniyada ishlayotgan ishchilarning qiladigan ishlarini bellgilash

    @Enumerated(EnumType.STRING)
    private CompanyPermissionEnum companyPermissionEnum;//Bular sistemada yagona: ADD_PRODUCT

    @Override
    public String getAuthority() {
        return companyPermissionEnum.name();
    }
}
