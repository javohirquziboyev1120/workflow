package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class InputProduct extends AbsEntity {//Omborga kirgan maxsulot

    @Column(nullable = false)
    private Double price;//maxsulot narxi

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private InputTrade inputTrade;//Kimdan va  qayerdan kirib kelayotgan maxsulot

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Product product;//maxsulot

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Warehouse warehouse;//Qaysi omborga ekanligi

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Measurement priceMeasurement;//Kirim narxi qaysi o'lchov birligida ekanligi

    @OneToMany(mappedBy = "inputProduct", cascade = CascadeType.ALL)
    private List<InputProductAmount> inputProductAmounts;

//    @OneToMany(mappedBy = "inputProduct", cascade = CascadeType.ALL)
//    private List<InputProductLeftover> inputProductLeftovers;
}
