package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class ProductPriceType extends AbsEntity {//PriceType bilan birlashgan joyi masalan olmani 500 sumdan Ixtiyorga ber degan joyi
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Product product;//olma

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Measurement measurement;//QAYSI O'LCHOV BIRLIGI UCHUN

    private double price;//3000

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PriceType priceType;//Ixtiyor uchun
}
