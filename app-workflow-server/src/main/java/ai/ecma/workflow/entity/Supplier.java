package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Supplier extends AbsEntity {// maxsulot yetkazib beruvchi

    private String name;// firma yoki kishi nomi

    private String email;

    private String phoneNumber;

    private String address;

    private boolean status;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Company company;// qaysi kompaniyaga tegishliligi
}
