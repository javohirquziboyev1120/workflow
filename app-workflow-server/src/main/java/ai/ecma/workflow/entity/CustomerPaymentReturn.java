package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CustomerPaymentReturn extends AbsEntity {//Mijozga qaytaradigan pulimiz!!!

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private OutputTrade outputTrade;//Qaysi savdo uchun qaytarib berishimiz

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CashBox cashBox;//qaysi kassa turidan qaytarib berishimiz

    private Double amount;// qaytariladigan summa

    private Double inFactAmount;//700$, 473.48$, 300$  | Hisobot uchun(clientning kiritgan valyutasini savdo valyutasiga moslab olgandagi narxi)

    private Date date = new Date();//maxsulot qaytarulgan sanasi

    private String description;//Niima uchunligi haqida
}
