package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class InputProductReturn extends AbsEntity {//yetkazib beruvchiga qaytariladigan maxsulot

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private InputProduct inputProduct;// qaysi maxsulot ekanni

    @OneToMany(mappedBy = "inputProductReturn")
    private List<InputProductReturnAmount> inputProductReturnAmounts;

    private boolean defect;// yaroqlimi yoki yaroqsiz

    @Column(nullable = false)
    private String description;//Shu maxsulotga nima bulganini

    private Timestamp date = new Timestamp(System.currentTimeMillis());// qaschon qaytarilgani

    //Agar inpurProductAmount ga kirgan maxsulot o'lchovida packaging bulsa qaytarilyotganda shu packaging bilan qaytaramiz
    // aks holta esa yani measurement bulsa measurmen bilan qaytariladi packaging 0 ga teng bulsa
}
