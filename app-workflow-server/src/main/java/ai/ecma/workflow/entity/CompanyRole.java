package ai.ecma.workflow.entity;


import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//IXTIYOR
public class CompanyRole extends AbsEntity {//User uzi uchun yaratadigan role

    private String name;//Role name

    @Column(length = 500)
    private String description;// rolening vazifasini haqida

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;//qaysi kompaniyaga tegishli ekanligi

    @ManyToMany
    @JoinTable(name = "company_role_company_permission",
            joinColumns = {@JoinColumn(name = "company_role_id")},
            inverseJoinColumns = {@JoinColumn(name = "company_permission_id")})
    private List<CompanyPermission> companyPermissions;

    public CompanyRole(String name, String description, Company company) {
        this.name = name;
        this.description = description;
        this.company = company;
    }
}
