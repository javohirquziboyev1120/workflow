package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class OutputTrade extends AbsEntity {//Mijozga sotiladigan savdo
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Customer customer;//Mijoz

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CurrencyType currencyType;// Savdoning pul birligi

    private Date date = new Date();//qachon sotiladigan sanasi

    private String description;//Shu maxsulot haqida malumot

    @Column(nullable = false)
    private String number;

    @OneToMany(mappedBy = "outputTrade", cascade = CascadeType.ALL)
    private List<OutputProduct> outputProducts;

//    //BU IXTIYOTNING IXTIYORIDA
//    @Enumerated(EnumType.STRING)
//    private PlanStatus planStatus = PlanStatus.REALIZED;//Statuslar yani buyurtmani rejalashtirish
//
//    private Date planningDate;//rejalashtirilgan sana
//
//    private Date completedDate;// reja amalga oshgan sana
}
