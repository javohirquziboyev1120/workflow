package ai.ecma.workflow.entity.enums;

public enum PayStatus {
    PARTIAL, PAID, DEBT
}
