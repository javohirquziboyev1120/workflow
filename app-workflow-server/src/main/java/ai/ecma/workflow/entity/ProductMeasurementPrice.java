package ai.ecma.workflow.entity;

import ai.ecma.workflow.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity//ABDURAZZOQ
public class ProductMeasurementPrice extends AbsEntity {// SHU O'LCHOV BIRLIGI BO'YICHA MAXSULOT NARXI
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Product product;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Measurement measurement;

    private double minPrice;//SOTISH MUMKIN BO'LGAN ENG KAM NARX

    private double inputPrice;//OMBORDA MAVJUD BO'LGANLARNING ENG QIMMATI  NARXI

    private double sellPrice;//MAXSULTONING SHU O'LCHOV BIRLIGIDAGI SOTILADIGAN NARXI
}
