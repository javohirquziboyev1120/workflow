package ai.ecma.workflow.controller;

import ai.ecma.workflow.entity.Attachment;
import ai.ecma.workflow.entity.AttachmentContent;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.repository.AttachmentContentRepository;
import ai.ecma.workflow.repository.AttachmentRepository;
import ai.ecma.workflow.security.CurrentUser;
import ai.ecma.workflow.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/file")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @PostMapping
    public ApiResponse uploadFile(MultipartHttpServletRequest request, @CurrentUser User user) throws IOException {
        return attachmentService.uploadFile(request, user);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id, HttpServletResponse response) {
        return attachmentService.getAttachmentContent(id, response);
    }

    @GetMapping("/content/{id}")
    public HttpEntity<?> getFileContent(@PathVariable UUID id, HttpServletResponse response) {

        return ResponseEntity.ok(new ApiResponse("", "attachmentContent", true, attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("attachment not found", "id", id))));
    }

    @PermitAll
    @GetMapping("/deleteByNameAndSize/{name}/{size}")
    public HttpEntity<?> deleteByNameAndSize(@PathVariable(value = "name") String name, @PathVariable(value = "size") long size, @CurrentUser User user) {
        List<Attachment> attachments = attachmentRepository.findAllByNameAndSize(name, size);
        if (attachments.size() > 0) {
            attachments.forEach(attachment -> {
                Optional<AttachmentContent> attachmentContentOptional = attachmentContentRepository.findByAttachment(attachment);
                if (attachmentContentOptional.isPresent()) {
                    attachmentContentRepository.delete(attachmentContentOptional.get());
                }
                attachmentRepository.delete(attachment);

            });
            return ResponseEntity.ok(new ApiResponse("", "file deleted", true));
        }
        return ResponseEntity.ok(new ApiResponse("", "file not found", false));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> delAttachment(@PathVariable UUID id) {
        Optional<Attachment> byId = attachmentRepository.findById(id);
        if (byId.isPresent()) {
            Optional<AttachmentContent> byAttachment = attachmentContentRepository.findByAttachment(byId.get());
            if (byAttachment.isPresent()) {
                attachmentContentRepository.delete(byAttachment.get());
                attachmentRepository.delete(byId.get());
                return ResponseEntity.ok(new ApiResponse("", "deleted", true));

            }
        }
        return ResponseEntity.ok(new ApiResponse("", "Error", false));
    }
}
