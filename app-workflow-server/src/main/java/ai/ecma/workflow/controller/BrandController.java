package ai.ecma.workflow.controller;

import ai.ecma.workflow.entity.Brand;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.BrandDto;
import ai.ecma.workflow.repository.BrandRepository;
import ai.ecma.workflow.service.BrandService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/brand")
public class BrandController {

    @Autowired
    BrandService brandService;
    @Autowired
    BrandRepository brandRepository;

    @PostMapping
    public HttpEntity<?> addOrEditBrand(@RequestBody BrandDto request) {
        return ResponseEntity.ok(brandService.addOrEditBrand(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getBrand(@PathVariable UUID id) {
        Brand brand = brandRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getBrand", "id", id));
        return ResponseEntity.ok(brandService.getBrand(brand));
    }

    @GetMapping
    public HttpEntity<?> getBrands(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                   @RequestParam(value = "companyId", defaultValue = "") UUID companyId) {
        return ResponseEntity.ok(brandService.getBrands(page, size, companyId));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteBrand(@PathVariable UUID id){
        return ResponseEntity.ok( brandService.deleteBrand(id));
    }
}
