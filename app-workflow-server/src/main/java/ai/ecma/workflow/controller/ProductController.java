package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.ProductDto;
import ai.ecma.workflow.service.ProductService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping
    public HttpEntity<?> saveProduct(@RequestBody ProductDto dto) {
        ApiResponse apiResponse = productService.addAndEditProduct(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping
    public HttpEntity<?> editProduct(@RequestBody ProductDto dto,@RequestParam(value = "companyId") UUID companyId) {
        ApiResponse apiResponse = productService.addAndEditProduct(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/page")
    public HttpEntity<?> getProductPage(
                                            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                             @RequestParam(value = "companyId") UUID companyId
                                             ) {
        return ResponseEntity.ok(productService.getProductPage(companyId, page, size));
    }

    @GetMapping("/getValues/{detailId}")
    public HttpEntity<?> getStatesByServiceId(@PathVariable UUID detailId) {
        return ResponseEntity.ok(productService.getValuesByDetail(detailId));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteProduct(@PathVariable UUID id) {
        return ResponseEntity.ok(productService.deleteProduct(id));
    }


}
