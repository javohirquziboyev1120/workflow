package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.ProductPriceTypeDto;
import ai.ecma.workflow.service.ProductPriceTypeService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/productPriceType")
public class ProductPriceTypeController {
    @Autowired
    ProductPriceTypeService productPriceTypeService;

    @PostMapping
    public HttpEntity<?> saveProductPriceType(@RequestBody ProductPriceTypeDto productPriceTypeDto) {
        ApiResponse apiResponse = productPriceTypeService.saveProductPriceType(productPriceTypeDto);
        return ResponseEntity.ok(apiResponse.isSuccess() ? 201 : 409);
    }

    @GetMapping("/page")
    public HttpEntity<?> getProductPriceTypePageable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                     @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                     @RequestParam UUID companyId) {
        return ResponseEntity.ok(productPriceTypeService.getProductPriceTypePageable(page, size, companyId));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteProductPriceType(@PathVariable UUID id) {
        return ResponseEntity.ok(productPriceTypeService.deleteProductPriceType(id));
    }

}
