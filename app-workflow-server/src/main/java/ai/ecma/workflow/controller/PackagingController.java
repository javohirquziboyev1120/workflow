package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.PackagingDto;
import ai.ecma.workflow.service.PackagingService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/packaging")
public class PackagingController {
    @Autowired
    PackagingService packagingService;

    @PostMapping
    public HttpEntity<?> savePackaging(@RequestBody PackagingDto dto) {
        ApiResponse apiResponse = packagingService.addAndEditPackaging(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping
    public HttpEntity<?> editPackaging(@RequestBody PackagingDto dto) {
        ApiResponse apiResponse = packagingService.addAndEditPackaging(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/page")
    public HttpEntity<?> getPackagingPage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                             @RequestParam(value = "productId") UUID productId) {
        return ResponseEntity.ok(packagingService.getPackagingPage(productId, page, size));
    }

    @DeleteMapping
        public HttpEntity<?> deletePackaging(@RequestParam(name = "product") UUID id) {
        return ResponseEntity.ok(packagingService.deletePackaging(id));
    }


}
