package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.OutputProductDto;
import ai.ecma.workflow.service.OutputProductService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/outputProduct")
public class OutputProductController {
    @Autowired
    OutputProductService outputProductService;

    @PostMapping
    public HttpEntity<?> saveOutputProduct(@RequestBody OutputProductDto outputProductDto) {
        ApiResponse apiResponse = outputProductService.saveOutputProduct(outputProductDto);
        return ResponseEntity.ok(apiResponse.isSuccess() ? 201 : 409);
    }

    @GetMapping("/page")
    public HttpEntity<?> getOutputProductPageable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                  @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                  @RequestParam UUID companyId) {
        ApiResponse apiResponse = outputProductService.getOutputProductPageable(page, size, companyId);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOutputProduct(@PathVariable UUID id) {
        ApiResponse apiResponse = outputProductService.deleteOutputProduct(id);
        return ResponseEntity.ok(apiResponse);
    }

}
