package ai.ecma.workflow.controller;

import ai.ecma.workflow.entity.Company;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.CompanyDto;
import ai.ecma.workflow.repository.CompanyRepository;
import ai.ecma.workflow.security.CurrentUser;
import ai.ecma.workflow.service.CompanyService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
    @RequestMapping("/api/company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @Autowired
    CompanyRepository companyRepository;

    @PostMapping
    public HttpEntity<?> addAndEditCompany(@CurrentUser User user, @RequestBody CompanyDto companyDto) {
        return ResponseEntity.ok(companyService.addAndEditCompany(user, companyDto));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCompany(@PathVariable UUID id) {
        Company company = companyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getCompany", "id", id));
        return ResponseEntity.ok(companyService.getCompanyDto(company));
    }
    @GetMapping("/page")
    public HttpEntity<?> getCompanyPage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                             @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(companyService.getCompanyPage(page, size));
    }
}
