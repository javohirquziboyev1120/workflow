package ai.ecma.workflow.controller;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.entity.SystemRole;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.entity.enums.SystemRoleNameEnum;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.UserDto;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.security.CurrentUser;
import ai.ecma.workflow.service.UserService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    MessageLanguage messageLanguage;

    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        return ResponseEntity.ok(new ApiResponse("Mana user", true, user));
    }

    //    @PreAuthorize("hasAnyRole('')")
    @PostMapping
    public HttpEntity<?> createUser(@RequestBody UserDto dto, @CurrentUser User user) {
        ApiResponse response = userService.addUser(dto, user);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping
    public HttpEntity<?> getUsers(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(value = "companyId", defaultValue = "") UUID companyId,
            @CurrentUser User user) {
        return ResponseEntity.ok(userService.getUsers(page, size, user, companyId));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getUserById(@PathVariable UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUserById", "id", id));
        return ResponseEntity.ok(userService.getUser(user));
    }

    @DeleteMapping("/id")
    public HttpEntity<?> deleteUser(@PathVariable UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", null));
        if (getRole(user.getSystemRoles())) {
            userRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse(messageLanguage.getMessageByLanguage("deleted"), true));
        } else {
            return ResponseEntity.ok(new ApiResponse(messageLanguage.getMessageByLanguage("error"), false));
        }
    }

    private boolean getRole(List<SystemRole> systemRoles) {
        int count = 0;
        for (SystemRole role : systemRoles) {
            if (role.getSystemRoleNameEnum().toString().equals(SystemRoleNameEnum.ROLE_USER)) {
                count++;
            }
        }
        return count > 0 ? true : false;
    }

    @PutMapping("/edit")
    public HttpEntity<?> editUser(@RequestBody UserDto dto, @CurrentUser User user) {
        ApiResponse response = userService.editUser(dto, user);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(response);
    }

}
