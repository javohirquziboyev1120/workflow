package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.OutputProductReturnDto;
import ai.ecma.workflow.service.OutputProductReturnService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/outputProductReturn")
public class OutputProductReturnController {
    @Autowired
    OutputProductReturnService outputProductReturnService;

    @PostMapping
    public HttpEntity<?> saveOutputReturnProduct(@RequestBody OutputProductReturnDto outputProductReturnDto) {
        ApiResponse apiResponse = outputProductReturnService.saveOutputProductReturn(outputProductReturnDto);
        return ResponseEntity.ok(apiResponse.isSuccess() ? 201 : 409);
    }

    @GetMapping("/page")
    public HttpEntity<?> getOutputProductReturnPageable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                @RequestParam UUID companyId) {
        ApiResponse apiResponse = outputProductReturnService.getOutputProductReturnPageable(page, size, companyId);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOutputProductReturn(@PathVariable UUID id) {
        ApiResponse apiResponse = outputProductReturnService.deleteOutputProductReturn(id);
        return ResponseEntity.ok(apiResponse);
    }

}
