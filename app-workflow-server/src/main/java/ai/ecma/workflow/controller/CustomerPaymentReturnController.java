package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CustomerPaymentReturnDto;
import ai.ecma.workflow.service.CustomerPaymentReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/customerPaymentReturn")
public class CustomerPaymentReturnController {
    @Autowired
    CustomerPaymentReturnService service;

    @PostMapping
    public HttpEntity<?> saveReturnPayment(@RequestBody CustomerPaymentReturnDto dto) {
        ApiResponse apiResponse = service.addAndEditCustomerPaymentReturn(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping
    public HttpEntity<?> EditReturnPayment(@RequestBody CustomerPaymentReturnDto dto) {
        ApiResponse apiResponse = service.addAndEditCustomerPaymentReturn(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @DeleteMapping
    public HttpEntity<?> deleteReturnPayment(@RequestParam(name = "customerPaymentReturn") UUID id) {
        return ResponseEntity.ok(service.deleteCustomerPaymentReturn(id));
    }
}
