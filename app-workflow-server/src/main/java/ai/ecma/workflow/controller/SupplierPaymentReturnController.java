package ai.ecma.workflow.controller;

import ai.ecma.workflow.entity.SupplierPaymentReturn;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.SupplierPaymentReturnDto;
import ai.ecma.workflow.repository.SupplierPaymentReturnRepository;
import ai.ecma.workflow.service.SupplierPaymentReturnService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/supplierPaymentReturn")
public class SupplierPaymentReturnController {

    @Autowired
    SupplierPaymentReturnRepository supplierPaymentReturnRepository;
    @Autowired
    SupplierPaymentReturnService supplierPaymentReturnService;

    @PostMapping
    public HttpEntity<?> addOrEditSupplierPaymentReturn(@RequestBody SupplierPaymentReturnDto supplierPaymentReturnDto) {
        return ResponseEntity.ok(supplierPaymentReturnService.addOrEditSupplierPaymentReturn(supplierPaymentReturnDto));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getSupplierPaymentReturn(@PathVariable UUID id) {
        SupplierPaymentReturn supplierPaymentReturn = supplierPaymentReturnRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getSupplierPaymentReturn", "id", id));
        return ResponseEntity.ok(supplierPaymentReturnService.getSupplierPaymentReturn(supplierPaymentReturn));
    }

    @GetMapping
    public HttpEntity<?> getSupplierPaymentReturns(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(value = "inputTradeId", defaultValue = "") UUID inputTadeId) {
        return ResponseEntity.ok(supplierPaymentReturnService.getSupplierPaymentReturns(inputTadeId, page, size));
    }
}
