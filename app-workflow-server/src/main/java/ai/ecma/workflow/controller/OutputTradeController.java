package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.OutputTradeDto;
import ai.ecma.workflow.service.OutputTradeService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/outputTrade")
public class OutputTradeController {
    @Autowired
    OutputTradeService outputTradeService;

    @PostMapping
    public HttpEntity<?> saveOutputTrade(@RequestBody OutputTradeDto outputTradeDto) {
        ApiResponse apiResponse = outputTradeService.saveOutputTrade(outputTradeDto);
        return ResponseEntity.ok(apiResponse.isSuccess() ? 201 : 409);
    }

    @GetMapping("/page")
    public HttpEntity<?> getOutputTradePageable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                     @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                     @RequestParam UUID companyId) {
        ApiResponse apiResponse = outputTradeService.getOutputTradePageable(page, size, companyId);
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOutputTrade(@PathVariable UUID id) {
        ApiResponse apiResponse = outputTradeService.deleteOutputTrade(id);
        return ResponseEntity.ok(apiResponse);
    }
}
