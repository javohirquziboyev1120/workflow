package ai.ecma.workflow.controller;

import ai.ecma.workflow.entity.SupplierPayment;
import ai.ecma.workflow.exception.ResourceNotFoundException;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.SupplierPaymentDto;
import ai.ecma.workflow.repository.SupplierPaymentRepository;
import ai.ecma.workflow.service.SupplierPaymentService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/supplierPayment")
public class SupplierPaymentController {

    @Autowired
    SupplierPaymentService supplierPaymentService;
    @Autowired
    SupplierPaymentRepository supplierPaymentRepository;

    @PostMapping
    public HttpEntity<?> addOrEditSupplierPayment(@RequestBody SupplierPaymentDto supplierPaymentDto){
        return ResponseEntity.ok(supplierPaymentService.addOrEditSupplierPayment(supplierPaymentDto));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getSupplierPayment(@PathVariable UUID id){
        SupplierPayment supplierPayment = supplierPaymentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("SupplierPayment", "id", id));
        return ResponseEntity.ok(supplierPaymentService.getSupplierPayment(supplierPayment));
    }

    @GetMapping
    public HttpEntity<?> getSupplierPayments(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
            @RequestParam(value = "inputTradeId", defaultValue = "") UUID inputTradeId){
        return ResponseEntity.ok(supplierPaymentService.getSupplierPayments(page, size, inputTradeId));
    }
}
