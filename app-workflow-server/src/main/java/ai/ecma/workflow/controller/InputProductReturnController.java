package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.InputProductReturnDto;
import ai.ecma.workflow.service.InputProductReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/input/return")
public class InputProductReturnController {

    @Autowired
    InputProductReturnService inputProductReturnService;

    @PostMapping
    public HttpEntity<?> createInputProductReturn(
            @RequestBody InputProductReturnDto inputProductReturnDto
    ) {
        ApiResponse inputProductReturn = inputProductReturnService.createInputProductReturn(inputProductReturnDto);
        return ResponseEntity.status(inputProductReturn.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(inputProductReturn);
    }
}
