package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.OutputProductDto;
import ai.ecma.workflow.payload.ProductMeasurementPriceDto;
import ai.ecma.workflow.service.OutputProductService;
import ai.ecma.workflow.service.ProductMeasurementPriceService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/productMeasurementPrice")
public class ProductMeasurementPriceController {
    @Autowired
    ProductMeasurementPriceService productMeasurementPriceService;

    @PostMapping
    public HttpEntity<?> saveProductMeasurementPrice(@RequestBody ProductMeasurementPriceDto productMeasurementPriceDto) {
        ApiResponse apiResponse = productMeasurementPriceService.saveProductMeasurementPrice(productMeasurementPriceDto);
        return ResponseEntity.ok(apiResponse.isSuccess()?201:409);
    }

    @GetMapping("/page")
    public HttpEntity<?> getProductMeasurementPricePageable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                     @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                     @RequestParam UUID companyId) {
        return ResponseEntity.ok(productMeasurementPriceService.getProductMeasurementPrice(page, size, companyId));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteProductMeasurementPrice(@PathVariable UUID id) {
        return ResponseEntity.ok(productMeasurementPriceService.deleteproductMeasurementPrice(id));
    }

}
