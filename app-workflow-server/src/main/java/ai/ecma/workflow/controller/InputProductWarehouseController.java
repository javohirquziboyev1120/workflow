package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.*;
import ai.ecma.workflow.service.InputProductWarehouseService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/api/input")
@RestController
public class InputProductWarehouseController {

    @Autowired
    InputProductWarehouseService inputProductWarehouseService;

    @PostMapping
    public HttpEntity<?> createInputProductWarehouse(
            @Valid @RequestBody InputTradeDto inputTradeDto
    ) {
        ApiResponse apiResponse = inputProductWarehouseService.incomeProcess(inputTradeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/edit/trade/{tradeId}")
    public HttpEntity<?> updateInputTrade(
            @PathVariable UUID tradeId,
            @RequestBody InputTradeDto inputTradeDto
    ) {
        ApiResponse apiResponse = inputProductWarehouseService.updateInputTrade(tradeId, inputTradeDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/edit/input-product/{productId}")
    public HttpEntity<?> updateInputProduct(
            @PathVariable UUID productId,
            @RequestBody InputProductDto inputProductDto
    ) {
        ApiResponse apiResponse = inputProductWarehouseService.updateInputProduct(productId, inputProductDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/edit/input-product-amount/{id}")
    public HttpEntity<?> updateInputProductAmount(
            @PathVariable UUID id,
            @RequestBody InputProductAmountDto inputProductAmountDto
    ) {
        ApiResponse apiResponse = inputProductWarehouseService.updateInputProductAmount(id, inputProductAmountDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @GetMapping("/product/{id}")
    public HttpEntity<?> getInputProduct(
            @PathVariable UUID id
    ) {
        ApiResponse inputProduct = inputProductWarehouseService.getInputProduct(id);
        return ResponseEntity.status(inputProduct.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(inputProduct);
    }

    @GetMapping("/trade/{id}")
    public HttpEntity<?> getInputTrade(
            @PathVariable UUID id
    ) {
        ApiResponse inputTrade = inputProductWarehouseService.getInputTrade(id);
        return ResponseEntity.status(inputTrade.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(inputTrade);
    }

    @GetMapping("/product/all")
    public HttpEntity<?> getAllInputProduct(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        PageableDto allInputProduct = inputProductWarehouseService.getAllInputProduct(page, size);
        return ResponseEntity.ok(allInputProduct);
    }

    @GetMapping("/trade/all")
    public HttpEntity<?> getAllInputTrade(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        PageableDto allInputTrade = inputProductWarehouseService.getAllInputTrade(page, size);
        return ResponseEntity.ok(allInputTrade);
    }
}
