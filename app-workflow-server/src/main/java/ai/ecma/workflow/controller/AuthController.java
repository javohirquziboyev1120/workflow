package ai.ecma.workflow.controller;

import ai.ecma.workflow.config.MessageLanguage;
import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.SignInDto;
import ai.ecma.workflow.payload.SignUpDto;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MessageLanguage messageLanguage;

    @PostMapping("/register")
    public HttpEntity<?> register(@Valid @RequestBody SignUpDto signUpDto) {

        ApiResponse response = authService.register(signUpDto);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
//        return ResponseEntity.ok("OK");

    }

    @PostMapping("/login")
    public HttpEntity<?> login(@Valid @RequestBody SignInDto signInDto) {
        ApiResponse response = authService.login(signInDto);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

}
