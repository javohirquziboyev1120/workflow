package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CompanyRoleDto;
import ai.ecma.workflow.payload.PageableDto;
import ai.ecma.workflow.service.CompanyRoleService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/company/role")
public class CompanyRoleController {

    @Autowired
    CompanyRoleService companyRoleService;

    @PostMapping
    public HttpEntity<?> addCompanyRole(
            @RequestBody CompanyRoleDto companyRoleDto
    ) {
        ApiResponse apiResponse = companyRoleService.addCompanyRole(companyRoleDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editCompanyRole(
            @PathVariable UUID id,
            @RequestBody CompanyRoleDto companyRoleDto
    ) {
        ApiResponse apiResponse = companyRoleService.editCompanyRole(id, companyRoleDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
    }

    @GetMapping("/permissions")
    public HttpEntity<?> getAllPermissions(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        PageableDto permissions = companyRoleService.getAllPermissions(page, size);
        return ResponseEntity.ok(permissions);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllCompanyRole(
            @RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size
    ) {
        PageableDto permissions = companyRoleService.getAllCompanyRole(page, size);
        return ResponseEntity.ok(permissions);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCompanyRoleDto(
            @PathVariable UUID id
    ) {
        ApiResponse companyRoleDto = companyRoleService.getCompanyRoleDto(id);
        return ResponseEntity.ok(companyRoleDto);
    }
}
