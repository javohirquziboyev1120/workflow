package ai.ecma.workflow.controller;

import ai.ecma.workflow.payload.ApiResponse;
import ai.ecma.workflow.payload.CustomerPaymentDto;
import ai.ecma.workflow.service.CustomerPaymentService;
import ai.ecma.workflow.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/customerPayment")
public class CustomerPaymentController {
    @Autowired
    CustomerPaymentService customerPaymentService;

    @PostMapping
    public HttpEntity<?> saveCustomerPayment(@RequestBody CustomerPaymentDto dto) {
        ApiResponse apiResponse = customerPaymentService.addAndEditCustomerPayment(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @PutMapping
    public HttpEntity<?> editCustomerPayment(@RequestBody CustomerPaymentDto dto) {
        ApiResponse apiResponse = customerPaymentService.addAndEditCustomerPayment(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @DeleteMapping
    public HttpEntity<?> deleteCustomerPayment(@RequestParam(name = "customerPayment") UUID id) {
        return ResponseEntity.ok(customerPaymentService.deleteCustomerPayment(id));
    }

    @GetMapping("/page")
    public HttpEntity<?> getCustomerPaymentPage(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                                @RequestParam(value = "outputTradeId") UUID outputTradeId) {
        return ResponseEntity.ok(customerPaymentService.getCustomerPaymentPage(outputTradeId, page, size));
    }
}
