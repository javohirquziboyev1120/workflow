package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Expense;
import ai.ecma.workflow.entity.ExpenseType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Date;
import java.util.UUID;

@Projection(name = "customExpense", types = {Expense.class})
public interface CustomExpense {
    UUID getId();
    String getNameUz();
    String getNameRu();
    Double getAmount();
    Date getDate();
    String getDescription();

    @Value("#{target.expenseType.id}")
    UUID getExpenseTypeId();

    @Value("#{target.cashBox.id}")
    UUID getCashBoxId();

}
