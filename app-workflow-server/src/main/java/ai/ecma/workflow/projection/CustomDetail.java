package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Detail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customDetail", types = Detail.class)
public interface CustomDetail {

    UUID getId();

    String getNameUz();

    String getNameRu();

    @Value("#{target.company.id}")
    UUID getCompanyId();
}
