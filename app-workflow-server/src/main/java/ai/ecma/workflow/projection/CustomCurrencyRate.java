package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.CurrencyRate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCurrencyRate", types = CurrencyRate.class)
public interface CustomCurrencyRate {

    UUID getId();

    Double getFormValue();

    Double getToValue();

    @Value("#{target.fromCurrency.id}")
    UUID getFromCurrencyId();

    @Value("#{target.toCurrency.id}")
    UUID getToCurrencyId();

}
