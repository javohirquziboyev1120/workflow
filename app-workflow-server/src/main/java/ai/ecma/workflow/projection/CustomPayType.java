package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.PayType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customPayType", types = PayType.class)
public interface CustomPayType {

    UUID getId();

    String getNameUz();

    String getNameRu();

    @Value("#{target.currencyType.id}")
    UUID getCurrencyTypeId();
}
