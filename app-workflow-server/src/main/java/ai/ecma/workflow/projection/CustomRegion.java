package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Region;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customRegion",types = {Region.class})
public interface CustomRegion {
    Integer getId();
    String getNameUz();
    String getNameRu();
}
