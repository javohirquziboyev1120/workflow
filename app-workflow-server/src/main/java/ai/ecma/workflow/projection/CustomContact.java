package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Contact;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customContact", types = Contact.class)
public interface CustomContact {

    UUID getId();

    String getPhoneNumber();

    String getEmail();

    Float getLan();

    Float getLat();

    String getStreet();

    @Value("#{target.district.id}")
    Integer getDistrictId();
}
