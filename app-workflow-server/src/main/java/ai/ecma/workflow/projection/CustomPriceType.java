package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.PriceType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customPriceType", types = PriceType.class)
public interface CustomPriceType {

    UUID getId();

    String getNameUz();

    String getNameRu();

    @Value("#{target.company.id}")
    UUID getCompanyId();
}
