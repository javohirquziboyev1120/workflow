package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Note;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;
import java.util.UUID;

@Projection(name = "customNote", types = {Note.class})
public interface CustomNote {
    UUID getID();
    String getNameUz();
    String getNameRu();
    Timestamp getNoteTime();
    Integer getRepeat();
    Integer getNoteDuration();
    boolean isEnabled();

}
