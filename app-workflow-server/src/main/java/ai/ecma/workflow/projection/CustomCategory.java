package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Category;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCategory", types = {Category.class})
public interface CustomCategory {
    UUID getId();
    String getNameUz();
    String getNameRu();
    Integer getIndex();

    @Value("#{target.company?.id?:null}")
    UUID getCompanyId();

    @Value("#{target.parent?.id?:null}")
    Integer getParentId();
}
