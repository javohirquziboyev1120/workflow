package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.CurrencyRate;
import ai.ecma.workflow.entity.CurrencyType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCurrencyType", types = CurrencyType.class)
public interface CustomCurrencyType {

    UUID getId();

    String getDescription();

    @Value("#{target.company.id}")
    UUID getCompanyId();


}
