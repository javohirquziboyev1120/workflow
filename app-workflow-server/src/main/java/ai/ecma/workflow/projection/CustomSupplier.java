package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Supplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customSupplier", types = Supplier.class)
public interface CustomSupplier {

    UUID getId();

    String getName();

    String getEmail();

    String getPhoneNumber();

    String getAddress();

    boolean isStatus();

    @Value("#{target.company.id}")
    UUID getCompanyId();
}
