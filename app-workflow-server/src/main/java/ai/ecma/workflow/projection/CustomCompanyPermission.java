package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.CompanyPermission;
import ai.ecma.workflow.entity.CurrencyRate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCustomCompanyPermission", types = CompanyPermission.class)
public interface CustomCompanyPermission {

    UUID getId();

    String getCompanyPermissionEnum();

    Double getToValue();

    @Value("#{target.companyRole.id}")
    UUID getCompanyRoleId();
}
