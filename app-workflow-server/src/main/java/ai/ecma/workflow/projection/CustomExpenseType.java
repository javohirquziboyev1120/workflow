package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.ExpenseType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customExpenseType", types = {ExpenseType.class})
public interface CustomExpenseType {
    UUID getId();
    String getNameUz();
    String getNameRu();

    @Value("#{target.company.id}")
    UUID getCompanyId();

}
