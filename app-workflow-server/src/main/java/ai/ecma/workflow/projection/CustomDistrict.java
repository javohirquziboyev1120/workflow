package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.District;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customDistrict", types = District.class)
public interface CustomDistrict {

    Integer getId();

    String getNameUz();

    String getNameRu();

    @Value("#{target.region.id}")
    Integer getRegionId();
}
