package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.SystemRole;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customRole", types = {SystemRole.class})
public interface CustomSystemRole {
    Integer getId();
    String getName();

}
