package ai.ecma.workflow.projection;

import ai.ecma.workflow.entity.Values;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customValue", types = Values.class)
public interface CustomValue {

    UUID getId();

    String getNameUz();

    String getNameRu();

    @Value("#{target.detail.id}")
    UUID getDetailId();
}
