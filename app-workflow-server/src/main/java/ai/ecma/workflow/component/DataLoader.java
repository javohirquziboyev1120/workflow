package ai.ecma.workflow.component;

import ai.ecma.workflow.entity.CompanyPermission;
import ai.ecma.workflow.entity.CompanyRole;
import ai.ecma.workflow.entity.User;
import ai.ecma.workflow.entity.enums.CompanyPermissionEnum;
import ai.ecma.workflow.repository.UserRepository;
import ai.ecma.workflow.repository.dataRest.CompanyPermissionRepository;
import ai.ecma.workflow.repository.CompanyRoleRepository;
import ai.ecma.workflow.repository.dataRest.SystemRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SystemRoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CompanyPermissionRepository companyPermissionRepository;
    @Autowired
    CompanyRoleRepository companyRoleRepository;
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {

            List<CompanyPermission> companyPermissions = new ArrayList<>();

            for (CompanyPermissionEnum permissionName : CompanyPermissionEnum.values()) {

                companyPermissions.add(companyPermissionRepository.save(
                        new CompanyPermission(permissionName)));

            }
            CompanyRole companyRole = new CompanyRole();
            companyRole.setName("Admin");
            companyRole.setCompanyPermissions(companyPermissions);
            CompanyRole savedCompanyRole = companyRoleRepository.save(companyRole);

            userRepository.save(new User(
                    "Xamrozjon",
                    "Suyunov",
                    "info@ecma.ai",
                    "+998974474741",
                    passwordEncoder.encode("2"),
                    roleRepository.findAll(),
                    savedCompanyRole
            ));
        }
    }
}
